---
title: "La crevasse"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - ice breaker
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 15min à 30mn
- Nombre de participants : 

## Speech de présentation

`Dans cet atelier, vous allez être des alpinistes qui se retrouvent face à une crevasse qu’il n’est possible de passer qu’en binômant.`

## Processus

- Préparation :
  - Formez au sol deux lignes de 4 à 6m semi-parallèles, en entonnoir avec le scotch.
  - Faire un marquage au niveau de la partie la plus large +, ++ ,+++
  - Le + augmente le bout de la crevasse de 2 x 15cm, le ++ augmente de 2 x 30cm et le +++ augmente de 2 x 45cm
  - Faire un affichage avec les noms des binômes en ligne et les +, ++ ,+++ en colonnes
- Les joueurs doivent former des binômes qui vont devoir traverser une crevasse en se tenant par la main
- Demandez à chaque binôme d’indiquer, selon lui, le niveau maximum qu’il peut atteindre
- Demandez aux binômes de se positionner au début de la crevasse (partie étroite) et de parcourir la crevasse de cette manière
- Faites passer les groupes 1 par 1 et quand jusqu’à ce que tout le monde ait réussi
- S’il y a un problème de confiance dans sa propre réussite individuelle, proposer de s’entraîner contre un mur (plus stable, ne risque pas d’entraîner dans la chute)
- Une fois que tout le monde est passé, agrandir la crevasse. Prendre le scotch et le tendre entre le point de départ initial et les nouveaux marqueurs d’arrivée
- Faire passer les groupes (si certains groupes n’y arrivent pas, pousser les autres participants en réussite à leur donner des conseils, les guider (“D’après vous, comment pourraient-ils se mettre pour réussir ?”)

## Feedback

- Que fallait-il faire pour réussir ?
- Comment s’est matérialisée la notion de confiance dans cet atelier ?
- Qui n’a pas atteint son objectif de réussite (revoir le tableau) ? Qui l’a atteint ? Qui l’a dépassé ? Comment avez-vous fait ? (Généralement “essaies”, “réadaptation”...)
- Comment cela peut-il se démontrer dans votre quotidien ?

## Apprentissage

- C’est donc un atelier basé sur la confiance
  - Confiance en soi (je peux réussir)
  - Confiance en l’autre (nous pouvons réussir)
  - Confiance dans la technique (notre mode de fonctionnement est le bon, notre solution est la bonne)
- Savoir communiquer, se reposer sur l’autre, ne pas avoir peur de l’échec et tester
- Si vous avez des groupes qui ont essayé contre un mur ou dans un espace hors de la Crevasse, cela s’apparente à de la formation, apprendre et s’entraîner dans un espace sécurisé

## Conseils

- Si un groupe n’y arrive pas, lui proposer de se mettre sur le côté et de s’entraîner à voir jusqu’où il peut descendre
- Tout se joue dans le positionnement des corps :
  - Une personne qui n’a pas confiance dans sa réussite va avoir tendance à se plier en deux - les jambes droites et verticales, le corps plié et horizontal.
  - Une personne en confiance va se reposer sur son partenaire et les corps seront droits et penchés reposants l’un sur l’autre et ayant trouvés leur equilibre

## Sources

- http://javamind-fr.blogspot.fr/2013/03/agilegame-la-crevasse.html
- Site de l’auteur : http://www.vialaventure.fr
- http://javamind-fr.blogspot.fr/2013/03/agilegame-la-crevasse.html
