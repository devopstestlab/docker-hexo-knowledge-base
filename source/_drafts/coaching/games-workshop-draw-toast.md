---
title: "Draw Toast"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Collectif, confiance et leadership
- Durée : 10-15mn
- Nombre de participants : 2 à 50

## Speech de présentation

`Je vais vous demander de dessiner comment vous faites des toasts`

## Processus

- Donner 2mn pour que chaque participant ait fait son dessin
- Proposez aux participants de positionner tous leurs dessins sur un mur et de prendre du recul. Laissez-les questionner, comparer, voir les similarités et les différences.

## Feedback

- Observez-vous des similarités entre tous ces dessins ? Quel serait le processus commun pour faire des toasts ?
- Combien de temps en moyenne avez-vous mis à comprendre le dessin d’un autre ?
- Selon vous, combien de temps auriez-vous pris si cette même personne avait décrit son dessin de façon littérale. Plus de temps ? Moins de temps ?

## Apprentissage

- Bien que tous les dessins soient différents, le processus est le même. Il n’y a pas de bonne et de mauvais réponse, il n’y a que des différentes manières de visualiser la même chose.
- La visualisation de ce type est facilement compréhensible bien qu’ils soient visuellement aussi riches et divers que leurs dessinateurs.
- Quand des personnes visualisent un modèle mental, il incluent habituellement 5 à 7 éléments liés entre eux par des lignes et des flèches. Le nombre d’éléments tend à correspondre au nombre de choses que les personnes peuvent retenir dans leur mémoire court terne (The Magical Number Seven, Plus or Minus Two)

## Conseils


## Sources

- http://gamestorming.com/core-games/draw-toast
- https://en.wikipedia.org/wiki/Visual_thinking
