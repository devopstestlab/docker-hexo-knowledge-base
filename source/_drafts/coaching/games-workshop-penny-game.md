---
title: "Penny Game"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots-clés : Découvrir l’agilité et le Lean par le jeu
- Durée : 1 h
- Nombre de participants : 5 à 18 (formez des groupes impairs si possible de 5 à 7)

## Speech de présentation

"Le but du jeu est de passer aussi rapidement que possible 20 pièces jusqu’à votre client."

## Processus

- Formez des groupes de 5 avec :
  - Le rôle des travailleurs est de faire passer les pièces jusqu’au client, le rôle du client est de chronométrer le temps que met la première pièce à arriver à lui et le temps que toutes les mettent à arriver vers lui, le rôle des managers est de mesurer le temps que prend son travailleur à se débarrasser de toutes ses pièces
    - 1 client
    - 2-3 travailleurs
    - 2-3 managers (1 par travailleur)
  - construisez un tableau de résultat avec une colonne par équipe et une ligne par itération qui contiendra : le temps de chaque travailleur et le temps de réception client
  - Placez les travailleurs et le client côte à côte comme une chaîne de production, positionnez les managers en face
  - Itération 1 : “Vous allez devoir passer le lot de ces 20 pièces à l’autre travailleur puis au client en suivant ces règles :
    - Avant de passer le lot, vous devez retourner toutes les pièces 1 par 1 (vous n’avez pas le droit de tout retourner d’un coup) avec votre main non directrice
    - Vous ne pouvez passer les pièces retourner à votre voisin qu’à partir que par lot de 20 pièces retournées (pas de fur et à mesure)
    - Notez les résultats sur le tableau
      - 5min de rétro de l’équipe pour décider d’une et une seule amélioration (taille du batch, technique, passer deux pièces en même temps... en gros amélioration du processus ou de la performance individuelle). L’amélioration peut être différente par équipe.
  - Itération 2 : idem en prenant en compte l’amélioration de rétro
  - Itération 3 : idem et si le groupe n’a pas décidé d’action de diminution de batch, les forcer (descendre à 15 ou 10)
  - Itération 4 : on continue

## Feedback

- Comment ça va ? Comment vous êtes-vous senti tout au long de l’atelier?
- Que s’est-il passé ? Quelles différences avez-vous apportées d’une itération à l’autre ?
- Y a-t-il eu des changements fondamentaux dans vos résultats ? Qu’est-ce qui a apporté ça ?
- Que peut-on dire sur l’impact de la taille des batchs ?
- Qu’en retenez-vous dans votre quotidien ?
- Que pourriez-vous faire dès demain avec cet apprentissage ?

## Apprentissage

- Les batchs plus petits permettent de commencer à fournir de la valeur au client plus rapidement
- Il est important de déclencher les idées d’amélioration une par une pour mesurer leur impact. Si on déclenche plusieurs améliorations, comment s’assurer laquelle est la plus importante ?
- Zoomer sur le temps de travail et le temps de passage (passer les pièces au voisin) Le sentiment de stress est impacté par la taille des batchs. Avec un gros batch, on sent la pression de l’attente puis plus rien. Avec des trop petits batchs, on a un stress fort permanent.

## Sources

- http://tastycupcakes.org/2013/05/the-penny-game
