---
title: "Le Principe de Pareto"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 
- Nombre de participants : 

Le principe de Pareto (ou la règle des 80/20) a été présenté par Vilfredo Pareto en 1895. Selon ce principe, la société est composée de deux groupes de personnes : 20 % des plus riches contrôlent plus de 80 % de la richesse; 80 % des plus pauvres ne contrôlent que 20 % de la richesse.

Cette règle peut être appliquée à de très nombreux autres domaines, dont la concentration sur les tâches importantes : 20 % des activités comptent pour 80 % de la valeur de l’ensemble des activités. Autrement dit, parmi une liste de dix choses à faire dans une journée, deux de ces éléments ont une valeur supérieure à toutes les autres réunies.

C’est pourquoi il est important de se concentrer sur les quelques tâches qui sont à très haute valeur ajoutée.

L’idée est de faire moins de choses mais de faire les choses les plus importantes.

## Speech de présentation


## Processus

- Faire une liste de tout ce que vous faites au cours d'un mois.
- Posez-vous les questions pour analyser l’importance de ces tâches afin d’en identifier juste quelques unes.
- Demander aux personnes d'apporter une liste de ses activités à une réunion.
- Aider chacun à définir clairement leurs tâches les plus importantes du point de vue contribution l'organisation.
- Les encourager à achever ces tâches avant de faire quoi que ce soit d'autre.

## Feedback


## Apprentissage


## Conseils

- Pour identifier ces tâches, répondez à la question "Si vous ne pouviez faire qu'une seule chose tout au long de la journée, quelle activité apporterait le plus de valeur ?" Pour y répondre, ne pas hésiter à poser des questions.
- Puis, à chaque vois que vous voulez prendre une activité en plus, posez-vous la question "Si je ne pouvais faire que deux choses par jour, quelle serait la deuxième chose la plus précieuse que je ferais ?" Et ainsi de suite.
- Aider les personnes à définir leurs trois tâches principales en discutant des responsabilités de chacun en matière de résultats.
- En procédant ainsi, tout le monde sait ce que les autres sont censés faire et dans quel ordre d'importance. Cela crée une pression naturelle saine pour s’occuper de ses tâches les plus importantes.
- Fixer des priorités claires pour chacun.
- Répéter encore et encore que chacun doit travailler sur les 20 % de ses activités qui représentent 80 % de la valeur de sa contribution.
- Apprendre aux personnes comment analyser leur travail et à fixer les priorités.
- Si ces tâches apportent une contribution précieuse au travail, cela apporte reconnaissance du travail de chacun et respect.
- Essayer de réduire les sollicitations extérieures perturbatrices de la concentration (notifications d'emails, appels téléphoniques, interruptions par d'autres personnes, …).

## Sources
