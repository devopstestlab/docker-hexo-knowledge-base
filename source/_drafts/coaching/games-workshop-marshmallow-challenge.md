---
title: "Marshmallow Challenge"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots-clés : Découvrir l’agilité et le Lean par le jeu
- Durée : 45min à 1h30mn
- Nombre de participants : 6 à 100

## Speech de présentation

Votre but est de construire, en 18 minutes, la plus haute tour de spaghettis possible qui soutienne le marshmallow”
Former des équipes de 4 personnes
Chaque équipe possède un kit Marshmallow Challenge
Construire la tour la plus haute en 18 minutes
    Elle sera mesurée de la table jusqu’au sommet du marshmallow
    La totalité du Marshmallow doit être au sommet (rien au-dessus)
    Aucun autre matériel n’est admis
    Toucher ou supporter la structure à la fin vous mènera à la disqualification
    Vous êtes libres de couper tout le matériel
    Vous pouvez utiliser autant de matériel que nécessaire dans le kit
Debrief ensemble

## Processus


## Feedback

Que s’est-il passé? Qu’avez-vous observé?
Comment vous sentez-vous ?
Qu’est-ce qui a bien marché ? Qu’est-ce qui a moins bien marché ?
Quels sont les éléments significatifs différenciant vos équipes en termes de comportement, processus de groupe ?
Qu’avez-vous appris ?

## Apprentissage

Le Marshmallow Challenge est une métaphore des suppositions d’un projet “un marshmallow, c’est léger, c’est sur qu’il sera supporté par les spaghettis”.
    La leçon est qu’il est nécessaire d’identifier les suppositions dans un projet - les vrais besoins des utilisateurs, les coûts de production... - et les tester rapidement.
Sur 18 minutes, la plupart des joueurs passe 3min à s’organiser, 5min à étudier la structure à construire, 8min à construire et les tours derniers moments à mettre le marshmallow au sommet et vérifier. Le résultat, généralement, est que la structure n’arrive pas à soutenir le marshmallow et il reste donc très peu de temps pour réajuster.
    La stratégie est alors de vérifier très régulièrement que la structure soutient bien le marshmallow. Réduire ce qu’on appelle “la boucle de feed-back” qui permet de se réajuster le plus rapidement possible.
    Schéma fun à montrer : “Qui, selon vous, performe le moins bien et le mieux ?”. On s’aperçoit que les élèves de maternelle se positionnent en bonne position, car ils passent à la phase de construction très rapidement et testent très vite la structure avec le marshmallow.
    Dans le graphique, on voit aussi que le PDF + Secrétaire réussit mieux que le PDG seul. Pourquoi ? Parce que des profils variés avec des points de vue variés et des compétences variées permettent de régler plus rapidement les blocages et de performer

## Sources

https://www.ted.com/talks/tom_wujec_build_a_tower?language=fr
http://www.tomwujec.com/design-projects/marshmallow-challenge
http://meprogram.com.au/wp-content/uploads/2016/02/TED2010_Tom_Wujec_Marshmallow_Challenge_Web_Version.pdf
