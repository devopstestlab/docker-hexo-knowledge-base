---
title: "Interview croisée"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - ice breaker
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 15min à 45mn
- Nombre de participants : 4 à 12

## Speech de présentation

`Fournir à chaque participant une feuille A5 et leur demander de renseigner : Je vais vous demander de vous mettre en binômes et d’interviewer votre interlocuteur, vous allez constituer une carte d'identité contenant les informations suivantes.`

## Processus

- Nom Prénom
- Rôle dans le projet/dans l’entreprise
- Une anecdote particulière (au choix de l’animateur) :
  - Quel est votre restaurant préféré ?
  - Quelle est la chose la plus folle que vous ayez jamais faite dans votre vie ?
  - Quel serait votre voyage de rêve ?
- Vous pouvez aussi poser une question ayant trait à la réunion (au choix de l’animateur) :
  - Pour vous, c’est quoi l’agilité ?
  - Quelle serait la plus belle chose qu’on pourrait faire avec ce produit ?
- Inviter à dessiner un portrait pour la carte d’identité
- Demander à chacun de présenter la carte d’identité qu’il vient de remplir et ainsi présenter son interlocuteur
  - Cette activité permettra aux participants de retenir les prénoms, de donner une teinte conviviale à la réunion et, au travers du dessin, d’amorcer la créativité des personnes.

## Feedback


## Apprentissage


## Conseils


## Sources
