---
title: "Le nombre secret"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - ice breaker
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 5min à 30mn
- Nombre de participants : 3 à 20

## Speech de présentation


## Processus

- Laissez les participants comprendre petit à petit avec les instructions
  - Inviter chaque participant à écrire un nombre sur un petit papier à le plier en 8 et à le mettre dans sa poche
  - Une fois que les papiers sont bien rangés, demander au groupe de se mettre dans l’ordre croissant des nombres de chacun en suivant ces règles : “Je vais vous demander de vous mettre par ordre croissant du nombre que vous avez choisi, mais :
    - Interdit de parler
    - Interdit d’écrire le nombre
    - Interdit de compter le nombre sur les doigts
    - Interdit de dessiner le nombre dans l’air
  - Assurez-vous que tout le monde a parfaitement compris et intégré les règles sans quoi le jeu sera gâché
  - Laissez au groupe le temps de s’organiser, observez des groupes se former par ordre de grandeur et pousser le groupe à ne former qu’une ligne (il est possible que deux personnes aient choisi le même nombre)
  - Une fois tout le monde aligné, faire le compte
- L’objectif de ce jeu et de pousser les participants à communiquer et trouver des solutions ensemble. Avoir l’idée d’un mode de communication n’est utile que si quelqu’un le comprend. Il pousse aussi à confronter les idées et les propositions et voir si deux leaders avec deux idées différentes arrivent à se mettre d’accord sans parler et donc sans argumenter.
- Cet ice-breaker invite à l’observation globale du groupe (des petits groupes se forment autour d’un même mode de langage, mais n’arrivent pas à se raccrocher au reste des participants, certains participants n’arrivent pas à se comprendre, ils peuvent peut être aller chercher des idées autour d’eux)
- L’objectif de ce jeu est de mettre en évidence l’observation et d’étudier le leadership par l’idée. Il faut être écouté et suivi par les autres membres de l’équipe pour que l’idée prenne, se propage et montre son efficacité.
- Grâce à l’Ice breaker, les participants apprendront à se connaître et travailleront donc ensemble plus facilement lors de la suite du team building.

## Feedback


## Apprentissage


## Conseils


## Sources

- https://blog.goood.pro/2014/02/13/agile-game-france-inside-oeil-de-gregory
