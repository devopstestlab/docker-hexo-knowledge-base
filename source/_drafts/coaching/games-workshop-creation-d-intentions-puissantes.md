---
title: "Création d’intentions puissantes"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Durée : 15 à 45 minutes
- Nombre de participants : 

La technique de création d'intentions fortes permet de confirmer avec une personne pourquoi le désir d’une chose est important pour elle et comment et de quelle manière il leas motive.
Cette technique, qui repose sur la PNL, a été développée par Robert Dilts dans son livre From Coach to Awakener (2003).

## Speech de présentation


## Processus

Créer des intentions fortes en utilisant le modèle "(sujet) est important et souhaitable ..." :
- "Parce que vous ..." : C’est la raison pour laquelle le sujet est important.
- "Par conséquent, vous ..." : Ce sont les avantages que la personne peut en retirer.
- "Chaque fois que vous ..." : C’est le contexte dans lequel la personne obtiend ces avantages.
- "Pour que vous ..." : C’est la raison pour laquelle les avantages sont importants dans ce contexte.
- "Si vous ..." : C’est ce qui est vrai en ce qui concerne l'importance du sujet.
- "Bien que vous ..." : C’est la reconnaissance que la personne n'agit pas toujours de manière conforme au sujet.
- "De la même manière que vous ..." : C’est la reconnaissance que la personne a besoin d'être rassurée sur le fait que la motivation sous-jacente est toujours là.

Cette technique permet de comprendre ce qui motive vraiment une personne et aide à utiliser ses ressources internes. En cas de doute ultérieur, elle peut se référer à sa déclaration de mission pour se rappeler le "pourquoi".

Cette technique est particulièrement adaptée en cas de manque de motivation ou si une personne évite systématiquement de prendre des mesures. Elle peut aussi être utilisée avec un groupe.

Elle nécessite d’avoir déjà une relation suffisamment développée avec la personne.

Processus :
- Poser les questions du modèle :

```
Exemple : (REVOIR L’EXEMPLE)
- "Parce que vous ... bénéficiez d'un réseau de soutien et de la possibilité de partager avec d'autres."
- "C’est pourquoi vous ... développez des liens durables avec les autres."
- "Chaque fois que vous ... voulez passer un moment agréable en société."
- "Pour que vous ... puissiez faire rebondir les idées des autres et obtenir un retour d'information."
- "Si vous ... aimez rire et être vous-même avec des gens en qui vous avez confiance."
- "Bien que vous ... parfois vous trouvez que vous pouvez vous éloigner de certains amis."
- "De la même manière que vous avez besoin de la compagnie des autres pour rester en contact avec la vie."
```

  - Commencer par l'affirmation "L'amitié est importante et souhaitable… » suivie de l'ouverture de chaque phrase.
  - Inviter le client à compléter la phrase suivante.
- Noter les mots exacts utilisés.
- Passer en revue avec la personne les déclarations.
- Lui demander de supprimer les mots qui les rejoignent, à l'exception de "Bien que vous..." :

```
Reformulez ensuite ce texte en une déclarations en supprimant les mots en début de phrase (sauf "Bien que ») et en utilisant la première personne :
L'amitié est importante et souhaitable : (REVOIR L’EXEMPLE)
- "Je bénéficie d'un réseau de soutien et de la possibilité de partager avec les autres."
- "Je développe des liens durables avec les autres."
- "Je veux passer un moment agréable en compagnie des autres."
- "Je peux faire rebondir des idées sur les autres et obtenir un retour d'information."
- "J'aime rire et être moi-même avec des gens en qui j'ai confiance."
- "Bien que je trouve parfois que je peux m'éloigner de certains amis."
- "J'ai besoin de la compagnie des autres pour rester en contact avec le fait d'être en vie."
```

## Feedback


## Apprentissage


## Conseils

- Ne pas expliquer les affirmations.
- Ce sont les mots de la personne qui ont le plus de pouvoir pour motiver la personne à atteindre un objectif.
- Faire écrire par la personne la déclaration de ses propres mains.
- Lui demander de la garder avec elle tout le temps.
- L’écriture crée rappel très puissant du lien émotionnel des mots.
- Si la personne est un peu tendue ou agitée, encourager à faire des exercices de respiration profonde.

## Sources
