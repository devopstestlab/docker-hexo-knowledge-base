---
title: "Remember the future (Innovation)"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Jeux pour faire
- Durée : 30mn-2h
- Nombre de participants : 3 à 15

## Speech de présentation

`Vous êtes 3 mois après la mise en production de votre produit/après la résolution de votre problème majeur. Que voyez vous, qu’entendez-vous, que ressentez vous qui montre que votre produit est un succès magnifique/que votre problème est réglé ? Comment cela se matérialise-t-il ?`

## Processus

Posez le sujet à vos participants, invitez-les à se lâcher, à imaginer l’inimaginable
Sur une phase de 30mn :
- Laissez 5min de réflexion individuelle pour permettre à tous de s’exprimer
- Laissez les participants partager entre eux pendant 15mn
- Debrief et présentation des résultats

## Feedback


## Apprentissage


## Conseils

Variante pour monter un plan d’action :
- Tracez une grande ligne de temps sur un mur. Le point d’arrivée est la date de sortie du produit ou de résolution du problème
- Partir de la fin de la ligne et remontez dans le temps (qu’avez-vous fait juste avant que ça sorte ? Et juste avant pour en arriver là et juste avant pour en arriver là ?) Vous pouvez y ajouter des ramifications s’il semble qu’il y a plusieurs prérequis

## Sources

- http://www.innovationgames.com/remember-the-future
