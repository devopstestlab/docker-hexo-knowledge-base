---
title: "SOS Titanic"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Collectif, confiance et leadership
- Durée : 20mn
- Nombre de participants : 7

## Speech de présentation

`Demander au groupe de 7 de se mettre sur le sac poubelle déposé au sol. “Vous êtes des passagers du Titanic et votre bateau a coulé, heureusement vous avez trouvé un canot de sauvetage, mais malheureusement il est retourné. Votre but va être de remettre votre canot dans le bon sens sans jamais mettre un pied dans les eaux glacial de l’Atlantique nord!”`

## Processus

- Mettez le sac poubelle au sol
- Demandez à 7 personnes de se mettre sur le sac
- Racontez l’histoire à l’équipe du Titanic
- Ils vont devoir retourner le sac poubelle sans jamais poser un pied en dehors du sac
- Chronométrez et laissez tourner

## Feedback

- S’ils réussissent :
  - Comment ça va ?
  - Qui a trouvé la solution ? Comment ? Vous avez détecté le leader
  - Les autres, pourquoi l’avez-vous écouté ? Qui a été le premier à l’écouter ?
  - Comment avez-vous réussi à mener à bien l’idée du leader ? Qu’avez-vous fait concrètement ? Checker si certains se sont mis en danger, dans des positions impossibles, en déséquilibre pour réussir le challenge. On peut se mettre en danger pour mener à bien une idée à laquelle on croit
- S’ils ne réussissent pas (c’est rare):
  - Comment ça va ?
  - Que vous a-t-il manqué pour réussir ?
  - (si quelqu’un a eu la bonne idée) Quelqu’un a eu une idée, pourquoi ne l’avez vous pas suivi ?
  - (si personne n’a eu d’idée) Que vous fallait-il pour trouver une idée ? Peur de l’échec, test and learn, essayer des choses, pensez différemment, changer de point de vue

## Apprentissage

Plusieurs types de leadership : leadership par l’action, leadership par l’idée, leadership par l’encouragement, leadership par la cohésion du groupe

## Conseils


## Sources
