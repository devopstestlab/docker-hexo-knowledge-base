---
title: "Product Box (Innovation)"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Jeux pour faire
- Durée : 1h30-2h
- Nombre de participants : 3 à 15

## Speech de présentation


## Processus

- Préparation : Récupérez une boîte de céréales et collez sur chaque face une feuille blanche pour créer une boîte vierge ou trouvez des boîtes d’archivage blanches
- Présentez comment se construit la boîte
- Construction : Laissez 15mn à l’équipe pour imaginer le produit, faites-les travailler sur des post-its à coller sur la boîte
- Feedback : demandez à chaque groupe de présenter le travail à ce niveau ; 3min pour présenter la boîte 5min de feed-back
- Adaptation : Donnez 15mn à l’équipe pour finaliser sa boîte avec les images
- Feed-back : 3min pour présenter la boîte, applaudir

## Feedback

- Que pensez-vous de votre produit ?
- Que retenez-vous du design de votre boîte ? Esthétiquement que pourriez-vous garder pour le design de votre produit ?
- Quelles idées vous semblent réalisables facilement ? Qu’allez-vous commencer en premier ?
- Quelles idées vous ont surpris ?
- Invitez les participants à partager leur boîte avec le maximum d’acteurs engagés sur le projet/produit

## Apprentissage


## Conseils


## Sources

- http://www.innovationgames.com/product-box
- http://www.officedepot.fr/catalog/catalogSku.do?id=7043318&XREF1=IAFRH10999GOOPS&=froogle-_-feed-_-Bo%C3%AEtes%20et%20caisses%20archives-_- 7043318&pr=DFT&gclid=COC6kLP0yNECFcLGGwodRL0Bzg#ectrans=1
