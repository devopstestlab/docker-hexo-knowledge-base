---
title: "Le glossaire visuel"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Durée : 30 minutes à 1 heure
- Nombre de participants : 2 à 10

Face à un terme inconnu, de nombreuses personnes font semblant de comprendre ce terme, ce qui peut être dangereux. Pour éviter cela, c’est une bonne pratique de définir ces termes visuellement.

## Speech de présentation

`Cet exercice est un moyen de créer un langage commun.`

## Processus

- Faire un brainstorming sur les expressions et termes difficiles rencontrés dans le groupe.
- Faire un brainstorming individuel sur des notes autocollantes.
- Afficher ces notes sur un mur et les examiner.
- Discuter des termes les plus courants et les plus importants.
- Regrouper ensemble les plus importants.
- Pour chacun des termes :
  - Demander au groupe de le décrire avec des mots.
  - Déterminer si ce terme est flou ou inadéquat.
  - Clarifier ce terme à l'aide d'une image :
    - A quoi cela ressemble-t-il ?
    - Si le terme est abstrait, utiliser un schéma.
    - Commencer par les personnes ou les choses impliquées. Puis les relier pour saisir visuellement la définition.

## Feedback


## Apprentissage


## Conseils

- Ne pas essayer de tout définir dès le début : commencer par ceux qui ont le plus de possibilités de clarification.
- Utiliser les éléments visuels dans les activités de suivi.
- Les mettre en ligne.
- Les insérer dans les supports de formation.

## Sources

- James Macanufo
