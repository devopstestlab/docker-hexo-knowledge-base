---
title: "Le noeud humain"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 5min à 20mn
- Nombre de participants : 6 à 15

## Speech de présentation


## Processus


## Feedback

- Ne pas trop en dire, laisser les participants comprendre peu à peu ce qu’ils vont devoir faire
- Demandez au groupe de se mettre en cercle
- Demandez à chacun de tenir deux personnes différentes par les mains. Cela devrait former une toile de bras au centre du cercle avec des bras au-dessus ou en dessous. Demandez alors au groupe de défaire le noeud de ces bras et de reformer un cercle sans jamais lâcher les mains que les personnes tiennent et sans se faire mal.

## Apprentissage


## Conseils


## Sources

