---
title: "Lean Startup Snowflakes"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Lean Startup et Design Thinking
- Durée : 1h
- Nombre de participants : 4 à 16 (max 8 par équipe)

## Speech de présentation

`Votre objectif est de conduire une entreprise rentable en créant et vendant des flocons de neige en papier. Je vais vous montrer qu’il est possible de réaliser ces flocons en moins de 3 minutes, en en faisant un pour vous maintenant.`

## Processus

- Préparation :
  - Pour chaque table :
    - Mettez 5 unités de valeur (2x2 + 1)
    - 5 feuilles blanches
  - Sur un mur visible :
    - Une feuille où figurent les prix de fournitures « feuilles : 2 pour 1€, ciseaux : 3€ »
    - Un tableau avec en colonnes : numéro de l’itération, trésorerie, travail en cours, nombre de ciseaux, quantités vendues, nombre de feuilles détenues.
- Installez les participants de 4 à 8 personnes par table
- Donnez-leur le contexte “votre objectif est de...”
- Donnez-leur les consignes pour faire un flocon : on prend une feuille A4, on commence par plier la feuille en triangle, on enlève la partie en trop. Lorsque le triangle est obtenu, on le plie en deux, deux fois de suite, de manière à obtenir au moins trois axes. Puis, on arrondit les angles du triangle à l’aide d’un ciseau afin d’obtenir un flocon de forme ronde. Le long des plis, on découpe des formes, afin d’embellir le flocon que l’on obtiendra en dépliant la feuille. Vous serez limité dans le temps de découpe, l’objectif étant d’expérimenter le fonctionnement d’une entreprise, et non de faire des flocons. L’itération de 3 minutes sera suivie d’un débriefing de 3 minutes, puis de 3 minutes supplémentaires afin de coordonner l’équipe, et de planifier l’itération suivante.
- Si vous vous retrouvez sans ressources, vous pouvez en acheter à n’importe quel moment au fond de cette salle. Papier : 2 feuilles pour 1€, ciseaux : 3€. Votre table peut s’auto-organiser autour de « comment produire un flocon de neige ».
- Demandez-leur s’ils ont des questions. S’ils demandent quoi que ce soit sur les critères d’acceptation, dites-leur que le sujet pourra être abordé lors de la vente du premier flocon. (l’objectif est de voir s’ils vont d’eux-mêmes vous demander ce que vous voulez où s’ils vont construire et essayer de vous vendre un flocon que vous n’aimerez pas)
- Faites tourner 4,5 itérations :
  - 3 minutes de production
  - 3/4 minutes de débrief
  - 3 minutes de planning de l’itération suivante
- Vos critères d’acceptante cachés :
  - les flocons de neige doivent être de forme ronde,
  - avoir 3 axes de symétrie,
  - les découpes doivent être précises.
  - Les papiers déchirés, carrés / rectangulaires, avec des recoupes sur la surface du flocon, les papiers apportés par le public / les participants doivent tous être rejetés. À chaque fois qu’un flocon vous est présenté, dites qu’il ne vous plait pas.
    - Expliquez pourquoi si et seulement si on vous pose une question précise Par exemple :
      - Pourquoi vous n’en voulez pas ? Il est moche. Comment ça il est moche ? Quel est le problème ? Le niveau de qualité n’est pas assez élevé ? En quoi ? Les angles sont déchirés.
      - Celui-ci ne me paraît pas « rond », je ne peux pas l’acheter
      - Au contraire, si le flocon vous plait sans plus, vous pouvez répondre
      - Il est beau – j’en donnerai 1€ sans expliquer pourquoi et donner des détails si demandés (j’aime la forme, les couleurs...). Savoir pourquoi le flocon est beau peut aider à reproduire mais creuser ce qui va bien n’est pas toujours un reflexe pour les participants
  - Ne marchandez pas, passez au vendeur suivant.
- Valorisation des flocons de neige :
  - Qu’ils soient complexes, uniques, symétriques, jolis, ils seront achetés entre 1€ et 5€. Durant le premier round, je n’ai jamais vu quelque chose valant plus d’1€. Je paie rarement plus de 3€.
  - Encouragez l’innovation en disant aux participants « c’est la première fois que je vois un flocon signé ! 2€! » ou d’autres commentaires de ce type.
  - Encouragez la complexité « impressionnant – un flocon très aéré, j’aime beaucoup ». Sur les questions de tailles, les petits flocons peuvent être achetés au prix de 2 pour 1€ à moins qu’ils ne soient particulièrement bien décorés. Lorsque vous achetez des flocons de neige, ordonnez-les sur une table de la plus petite valeur à la plus élevée ou accrochez-les au mur. Ne faites pas cela de manière tangible, mais en appliquant une estimation par intervalle, en faisant survoler l’ensemble du spectre au flocon que l’on vient de vous apporter et en disant « cela passe parfaitement ici, OK, 2€ ».

## Feedback

- Observez ce que les équipes font, et aidez-les à penser – lean startup.
- Ne faites qu’une allusion lors du débriefing au « lean startup » et laissez-les découvrir par eux-mêmes au cours de l’atelier.
- Quelques équipes ignoreront ce que vous direz, c’est normal. Au cours de l’atelier, vous pourrez faire quelques commentaires afin de faciliter la prise de conscience, voici quelques exemples :
  - Avez-vous besoin de finaliser un flocon entièrement pour avoir un retour des clients ?
  - Est-ce que votre équipe fait des profits ?
  - Savez-vous ce que veulent les clients/consommateurs ?
  - Avez-vous suivi les clients/consommateurs afin de voir ce qu’ils souhaitent acheter ?
  - Qu’est-ce qu’il arrive lorsque vous faites des flocons identiques ?
  - Devez-vous forcément utiliser la feuille entièrement pour réaliser un flocon ?

## Apprentissage

- La découverte du consommateur / client est une activité pour toute l’équipe (le responsable produit peut donner aux développeurs un faux sentiment de sécurité).
- Vous devez sortir de vos bureaux (ou, dans le cas présent, quitter votre table) afin de découvrir pour quoi les clients/consommateurs sont prêts à payer.
- La pression des livraisons associées à un travail créatif fait perdre aux collaborateurs la vision commune.
- Les activités de business et d’apprentissage fonctionnent mieux lorsque vous collaborez et partagez avec plus de personnes (les tables ne doivent pas rester des îles isolées).
- Les déchets / gaspillages viennent de la supposition que vous devez utiliser la feuille de papier complète, le volume de gaspillage que cela représente est généralement supérieur aux gains générés dans la rencontre avec le client.
- Vous n’avez rien de particulier à faire pour connaître les critères d’acceptation : simplement, allez voir le facilitateur et lui demander ce qu’il souhaite ? Réponse : beauté, symétrie, complexité, forme ronde.
- Le temps du consommateur / client est précieux et limité, utilisez-le avec discernement / sagesse.

## Conseils


## Sources

- http://tastycupcakes.org/fr/2012/05/lean-startup-snowflakes
