---
title: "Test de la NASA"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Collectif, confiance et leadership
- Durée : 45mn
- Nombre de participants : 6 à 24

## Speech de présentation

`Vous êtes des cosmonautes qui venez de vous écraser sur la lune, vous devez rejoindre une station et pour survivre, vous allez devoir prioriser les éléments nécessaires à votre survie`

## Processus

- Phase en individuel en 8min : demandez au groupe de renseigner la colonne A avec “1” l’objet le plus prioritaire et “15” le moins prioritaire
- Phase de groupe (3,4 personnes) en 10min : remplissez la colonne C Informez souvent le groupe du temps restant pour les forcer à poser des choses.
- Donnez la priorisation selon la NASA et faites remplir la colonne E
- Demandez à chaque personne d’indiquer :
  - Dans la colonne B : le résultat de valeur absolue(E moins A) qui est l’écart individuel avec la position de la NASA
  - Dans la colonne D : le résultat de valeur absolue (E moins C) qui est l’écart du groupe avec la position de la NASA
  - En bas des colonnes B et D, la somme des chiffres de la colonne (égal à la somme des écarts)
  - Toute dernière ligne de B : Moyenne des écarts individuels pour le groupe (somme des écarts indiv / nbre de participants du groupe)

## Feedback

- Qui a un écart plus petit en D qu’en B (normalement la plupart) ? Pourquoi selon vous ? (amener si nécessaire le terme intelligence collective)
- Pourquoi selon vous ? Pourtant aucun d’entre vous n’est expert. Vous pourrez parler de visions complémentaires, d’angles d’étude, d’expériences vécues différentes
- Comment vous êtes-vous décidé ? Quel a été votre mode de prise de décision ?
- Qui a eu un écart plus petit en B qu’en D ? Qu’est-ce qui a fait que votre groupe n’a pas su bénéficier de votre expertise ?

## Apprentissage

- Puissance de l’intelligence collective
- Leadership, leader et suiveur
- Parlé du leader et du suiveur, un leader sans suiveur n’aura aucun impact sur le groupe
- Vous pouvez même montrer cette vidéo (globalement dans tous les jeux de leadership https://www.youtube.com/watch?v=fW8amMCVAJQ) <https://www.youtube.com/watch?v=fW8amMCVAJQ)>

## Conseils


## Sources

- https://gooodpro.files.wordpress.com/2017/01/test-nasa.doc
