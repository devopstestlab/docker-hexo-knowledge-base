---
title: "Artist & Specifieur"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots-clés : Découvrir l’agilité et le Lean par le jeu
- Durée : 1h à 2h
- Nombre de participants : 6 à 12 (formez des groupes de 4-7)

## Speech de présentation

"Vous allez former un groupe de spécifieurs et d’artistes. Je vais fournir un dessin aux spécifieurs qui vont devoir le décrire textuellement. Les dessinateurs vont devoir reproduire le dessin initial uniquement à partir de votre spécification littéraire."

## Processus

- Formez les groupes de 4 à 7 personnes
- Dans le groupe demandez qui sont les artistes, qui sont les spécifieurs (pas de nombre prédéfini)
- Un spécifieur est aussi le messager qui portera les messages entre les spécifieurs et les artistes
- Les spécifieurs décrivent textuellement le dessin aux artistes pour qu’ils le dessinent.
- Les règles
  - Les spécifieurs travaillent en dehors de la pièce
  - Les spécifieur écrivent des instructions simples et non picturales aux artistes.
  - Le messager peut regarder, écouter quand il est avec les artistes, mais il n’a pas le droit de leur parler, d’indiquer et de leur émettre de l’information
  - Les artistes peuvent renvoyer des messages au messager selon les mêmes règles : textuel, pas de dessin. Pas de communication orale, gestuelle ou picturale.
- Process :
  - 5min d’organisation de l’équipe
  - 10mn de spécification + dessin. ATTENTION : à l’issue des 10mn, le dessin doit apparaître sur la feuille des artistes ! Assurez-vous bien que les participants ont compris ça
  - 5min d’introspection, comment faire mieux.
  - 10-15mn d’échange sur les idées de chaque groupe
- Refaire une itération
- À la dernière itération, proposer au groupe de se colocaliser, vous avez le droit de vous parler. Cependant les artistes n’ont pas le droit de voir le dessin.

## Feedback

- La plupart des éléments ressortiront d’eux-mêmes via les participants
- Que s’est-il passé? Qu’avez-vous observé?
- Comment vous sentez-vous ?
- Qu’est-ce qui a bien marché ?
- Qu’est-ce qui a moins bien marché ?
- Qu’avez-vous appris ?
- Qu’est-ce que cela peut changer pour votre équipe ?

## Apprentissage

- Faire l’expérience de l’incommunicabilité au sein d’une équipe;
- Illustrer les limites de la communication papier
- Comprendre que l’éloignement est problématique et diminue la fluidité de l’information;
- L’intérêt de la pratique de la rétrospective;
- La communication orale a aussi ses limites;

## Sources

- http://alistair.cockburn.us/The+Draw+the+Drawing+Game
- Sources de dessins : https://gooodpro.files.wordpress.com/2017/01/artist-and-specifiers.pdf
