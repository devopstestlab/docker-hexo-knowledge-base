---
title: "L'effet de réseau"
date: 2020-06-07 21:02:32
tags:
  - management
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

- L'effet de réseau repose sur la notion de point de basculement, qui est le moment où quelque chose de rare se répand soudainement dans toute une population. Cet effet est d'autant plus fort qu'il est favorisé par une multitude de nombreuses connexions faibles. C'est ainsi qu'un livre ou un film devient populaire.
- L'effet Matthieu fait que ce qui est populaire devient de plus en plus populaire. C'est ainsi que les informations qui sont les plus propagées sont encore plus propagées.
- L'effet d'homogénéisation des groupes sociaux correspond au mécanisme de "contagion", de partage de la culture, des modes et des tendances. C'est ainsi que de nombreuses personnes aiment et détestent les mêmes choses. Cet effet tend à disparaître après trois degrés dans les réseaux sociaux.
