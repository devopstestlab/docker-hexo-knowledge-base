---
title: "Arbre d’Ostende"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - ice breaker
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 15mn
- Nombre de participants : 3 à 15 (travail individuel)

## Speech de présentation

`Vous avez devant vous une représentation d’un arbre avec des personnages, choisissez un personnage qui représente le plus votre état d’esprit actuel et entourez-le. Je vous demanderai de partager avec le groupe ensuite.`

## Processus

- Laissez 3min au groupe pour se décider individuellement et partager ensuite avec le groupe
- Partage de groupe pour permettre à chacun de positionner son niveau de dialogue par rapport aux autres
- Vous pouvez refaire l’exercice en fin de réunion et demander ce qui a fait changer le positionnement ou au contraire, n’a rien changé

## Feedback


## Apprentissage


## Conseils


## Sources

- http://decouvesverte.free.fr/IMG/pdf/arbreostende.pdf
