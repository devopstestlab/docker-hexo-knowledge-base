---
title: "Post the Path"
date: 2020-06-07 21:02:32
tags:
- coaching
- games
- agile
- workshop
categories:
- [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Durée : 2 à 10
- Nombre de participants : 30 à 60 minutes

Ce jeu est très utile pour clarifier ce qui fait quoi et quand. Il est donc très utile quand il y a de la confusion. Il permet de clarifier ces zones de flou.

## Speech de présentation

Formuler l'objectif : `Ce jeu consiste à créer une image de la façon dont nous créons (résultat du processus)`. Le résultat du processus peut être un document, un produit, etc ...

## Processus

- Ecrire ou dessiner le résultat du processus sur le mur.
- Fixer un point de départ du processus avec le groupe ("le début de la journée", "après avoir terminé le dernier", ...) : c'est le déclencheur.
- Si le groupe a du mal à fixer le déclencheur, choisir un déclencheur et le proposer comme une hypothèse.
- L'écrire sur un post it.
- Le placer au mur.
- Demander au groupe de réfléchir au processus du début à la fin.
- Leur demander d'écrire les étapes du processus en ajoutant des post its.
- Leur demander d'approcher du mur.
- Leur faire coller au mur les post its.
- Faire placer par le groupe les étapes les unes au-dessus et au-dessous des autres pour les comparer.
- Inviter le groupe à trouver les points d'accord et de confusion.
- Identifier les problèmes de terminologie.

## Feedback


## Apprentissage


## Conseils

Chaque groupe détermine ses propres :
- significations des différentes étapes du processus,
- actions qui peuvent être faites;
- actions qui doivent être faites.

Si le groupe est important, il peut être opportun de ne pas faire lire les post its et de les faire afficher directement au mur.

Si le nombre d'étapes est trop important, limiter le nombre d'étapes.

## Sources

James Macanufo
