---
title: "Ballpoint Game"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots-clés : Découvrir l’agilité et le Lean par le jeu
- Durée : 15min à 2h
- Nombre de participants : 10 à 100 (formez des groupes de 20 max)

## Speech de présentation

"Le but du jeu va être de passer le plus de balles entre les participants en 2mn et en suivant quelques règles, on va faire plusieurs essais en essayant de s’améliorer."

## Processus

- Demandez au groupe de former deux lignes parallèles de même longueur (à 12, 6 d’un côté, 6 de l’autre qui se font face)
- Le groupe va devoir faire passer le plus de balles possible dans les mains de tous les mondes sachant que :
  - Entre chaque équipier les balles prennent l’air. (C’est à dire que l’on ne peut pas se passer la balle de main en main sans qu’à un moment la balle soit seule dans l’air).
  - Pas de passe à votre voisin direct. (pas de passe à votre voisin le plus proche).
  - Le point de départ est le point d’arrivée (la personne qui plonge la main dans le sac pour attraper une balle est aussi celle qui place les balles qui ont parcouru le système dans un autre sac) (le chef de produit, la MOA..).
  - Tous les membres de l’équipe doivent toucher la balle une seule fois strictement sauf le point de départ qui est aussi le point d’arrivée.
  - Une balle qui tombe, touche le sol, ou qui ne respecte pas ces règles est perdue.
- 5 itérations : Pour chaque itération :
  - 2mn Organisation : laissez le groupe se positionner, choisir le point de départ/arrivée
  - 30s Estimation : demandez au groupe combien de balles il pense faire passer dans les 2mn. Si vous êtes nombreux, demandez plusieurs estimations. Ne perdez pas de temps à ce niveau, actez vous-même si le groupe n’arrive pas à choisir. Notez sur un paperboard si vous en avez un
  - 2mn Itération : Chronométrez dans donner le temps, laissez le groupe faire.
  - Introspection rapide (1mn30) : laissez le groupe définir des idées d’amélioration, notez-les. Si vous êtes nombreux, aidez le groupe en actant des choses que vous avez entendues.

## Feedback

- Que s’est-il passé ?
- Quelle a été votre meilleure itération ?
- L’amélioration est venue de quoi? On a travaillé plus dur ? Plus vite ?
- L’auto-organisation de l’équipe a-t-elle bien fonctionné ?
- D’où est venu le "leadership" ? Avec quel style ?
- Que pensez-vous de vos estimations ? Qu’en tirez-vous sur la viabilité d’une estimation ?
- Application chez vous ?

## Apprentissage

Le débrief de l’atelier est très fortement associé à ce qu’il s’est passé réellement, soyez attentifs, regardez bien et voyez ce qui émerge. Souvent, les participants eux-mêmes amènent les informations.
Cependant :
- La performance passe par le système, pour améliorer la performance, il faut impacter le système. Améliorer les individus n’a qu’un impact limité.
- Attention aux “il faut mieux lancer la balle”, “il faut faire attention”... dans l’amélioration continue, car il n’y a aucun moyen de s’en assurer. Toujours aller chercher un comment déclenchable (SMART1).
- Le jeu illustre la différence entre performance individuelle et performance collective. L’expert n’a qu’un impact limité par rapport à la performance de groupe.
- Les estimations initiales sont souvent catastrophiques. Elles s’améliorent avec l’expérience.
- Chercher à régler un problème à la fois (selon la théorie des contraintes de Goldratt2)
- L’amélioration continue sur des cycles courts permet de diminuer le risque de l’erreur, on parle d’expérimentation
- On peut étudier le leadership et l’auto-organisation “aviez-vous absolument besoin que je vous dise comment faire pour vous améliorer ?”
- Si vous jouez avec une équipe de grande taille, demandez ce qu’il se passe et qu’est- ce qui se passerait si l’équipe était plus petite.
- On critique ce jeu comme une illustration du Taylorisme (effectivement, une partie des participants reste inactive dans les premiers temps de l’itération). Cependant rien ne les empêche de prendre l’initiative, de réfléchir à comment être moins passifs.

## Conseils

Variantes :
- Axer le jeu sur l’expérimentation de l’amélioration continue. En amont, construire un tableau de suivi des expérimentations, préparer des fiches avec comme indications : http://oyomy.fr/2017/01/le-ball-flow-game-revisite-a-lagile-tour-aix-marseille-2016
- À chaque phase d’introspection, laissez les groupes réfléchir à des idées d’améliorations et de les écrire (vous pouvez mettre des sous-groupes). (laissez 3mn pour réfléchir et noter), faites une courte phase de sélection.

## Sources

- http://www.areyouagile.com/2013/03/le-jeu-scrum-des-balles-et-des-points/ (variante très orientée Kanban)
- http://oyomy.fr/2017/01/le-ball-flow-game-revisite-a-lagile-tour-aix-marseille-2016
- https://fr.wikipedia.org/wiki/Objectifs_et_indicateurs_SMART
- https://en.wikipedia.org/wiki/Theory_of_constraints
