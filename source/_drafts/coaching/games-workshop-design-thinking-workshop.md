---
title: "Design Thinking Workshop"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Lean Startup et Design Thinking
- Durée : 1h30
- Nombre de participants : 2 à 60 (un nombre pair de participants est primordial)

## Speech de présentation

`Vous allez travailler en binôme et tenter d’améliorer l’expérience d’offrir des cadeaux, non pas d’améliorer un cadeau, mais l’expérience d’offrir.`

## Processus

- Fournir les supports aux participants (un feuillet par personne) et les stylos
- Donnez 2mn au groupe pour se remémorer individuellement la dernière fois qu’il a eu à offrir un cadeau. Depuis l’opportunité d’offrir au don du cadeau voir un petit peu  après. Cela servira d’expérience de base à améliorer
- Formez des binômes.
- Expliquer le jeu : “Vous allez travailler...” et ajouter “mais vous n’allez pas essayer d’améliorer votre expérience, vous allez essayer d’améliorer l’expérience de votre partenaire”
- Etapes
  - Étape 1 : Interview : Dans la phase d’interview, demandez aux participants d’écouter toute l’histoire du partenaire, de questionner avec des pourquoi pour comprendre les raisons qui l’ont poussé à agir d’une manière ou d’une autre, étudiez l’expérience, rentrez dans le sujet
  - Étape 2 : Creusez : Dans cette phase, allez chercher les éléments qui vous ont étonné, qui vous ont semblé importants pour le partenaire. Allez chercher les émotions, le rapport qu'entretient votre interlocuteur avec son expérience et la personne à qui il cherche d’offrir des cadeaux. Appuyez là où vous voyez quelque chose d’intéressant
  - Etape 3 : analysez les données :  À partir des deux phases d’interviews, notez ce qui est du ressort de la motivation, ce qu'essaye d’atteindre votre partenaire en offrant un cadeau (il veut faire plaisir, il souhaite de la reconnaissance, il veut aller chercher le bon cadeau pour être original...) Notez aussi ce qui est du ressort de l’émotion, les éléments notables de l’interview, des éléments qu’il faut absolument prendre en compte pour créer la meilleure expérience pour lui (il a besoin d’amour, offrir un cadeau est plus un geste ego centré que dirigé vers le receveur, il trouve que le fait main est plus authentique...)
  - Étape 4 : Définir la problématique : À partir des éléments notés, établir la problématique, votre sujet de travail. Qui est votre partenaire, quel est son besoin fondamental et que faut-il prendre en compte
  - Étape 5 : Imaginez des solutions : En dessin uniquement, imaginez des solutions à présenter à votre partenaire. À ce niveau vous êtes dans la quantité plus que la qualité, vous ne cherchez pas L’IDÉE, mais des pistes. Pensez à des solutions drastiquement différentes pour ouvrir les pistes. La qualité du dessin n’a pas d’importance, au contraire, cela peut donner de bons feed-back sur un malentendu
  - Étape 6 : Feed-back. Présentez vos dessins à votre interlocuteur, soyez le plus évasif possible pour laisser de la place à l’interprétation. Passez plus de temps à écouter qu’à parler, n’essayez surtout pas de vendre l’idée, vous voulez du feed-back, pas de l’adhésion. Variante proposée : montrez les dessins sans expliquer, laissez-le décrire ce qu’il voit et attendez que votre interlocuteur se pose des questions pour lui répondre. Pour celui/celle qui donne le feed-back, ne soyez pas “gentil”, exprimez ce que vous ressentez face à ces propositions, un mauvais commentaire est une piste d’amélioration
  - Étape 7 : Posez l'IDÉE. À partir de votre travail enrichi de feed-back, faites émerger la bonne idée, celle qui fera mouche
  - Étape 8 : Prototypez. Rendez tangible votre concept, créez quelque chose qui rassemble toutes vos idées, soyez créatifs
  - Étape 9 et dernière étape : Feed-back sur le prototype
- Montrez votre prototype et attendez les commentaires et questions. Remplissez le canevas de feed-back pour voir ce qui fonctionne bien et ce qui ne fonctionne pas.

## Feedback

- En quoi échanger avec une vraie personne, tester avec une vraie personne, change la direction que votre prototype a prise
- Qu’est-ce que ça vous a fait de montrer un travail non terminé ?
- Comment avez-vous senti le rythme ? Rapide, itératif, comment avez-vous ressenti ça par rapport à votre fonctionnement habituel ?
- Design Thinking est un processus itératif. En vous basant sur ce que vous avez appris, que feriez-vous différemment ? Quelle étape referiez-vous ?
- Quels principes, concepts vont imprégner votre travail demain ?
- Vous pouvez demandez à un binôme d’illustrer tout le process avec leur expérience (cela peut prendre du temps)
- (vous pouvez) Regrouper tous les prototypes au centre de la salle et prendre le temps d’admirer et célébrer
- Qui a construit quelque chose que son partenaire a particulièrement apprécié ?
- Qui est curieux d’en savoir plus sur le prototype de son partenaire ?
- Qu’est-ce qui vous a semblé le plus inconfortable ?Ou, au contraire le plus naturel ?

## Apprentissage

- Le Design Thinking est une méthode puissante pour aller chercher les besoins fondamentaux, non exprimés de vos clients grâce à l’empathie, l’écoute...
- Aller sur le terrain est essentiel pour avoir une information non biaisée
- Ne demandez pas aux prospects ce qu’ils veulent, discutez avec eux et essayez de comprendre
- Testez autant que possible

## Conseils


## Sources

- Avant l’atelier, regardez la vidéo complète proposée par la d.school (http://dschool.stanford.edu/crash-course-video)
- Apprenez en plus sur le Design Thinking : https://dschool.stanford.edu/sandbox/groups/designresources/wiki/31fbd/attachments/acf2a/METHODCARDS_FRENCH_March_2014_m.pdf?sessionID=e62aa8294d323f1b1540d3ee21e961cf7d1bce38
- Support : http://dschool.stanford.edu/wp-content/uploads/2012/02/Participant-Worksheet.pdf
- http://dschool.stanford.edu/dgift
- http://www.slideshare.net/GregoryAlexandre/atelier-design-thinking-au-cara-lyon
- http://dschool.stanford.edu/wp-content/uploads/2012/02/crashcourseplaybookfinal3-1-120302015105-phpapp02.pdf
- http://dschool.stanford.edu/wp-content/uploads/2012/02/Participant-Worksheet.pdf
- http://dschool.stanford.edu/crash-course-video
