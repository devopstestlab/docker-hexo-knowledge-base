---
title: "WOOSH"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 10min à 30mn
- Nombre de participants : 8 à 50

## Speech de présentation

`Vous allez vous mettre en cercle et faire passer une boule d'énergie dans le groupe le plus rapidement possible.`

## Processus

- Former un cercle avec vous-même et les participants
- Expliquer que vous disposez dans vos mains d’un flux d’énergie qui doit se déplacer dans le cercle. Pour passer le flux d’énergie à son voisin, il faut crier “WOOSH” et mimer l’envoi d’un flux d’énergie puissant Essayez plusieurs fois jusqu’à ce que le flux soit passé dans les mains de tout le monde dans le sens horaire et antihoraire
-  accélérer de manière à augmenter un peu la difficulté.
- Une fois que chacun maîtrise bien le “WOOSH”, ajouter le “BLOCK”. Le “BLOCK” arrête le flux d’énergie et le fait repartir dans l’autre sens, testez plusieurs fois et ajoutez ensuite autant de bruits farfelus que vous voulez comme un “BOING” qui saute la personne à côté de vous et envoie le flux à la personne d’après ou le “WAAAA” qui vous permet d’envoyer le flux à qui vous voulez.
- C’est un excellent exercice pour voir qui s’engage et qui reste en retrait qui aime mettre le bazar et qui est plutôt exubérant.
- Ce jeu à lui tout seul peut vous donner beaucoup d’information sur un groupe et comme les membres du groupe peuvent travailler ensemble.
- C’est aussi un bon moyen de focaliser le groupe tout en l’énergisant.

## Feedback


## Apprentissage


## Conseils


## Sources

- https://www.youtube.com/watch?v=ZxwXhjg4D_U
