---
title: "Systémie"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 2min à 5mn
- Nombre de participants : 10 à 50

## Speech de présentation

`On va vivre une expérience de systémie.`

## Processus

- Demandez au groupe de se positionner en cercle
- Demandez à chaque personne de pointer avec son bras gauche une personne en face d’eux
- Demandez à chaque personne de pointer avec son bras droit une autre personne dans le cercle, peu importe où elle est
- Demandez au groupe de former un triangle isocèle (la distance qui me sépare de mes deux cibles doit être égale) avec ses deux cibles
- Le but du groupe est se stabiliser et rester immobile, laissez le groupe se stabiliser
- Avec un peu de chance, le groupe ne se stabilisera jamais, effet systémique et démonstration de la complexité à chaque fois que je réussis à faire un triangle, cela oblige quelqu’un dans le groupe à se réadapter ce qui aura aussi un impact sur mes cibles qui vont se déplacer et donc m’obliger à me redéplacer et ainsi de suite.
- Questions à poser :
  - Que venez-vous de vivre ?
  - Que s’est-il passé ?
  - Avez-vous réussi à vous stabiliser ?
  - Pourquoi est-il difficile de rendre le groupe immobile ?

## Feedback


## Apprentissage

- Plus le groupe est grand plus il est difficile de se stabiliser, au contraire, un petit groupe trouvera rapidement un moyen de se stabiliser
- Mes actions ont un impact global sur le système et surtout beaucoup d’impacts cachés. Chercher à comprendre le système peut le permettre de le stabiliser.

## Conseils


## Sources
