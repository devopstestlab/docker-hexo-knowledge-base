---
title: "My Worst Nightmare (Innovation)"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Jeux pour faire
- Durée : 30 mn
- Nombre de participants : 3 à 15

## Speech de présentation

`Imaginez le pire qui pourrait arriver sur votre projet, imaginez la pire forme que pourrait prendre votre produit, imaginez le pire chef de projet, imaginez la pire équipe... à quoi cela ressemble-t-il ?`

## Processus

- Posez le sujet avec les participants
- Poussez-les à imaginer les pires éléments possible, à se lâcher. Invitez-les à dessiner la forme que cela pourrait prendre. Si ce cauchemar était un monstre, à quoi ressemblerait-il ?

## Feedback

- Quelles peurs avez-vous fait émerger ? De quoi sont-elles représentatives ?
- Que faudrait-il faire pour ne pas arriver à ça (enchaîner avec un atelier de gestion des risques)

## Apprentissage


## Conseils


## Sources

- http://www.innovationgames.com/my-worst-nightmare
