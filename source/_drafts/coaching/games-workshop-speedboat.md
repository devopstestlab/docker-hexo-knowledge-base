---
title: "Speedboat (résolution de problème)"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Jeux pour faire
- Durée : 1 h
- Nombre de participants : 3 à 9

## Speech de présentation

```
Vous êtes l’équipage d’un bateau et souhaitez vous rendre vers une île au trésor, votre but ultime. Votre bateau dispose de voile qui lui permettent d’avancer plus vite, d’ancre qui le freinent et vous bénéficiez de bons coups de vent pour accélérer et devez faire attention aux rochers qui pourraient vous couler
- Bateau : représentation du nous (le projet, l’équipe, le produit)
- Ile : notre objectif, vers quoi souhaitons-nous aller
- Voiles : permettre d’avancer, ce sont les forces de l’équipe, du projet
- Ancres : elles freinent le bateau, l’empêchent d’aller vers l’île, mais elles pourraient être levées. Permet d’identifier des chantiers de travail
- Vents : éléments sur lesquelles nous n’avons pas d’influence, mais que nous devrions intercepter pour faire avancer plus vite notre bateau
- Rochers : éléments de blocage que nous ne pouvons pas modifier, il faut donc trouver un moyen de les contourner.
```

## Processus

- Valider avec le groupe ce que représente le bateau
- Valider avec le groupe ce que représente l’île, exprimer la représentation de manière positive
- Fournir des post-its individuels et les laisser faire des propositions pour chacun des 4 axes (donnez 10mn)
- Faire une présentation des posts-its
- Faire des regroupements si nécessaire
- Faites un dot voting (trois traits par personne à mettre sur des post-its) pour choisir les sujets à traiter
- Faire un atelier de résolution de problème (attaquer la cause racine avec les 5 POURQUOI, identifier vers quoi vous souhaiteriez aller, identifier les actions pour faire +1 vers l’objectif...)

## Feedback

Il existe plusieurs interprétations des éléments de l'atelier, à vous de trouver votre interprétation.

## Apprentissage


## Conseils


## Sources

- http://www.innovationgames.com/speed-boat
