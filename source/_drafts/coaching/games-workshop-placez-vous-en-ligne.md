---
title: "Placez-vous en ligne"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - ice breaker
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 5min à illimité
- Nombre de participants : 3 à 50+

## Speech de présentation

`On va se chauffer un peu, je vais vous donner une série de demandes et pour chaque vous allez vous positionner en ligne les uns par rapport aux autres.`

## Processus

- Exemples de demandes :
  - Pour chaque demande, indiquer le positionnement du minimum et du maximum. (ex : le A est de ce côté de la salle, le Z est de l’autre côté)
  - Mettez-vous par ordre alphabétique du prénom
  - Mettez-vous par ordre de distance pour arriver ce matin
  - Mettez-vous par distance la plus longue faite pour aller voyager
  - Ordonnez-vous par jour et mois d’anniversaire
  - Ordonnez-vous par clarté des yeux ●...
  - Le jeu gagne en puissance dès lors que vous alternez des questions “fun” et des questions ayant attrait au contexte
- Exemples de questions :
  - Mettez-vous par ordre d’arrivée sur le projet
  - Positionnez-vous par rapport à votre degré de confiance de 0 à 10 sur la réussite du projet
    ...
- Invitez chaque participant à clarifier sa position (donner son prénom permettra au groupe de le retenir, donner la distance de voyage permettra de partager une anecdote, se positionner par ordre d’arrivée du projet permettra de positionner les degrés de connaissance de l’historique du projet)

## Feedback


## Apprentissage


## Conseils

Variantes :
- Demander au groupe de se regrouper
  - Vous êtes plutôt thé, café ou chocolat ?
  - Regroupez-vous par service dans l’entreprise
  - Crayon, feutre ou pinceau ?
  - Regroupez-vous par couleurs d’yeux

## Sources
