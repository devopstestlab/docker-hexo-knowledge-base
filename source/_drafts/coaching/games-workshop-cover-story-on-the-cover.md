---
title: "Cover Story/On the Cover (Innovation)"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Jeux pour faire
- Durée : 1h30-2h
- Nombre de participants : 3 à 15

## Speech de présentation

`Votre produit vient de sortir et fait la une d’un magazine, qu’en dit-il ?`

## Processus

- Présentez le template aux participants
  - Donnez un nom à votre produit “XXX ON THE COVER !”
  - Couverture : imaginez le magazine dans lequel vous souhaitez être publié, une belle image de couv’ et le gros titre
  - Brain storms : notez toutes les idées, informations qui vous passent par la tête
  - Quotes : quels verbatims aimeriez vous voir dans le magazine, qu’est-ce que les gens disent de votre produit ?
  - Big headlines : atouts majeurs de votre produit en gros titre avec un très court descriptif
  - Images : visuels associés
  - Sidebars : description rapide des fonctionnalités
- Laissez à l’équipe 30mn pour poser les éléments
- Présentation du résultat : 5mn

## Feedback

- Qu’avez-vous fait émerger de primordial ? Quelles réelles découvertes ont émergé ?
- Pourquoi avoir choisi ce magazine ?
- En quoi les visuels choisis traduisent-ils l’âme de votre produit ?

## Apprentissage


## Conseils


## Sources

- http://gamestorming.com/games-for-design/cover-story
