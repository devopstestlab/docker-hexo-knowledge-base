---
title: "Jeopardy (résolution de problème)"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Jeux pour faire
- Durée : 1 h
- Nombre de participants : 3 à 9

## Speech de présentation

`Nous allons faire un atelier dans lequel je vais vous demander de penser à des questions sur le projet dont vous avez la réponse. Gardez la question pour vous et notez la réponse sur un post-it (ex : “Qui est le meilleur développeur de l’équipe ? Marie. Notez Marie sur un post-it et souvenez-vous de la question ou notez-la pour vous”)`

## Processus

- Chaque équipier réfléchit individuellement à une ou plusieurs questions (qu’il garde secrètement pour lui) et note la réponse sur un post-it (toujours suivant le principe : une idée = un post-it).
- Toutes les réponses sont affichées au tableau pour former un tableau de jeu.
- Chaque équipier est invité à imaginer les questions aux réponses qui ne sont pas les siennes
- Toutes les questions sont notées à côté des réponses (à ce moment-là, généralement, l’auteur de la réponse a une réaction),
- Quand les équipiers sont à court de questions et que toutes les réponses ont été couvertes, les auteurs révèlent leurs questions originales,
- C’est le moment des réactions : surprises, découvertes, hallucinations, éclats de rire, incompréhensions, quiproquos, parfois déceptions (ça arrive),
- Les équipiers votent pour les questions qu’ils estiment les plus importantes (par exemple en utilisant la technique du Dot voting),
- L’équipe décide à minima d’une action réalisable d’ici à la prochaine rétrospective.

## Feedback


## Apprentissage


## Conseils


## Sources

- http://talondagile.fr/2013/02/un-oeil-dans-la-retro-33-jeopardy
