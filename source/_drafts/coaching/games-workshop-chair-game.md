---
title: "Chair Game"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Collectif, confiance et leadership
- Durée : 20mn
- Nombre de participants : 9 à 21

## Speech de présentation

`Je vais vous fournir des objectifs de groupe secrets concernant le positionnement des chaises et votre but va être de remplir votre objectif`

## Processus

- Préparation : Positionner un grand nombre de chaises (non corrélé avec la quantité des participants) dans la pièce
- Formez trois groupes et fournissez-leur les indications secrètes suivantes :
  - “Votre objectif est que toutes les chaises soient collées entre elles”
  - “Votre objectif est que toutes les chaises soient en cercle”
  - “Votre objectif est que toutes les chaises aient les pieds en l’air”
- Chaque groupe va devoir remplir son objectif sans jamais parler (entre eux ou avec les autres groupes)
- Lâchez-les et chronométrez
- Une fois que tous les groupes en remplis leur objectif stoppez le chrono

## Feedback

- Comment ça va ?
- Quels étaient vos objectifs respectifs ?
- Comment l’avez-vous rempli ? À quel moment vous êtes-vous rendu compte que les autres groupes avaient un objectif compatible ? Comment vous en êtes-vous rendu compte ?
- Avez-vous déjà vécu une situation où aviez le sentiment de travailler avec un autre groupe de personnes et que vos objectifs n’étaient pas les mêmes ? Qu’avez-vous ressenti ? Qu’auriez-vous pu faire ?
- En réalité, les trois objectifs sont compatibles, mais la première chose que se disent les participants est que les objectifs ne sont pas compatibles (compétition) jusqu’à ce qu’une ou plusieurs personnes regardent ce que font les autres groupes et se rendent compte de l’objectif des autres.

## Apprentissage

- Auto-organisation
- S’intéresser à l’autre, lever la tête de son contexte personnel
- Illustration d’une situation client/fournisseur (le client veut le meilleur produit au moindre coût, le fournisseur veut marger)

## Conseils


## Sources
