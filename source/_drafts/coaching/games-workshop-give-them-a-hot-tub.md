---
title: "Give Them a Hot Tub"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 10mn
- Nombre de participants : 1 à 20

## Speech de présentation

`On va faire chauffer les cerveaux, je vais vous demander d’imaginer le nouvel usage que pourraient avoir deux objets de la vie courante combinés.`

## Processus

- Construire un jeu de cartes ayant comme inscriptions des objets :
  - Lave-linge
  - Fourchette
  - Tondeuse à gazon
  - Miroir
  - Toilettes
  - ...
- Construire deux pioches de volume égal, présentées faces cachées
- Demander à chaque participant de piocher exactement au même moment une carte de chaque pioche (Fourchette/Miroir, Tondeuse/Lave Linge) et de donner un usage à un nouveau produit innovant formé par la combinaison des deux objets (la fourchette miroir vous permet de vous recoiffer en mangeant, la tondeuse lave linge rend lave à la perfection tous vos vêtements verts...) pousser les joueurs à vendre leur produit et être convaincu des vertus de l’usage.

## Feedback


## Apprentissage


## Conseils


## Sources

- http://www.innovationgames.com/hot-tub
