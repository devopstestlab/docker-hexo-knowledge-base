---
title: "Passe le Clap"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - warm up
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 10mn
- Nombre de participants : 8 à 50

## Speech de présentation

`Le but du jeu va être de vous passer des claps (faire le geste) le plus rapidement possible.`

## Processus

- Mettre les participants en cercle
- Pour se passer le clap, s’assurer d’avoir un contact visuel avec le destinataire et lui envoyer le clap (le clap est un applaudissement unique orienté vers le receveur)
- Laisser les participants se passer le clap de plus en plus vite tout en s’assurant tout le temps qu’il y ait bien eu un contact visuel et que le clap est bien reçu par la bonne personne

## Feedback

- Parler de la communication, êtes-vous sûr que votre interlocuteur a bien compris que vous lui envoyiez le Clap ? Avez-vous, à un moment envoyé un clap que vous n’aviez pas reçu ? Avez- vous entendu deux claps se faire en même temps ? Pourquoi ?

## Apprentissage


## Conseils

Variante :
- Mettre le groupe en mouvement dans toute la salle et s'envoyer le clap par-dessus les autres, en arrière, entre les jambes...
- Passe le Clap est un jeu très simple à expliquer et permet d’énergiser un groupe très rapidement

## Sources

- https://www.youtube.com/watch?v=eUP1a-9q2aA
