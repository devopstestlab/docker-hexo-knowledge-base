---
title: "Banana Game (résolution de problème)"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Jeux pour faire
- Durée : 1h
- Nombre de participants : 3 à 9

## Speech de présentation

"Prenez chacun une banane et dessinez un avatar qui représente votre état d’esprit en ce moment dans l’équipe."

## Processus


## Feedback


## Apprentissage


## Conseils


## Sources

- http://fr.slideshare.net/coactiv/retrouvez-la-banane-en-rtrospective
