---
title: "Image-ination"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 15 à 60 minutes
- Nombre de participants : 5–7 par groupe

## Speech de présentation


## Processus

- Avant la réunion, découpes des images variées ne contiennant pas de mots dans des magazines ou des catalogues.
- Rassembler 3 à 5 photos par personne.
- Accrocher une grande feuille de papier sur la table.
- Au centre, décrire le sujet en un à trois mots (exemple : "trouver de nouveaux clients").
- Placer les images face vers le bas au bord du papier.
- Donner à chaque personne une pile de post its.
- Montrer une image et indiquer plusieurs façons dont elle se rapporte au sujet.
- 5 minutes : Demander à chaque participant de :
  - choisir une image au hasard,
  - la retourner,
  - écrire sur les post its un maximum d'idées sur la façon dont l'image se rapporte au sujet,
  - mettre une idée sur chaque post it,
  - la reporter sur le tableau de papier autour du sujet.
- Répéter jusqu'à ce qu'il n'y ait plus assez d'image ou de temps.
- Demander de rassembler les post its avec toutes les idées.
- Demander de réorganiser les idées en groupes.
- Pour chaque groupe :
  - demander au groupe de sélecionner une image d'illustration de l'idée,
  - lui donner un titre court,
  - écrire le titre sous l'image.
- Faire partager les images et les titres.
- Discuter de la manière dont les images titrées peuvent informer les groupes sur le sujet.
- Faire une liste des actions qui pourraient être mises en place pour mettre en oeuvre ces idées.

## Feedback


## Apprentissage


## Conseils

- Les images permettent de susciter des idées et de créer de nouvelles associations.
- Ce jeu utilise à la fois les compétences visuelles et verbales.
- L'objectif n'est pas de trouver un concept spécifique.
- Les délais courts permettent d'obliger aux gens de moins réfléchir et ainsi de faire émerger quelque chose de différent.
- Le fait que les participants de trouvent pas l'image qu'ils veulent pour décrire leur idée leur permet de trouver un autre type d'association.

## Sources

Picture This !, adapté de Visual Icebreaker, de VisualsSpeak (© 2010 VisualsSpeak LLC)
