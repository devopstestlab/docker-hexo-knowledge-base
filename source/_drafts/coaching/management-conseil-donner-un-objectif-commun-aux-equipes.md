---
title: "Donner un objectif commun aux équipes"
date: 2020-06-07 21:02:32
tags:
  - management
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---


Le manager devrait définir un but, un objectif commun, pour les membres de l'équipe afin de les unir et de les motiver. Ce doit à la fois faire rêver et être réalisable. C'est un besoin très important des équipes. Il permet aussi de faire prendre conscience du contexte.
