---
title: "Jeu des prénoms revisités"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots-clés : Découvrir l’agilité et le Lean par le jeu
- Durée : 1h30
- Nombre de participants : 6 à 24 (formez des groupes impairs si possible de 6 à 8)

## Speech de présentation

`Vous allez former un/des groupe(s) dont un écrivain et des clients, le but des clients et que l’écrivain note leur nom le plus vite possible`

## Processus

- Formez le(s) groupe(s)
  - Un écrivain dont la seule capacité est de savoir écrire
  - Des clients qui vont commander un prénom à leur écrivain
- Demandez, selon eux, combien de temps il faut pour écrire un prénom, notez. Si le groupe répond “ça dépend”, guidez le à donner une estimation moyenne.
- Demandez, selon eux, combien de temps il faut pour écrire [le nombre de clients] prénoms, notez.
- Quels sont les facteurs qui influent sur ce temps, listez-les (la plupart du temps, le multitâche n'apparaîtra pas)
- Les clients vont prendre chacun un bout de papier, écrire un prénom (pas nécessairement le leur) et garder de l’espace pour noter le temps qu’il aura fallu à l’écrivain pour leur livrer leur prénom
- Phase 1 :
  - En tant que client, votre job est de donner votre prénom à l’écrivain et son job est de vous livrer un papier avec votre prénom.
  - La politique de l’entreprise de l’écrivain est de ne jamais faire attendre un client, nous souhaitons maintenir l’image de l’entreprise au top ! En plus, plus vite on commence plus vite on finit hein ?!
  - Donc, pour remplir correctement cette politique, vous devez exécuter tous les projets en même temps.
  - Comment cela va se passer : les clients vont se mettre les uns derrière les autres et, à tour de rôle, donner lettre par lettre leur prénom. L’écrivain va donc ajouter la première lettre de chaque prénom avant de passer à la deuxième avant de passer à la troisième...
  - Ex : 3 clients (Laura, Michel et Donna) pour un écrivain.
  - Laura arrive près de l’écrivain et lui dit “je m’appelle Laura L”, puis Michel arrive “Michel M” puis “Donna D” puis Laura revient “A”, puis Michel “I” puis Donna “O”, Laura “U”.... Normalement, la première personne à avoir son prénom terminé sera Laura. Quand l’écrivain a terminé d’écrire son prénom, il lui donne la feuille marquée et Laura note le temps indiqué sur son chronomètre ou le chronomètre de l’animateur. Idem pour Michel et Donna lorsque leur prénom apparaît.
  - Demandez au groupe de remplir leurs indicateur
  - Indiquer les prénoms en ligne (dans l’ordre de passage) et tracez une ligne de 0 à [temps noté par les clients pour leur prénom]
  - Notez sur le tableau d’estimation le temps moyen d’écriture réel d’un prénom et le temps d’écriture total des prénoms.
  - Revenez vers la liste des raisons qui influent sur le temps d’écriture et demander au groupe de pointer ceux qui ont influencé cette première phase (“Vos prénoms étaient-ils tous incroyablement longs ?”, “l’écrivain écrivait-il si lentement que ça ?”...)
  - Normalement, après une seconde ou deux de réflexion, le groupe fera émerger le fait que l’écrivain écrivait tous les prénoms en même temps. L’animateur ajoute ça au tableau.
- Phase 2
  - Faites tourner de table les développeurs
  - Dites aux clients qu’ils travaillent désormais avec une entreprise qui a une politique totalement différente. Leur politique est “limiter le travail en cours” et leur limite est aujourd’hui à 1.
  - Cela se traduit par le fait que le développeur traite les clients 1 par 1 et ne répond à la demande d’un client que s’il a totalement terminé avec le précédent. C’est donc l’écrivain qui est en charge de son flux, personne ne viendra le charger, c’est à lui qui tire les projets clients contrairement à tout à l’heure où c’étaient les clients qui lui poussaient leur demande projet.
  - Désormais les clients vont devoir épeler la totalité de leur prénom, que l’écrivain va noter avant qu’il ne prenne le client suivant.
  - Ex. : 3 clients (Laura, Michel et Donna) pour un écrivain (pas le même que tout à l’heure. “Laura L A U R A”, “Michel M I C H E L”, “Donna D O N N A”.
  - De la même manière, chaque client note le temps auquel il a réceptionné son prénom. Lorsque tout le monde est passé, le groupe donne à l’animateur le temps moyen et le temps total. Il trace aussi sa deuxième carte de visualisation.

## Feedback

- Comment vous êtes-vous senti pendant ces deux phases en tant qu’écrivain et en tant que client ?
- Qu’a ressenti le dernier client à la phase 2 par rapport à la phase 1 ?
- Que pensez-vous de l’affirmation “si on commence tôt, on finit tôt ?”
- De quelle manière cela influence le planning de mise en production ? Par exemple, que savons-nous au bout de 10 secondes sur la phase 1 et sur la phase 2 ? (s’appuyer du graphique dessiné)

## Apprentissage

- On se rend compte qu’à la première phase, au bout de 10s, on n’a aucune information sur notre capacité à livrer un prénom. À la phase deux, au bout de 10s, on a probablement déjà livré 2 prénoms et avons des informations quant à la capacité de production et date de livraison des prénoms suivants
- Que serait-il arrivé si le développement était expert en task switching (changement de tâche) ? Si le coût du temps de switch avait été gratuit, l’écrivain aurait probablement gagné du temps. Cependant, et quelle que soit la performance en task switching de l’écrivain, le temps de réalisation d’un prénom dans la phase 1 aurait quand même pris beaucoup plus de temps qu’en phase deux. Cela est dû à la Loi de Little3 qui dit que si vous faites X choses simultanément, alors chaque élément prendra X fois plus de temps.
- Comment cela influence-t-il la qualité produit ? Si l’écrivain a fait une faute d’orthographe, le feed-back sera donné lorsque le client récupère son produit. Appuyez-vous sur le temps de livraison en phase 1 et phase 2. Vous pouvez en tirer des apprentissages sur le temps de feed-back (feedback loop). De plus, le temps gagné sur le task switching pourra être utilisé pour corriger les erreurs.
- Est-ce que les problèmes de multi-tâches vous semblent familiers ? Qui vit ça en ce moment ? Qui l’a déjà vécu par le passé ?
- Qu’est-ce qui fait que nous faisons ça ? Pourquoi ce problème est-il aussi fréquent ?
- Quand est-ce que le multitâche est une bonne idée ? Qui en a bénéficié à la phase 1 ?
  - Ce n’est quasiment jamais une bonne idée. Cela peut l’être quand le projet A est totalement bloqué. Il sera nécessaire de passer sur le projet B pour ne pas perdre trop de temps, mais il faut cependant limiter cette pratique. Si plus de projets sont bloqués, au lieu de passer sur le projet D, mieux vaut prendre du temps pour supprimer le plus rapidement possible les blocages des projets A, B et C. Cela permet aussi d’illustrer la notion de WIP limit (Work In Progress Limit), indicateur sur le nombre de tâches à mener simultanément. Le WIP limit est un concept omniprésent dans le Kanban. Ici la limite était à 1, mais doit, bien sûr, être étudiée par rapport au système.
- Que pourriez-vous faire pour combattre ce problème dans votre environnement ? Rendre visible le problème, mettre en place des indicateurs

## Conseils

Variantes : Jeu des prénoms revisités d’Alfred Almendra (https://docs.google.com/presentation/d/1uBki-6ovuVelwSCJtINx-AiSG-)

## Sources

- https://docs.google.com/presentation/d/1uBki-6ovuVelwSCJtINx-AiSG-Mcyakbow0SD8qbTpE/edit#slide=id.g8e679a533_00 (slides 45, 48, 49 et 50)
- https://dl.dropboxusercontent.com/u/1018963/Multitasking-Name-Game/Multitasking-Name-Game.pdf
- http://www.wikilean.com/Articles/Le-Juste-A-Temps/6-La-Theorie-des-contraintes-7-articles/La-Loi-de-Little
