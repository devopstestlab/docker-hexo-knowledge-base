---
title: "Le Pitch"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Durée : 30 à 90 minutes
- Nombre de participants : 4 à 12

Ce jeu permet de ramener l'attention sur le monde réel et se concentrer sur les aspects réalisables et viables des concepts.
L'idée est de s'imaginer entrepreneurs et de devoir vendre son idée à un groupe d'investisseurs (capital risque).

## Speech de présentation


## Processus

- Former de petits groupes de 2 à 3 personnes.
- Désigner un groupe pour jouer le rôle de l'investisseur; les autres jouent le rôle des entrepreneurs.
- Faire définir et approuver par le groupe un produit ou un service.
- Répartir les groupes dans des salles différentes.
- Chaque groupe formule son argumentaire pour le présenter aux investisseurs (10 minutes).
- Faire désigner au sein de chaque groupe un ou deux représentants qui vont présenter verbalement le pitch aux investisseurs. Le groupe entier répondra aux questions.
- Au bout de 8 minutes, demander aux investisseurs d'informer les entrepreneurs : "Il vous reste 2 minutes de préparation".
- Chaque groupe présente son argumentaire (3 minutes).
- Chaque investisseur peut poser jusqu'à deux questions.
- Eventuellement, chaque investisseur peut désigner lequel gagne.

## Feedback


## Apprentissage


## Conseils

- Ce jeu permet de saisir les différentes perspectives que les différents groupes ont sur un produit, un service ou un concept.
- Il permet de d'obliger à se concentrer sur les idées vraiment importantes.
- La limite de temps aide à se concentrer sur le coeur du sujet.
- La variété de groupes permet de mettre l'accent sur différents aspects.
- Les questions des investisseurs mettent en évidence les faiblesses ou aident à clarifier les idées.

## Sources

- Sarah Rink
