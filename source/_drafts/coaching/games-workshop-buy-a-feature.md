---
title: "Buy a feature (résolution de problème)"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Jeux pour faire
- Durée : 1 h
- Nombre de participants : 3 à 9

## Speech de présentation

`Vous disposez ici de toutes les fonctionnalités que vous avez jugé importantes pour cette version. Afin de savoir par lesquelles commencer, je vais vous fournir un budget qui, malheureusement, ne permettra pas d’acheter toutes les fonctionnalités. Ce budget sera réparti entre vous et vous devrez vous mettre d’accord pour acheter des fonctionnalités.`

## Processus

- Préparation :
  - imprimer de faux billets
  - imprimer des fiches comprenant le titre de la fonctionnalité, un résumé rapide et une estimation de son coût de réalisation (le but n’est pas d’être très précis, mais de travailler en ordre de valeur) Faire en sorte que le montant en faux billets permette de n’acheter qu’un tiers du montant total de toutes les fonctionnalités
- Disposez toutes les cartes sur une table (vous pouvez les ordonner par niveau de coût, par ordre alphabétique, pas pan applicatif)
- Répartissez le budget en faux billet en parts égales entre les participants
- Leur expliquer le fonctionnement
- Les laisser discuter, argumenter jusqu’à ce qu’ils aient acheté les fonctionnalités qui leur semblent primordiales
- Prendre acte

## Feedback

- Comment avez-vous vécu l’exercice ?
- Que se serait-il passé si l’on vous avait donné de quoi tout acheter ? Comme cela se serait matérialisé sur le projet ?
- D’après vous, pourquoi souhaitons-nous fonctionner comme cela ? Cela vous semble- t-il anormal ?
- Seriez-vous partants pour participer à ce type d’atelier à nouveau dans la suite du(des) projet(s) ?

## Apprentissage


## Conseils


## Sources
