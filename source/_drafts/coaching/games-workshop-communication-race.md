---
title: "Communication Race"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Collectif, confiance et leadership
- Durée : 10-20mn
- Nombre de participants : 4 à 16 (un minimum de 4 personnes par équipe)

## Speech de présentation

`Vous êtes tous aux commandes d’une formule 1 qui doit effectuer le meilleur trajet possible sur un circuit`

## Processus

- Préparation des supports :
- Découpez un carré dans un carton
- Faire un trou au centre du carton et faites traverser un gros feutre (scotchez-le de manière à ce que le feutre tienne parfaitement)
- Faites jusqu’à 12 trous tout autour du carton, positionnez de façon régulière et accrochez pour chaque trou une ficelle
- Tracez sur une feuille de paperboard un circuit simple (une route en boucle)
- Tendez votre formule 1 aux participants qui choisiront une ficelle comme ils le souhaitent
- Expliquez les règles aux participants : Ils doivent réussir effectuer un tracé avec le feutre central en tirant sur les ficelles pour faire avancer la formule 1. L’équipe n’a pas le droit de regarder sous le carton pour suivre le tracé Le tracé de feutre doit rester sur la route et ne doit jamais avoir de discontinuité
- Retirez le bouchon du feutre (c’est important) et positionnez-le sur la ligne de départ
- GO
- Regardez le tracé et recommencez si vous voyez que la ligne est sortie du circuit ou
- si vous voyez des trous dans le tracé
- Sinon faites autant d’itérations que souhaité et complexifiez le tracé du circuit

## Feedback

- Observez les comportements de l’équipe.
- Comment avez-vous réussi à faire un tracé parfait ?
- Y avait-il un leader ? Comment avez-vous ressenti le leadership ?
- Que se serait-il passé s’il n’y avait pas eu de leader ou si vous aviez tous été leaders ?
- Que se serait-il passé si vous n’aviez pas parlé ? (vous pouvez d’ailleurs tenter la première itération sur ce mode)

## Apprentissage

- Notion de leadership, qui est celui qui guide, qui sont les suiveurs et comment ça se démontre
- L’importance de la communication

## Conseils


## Sources

- http://tastycupcakes.org/2013/04/communication-race
