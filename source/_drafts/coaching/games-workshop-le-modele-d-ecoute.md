---
title: "Le modèle d'écoute"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 
- Nombre de participants : 

En moyenne, les gens n'utilise qu'un quart de leur capacité d'écoute. Le reste est souvent utilisé à d’autres choses.

Ce modèle incite à écouter sans jugement et sans préjugés. Il repose essentiellement sur la réflexion. L’idée est de vérifier ce que la personne vous a dit et de vérifier votre compréhension.

Ce modèle peut être utilisé quand une autre personne vous parle. Ce modèle demande beaucoup d'énergie.

## Speech de présentation


## Processus

- Ecouter activement.
- Etablir un contact visuel afin de maintenir un bon rapport.
- Si le contact visuel est inconfortable, fixer le point situé entre les sourcils de la personne.
- Utiliser des encouragements minimaux variés et bien synchronisés ("Aha", "Bien", "Oui", "Ok ») pour montrer son intérêt.
- Au moment opportun, donner un feedback sur ce que vous avez entendu sans rien interpréter ("Alors, ce que vous dites est… »).
- Eventuellement, faites un résumé plus long pour s’assurer de bien avoir compris.

On définit trois niveaux d'écoute :
| Niveau | Description |
|-|-|
| 1 | L’auditeur n'entend que partiellement ce qui est dit. Il se concentre sur d’autres sujets. |
| 2 | L’auditeur est très attentif aux mots. Il donne des confirmations verbales et non verbales. |
| 3 | L’auditeur est très attentif aux mots et autres signes non verbaux (regard, mouvements, …). |

Respectez l’opinion de la personne.

Le modèle d'écoute repos sur les quatre R :

| R | Description |
|-|-|
| Respect (Respect) | Comprenez que les opinions sont importantes pour l’autre personne.<br>Faites le vide dans votre esprit.<br>N'ayez pas de préjugé. |
| En temps réel (Real time) | Montrez un intérêt à la personne et à sa situation.<br>Ecoutez activement.<br>Concentrez-vous sur ce que la personne dit.<br>Concentrez-vous sur le fait d'écouter.<br>Evitez de poser des questions. |
| Reliez (Relate) | Identifiez le point de vue de vos clients.<br>Alignez-vous sur ce que dit la personne.<br>Montrez que vous comprenez la position de la personne en utilisant la communication réactive ("OK »).<br>Faites preuve d'empathie en utilisant des expressions du visage et en utilisant des mots tels que "exactement", "je vois" ou "je comprends".<br>Résumez vos sentiments. |
| Réflexion (Reflect) | La réflexion permet de demander des éclaircissements en cas de doute.<br>Levez toute ambiguïté.<br>Identifiez les véritables problèmes.<br>Utilisez des mots pour montrer que vous avez compris les points les plus importants.<br>Encouragez les clients à continuer à parler.<br>Donner un feedback sur les sentiments décrits par la personne.<br>Demandez des éclaircissements sur ce que vous ne comprenez pas.<br>Résumez pour vérifier la compréhension. |

## Feedback


## Apprentissage


## Conseils

Si la personne parle beaucoup, songer à faire des pauses pour classer les informations.

## Sources
