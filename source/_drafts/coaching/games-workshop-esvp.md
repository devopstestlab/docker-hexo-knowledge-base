---
title: "ESVP"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
  - ice breaker
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Mots clés : Ice Breakers & Warm Ups
- Durée : 5min à 15mn
- Nombre de participants : 5 à 15

## Speech de présentation

`Je vais vous demander d’indiquer, de manière anonyme, dans quel conditions vous arrivez ce matin, dans quel état d’esprit. Je vais vous donner des archétypes que vous allez choisir, indiquer l’archétype sur un papier, nous dépouillerons et prendrons acte.`

## Processus

- Demandez à chaque participant de renseigner de façon anonyme sur un petit papier leur archétype et de les déposer dans un récipient
  - Explorer : Curieux de nature, les explorers souhaitent découvrir, d’apprendre de nouvelles choses. Ils viennent pour toutes les informations possibles sur le projet, le produit... qui émergera de la réunion;
  - Shopper : Les shoppers sont intéressés par un sujet précis et souhaitent repartir avec des réponses;
  - Vacationer : Ils ne sont pas particulièrement intéressés par la réunion, mais c’est toujours mieux que de “travailler”
  - Prisoner : Ils auraient préféré faire autre chose, mais n’ont même pas eu le choix
- Dépouillez les papiers et dessinez des nuages de post-its correspondants
- Acter le résultat et guider une discussion sur ce que ça signifie pour le groupe
  - Une majorité d’Explorer vous permettra de mener un atelier avec une dynamique constructive et enrichie
  - Une majorité de Shopper vous poussera à faire émerger les attentes
  - Une majorité de Vacationer questionnera sur la pertinence des profils présents
  - Une majorité de Prisoner questionnera sur le processus d’invitation ou le profil des participants ou la réunion elle-même ou le projet.

## Feedback


## Apprentissage


## Conseils

Variantes :
- Si vous connaissez le contexte du groupe et vous sentez en confiance, vous pouvez mener l’ESVP au travers de positionnements physiques.
- Dans ce cas, opter pour une posture très ouverte sur les résultats, ne soyez pas gênés ou choqués que des personnes expose ouvertement qu’elles préfèrent être là plutôt qu’à leur bureau. Accepter les Vacationer vous permettra de gagner la confiance du groupe, les avis divergents comptent et l’honnêteté paye.
- Pour les prisoners, être plus vigilant. En fonction, remercier pour l’honnêteté, acter et dire à la personne que personne ne doit se sentir obligé d’être là et qu’elle est tout à fait libre de partir. Cette variante est à utiliser avec précaution

## Sources

- http://www.funretrospectives.com/esvp-explorer-shopper-vacationer-prisoner
