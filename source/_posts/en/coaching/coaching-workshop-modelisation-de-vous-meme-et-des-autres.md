---
title: "Modélisation de vous-même et des autres"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet de modéliser un comportement réussi afin d'apprendre, de s'améliorer.

Il permet ainsi de comprendre les stratégies qui contribuent au succès.

Cet outil permet d’aider à avoir une meilleure idée de son propre style d'apprentissage en réfléchissant sur les moments où le coaché a réussi.

### Processus

- Identifier une personne qui réussit déjà dans le domaine ou l'emploi qu’elle veut occuper.
  - Cette personne doit être une personne que le coaché peut interroger.
- Fixer un moment pour parler avec cette personne (que l’on va appeler « modèle »).
- Demander à cette personne de faire des observations sur ce qu'elle fait en posant des questions du type :
  - Qu'est-ce que j'observe sur le modèle ?
  - Quel est l'impact de ce que fait le modèle sur les autres ?
  - Qu’est-ce que je remarque sur le modèle ?
  - Qu'est-ce qui fait que le modèle se comporte de cette façon ?
  - Que ressent le modèle lorsqu'elle se comporte de cette façon ?
  - Comment le modèle est-elle devenue capable de faire ce qu'elle fait ?
  - Que faudrait-il pour que je puisse faire les mêmes choses ?
  - Comment aurais-je besoin de me sentir pour faire les mêmes choses ?
  - Quelles sont les actions spécifiques que je vais entreprendre pour y arriver ?
  - Pourquoi le modèle fait-il ce qu'il fait ?
  - Quelles pourraient être ses motivations ?
- Noter les réponses.
- A partir de ces réponses, identifier ce que le coaché devrait faire pour copier les stratégies de réussite utilisées par le modèle.
- Dans quelle mesure est-il à l'aise de le faire ?
- De quoi a-il besoin pour se sentir capable de faire les mêmes choses ?

### Conseils

- Au lieu de poser les questions au modèle, il est possible de demander au coaché de réfléchir aux questions et d'y répondre en fonction de ses propres observations.
- En posant les questions, ce peut être une bonne idée de faire parler d'un événement spécifique et d’observer le langage corporel.
- Il est possible d’appliquer cet outil à son propre comportement lors de ses propres succès : ce que vous avez fait, comment vous avez parlé, ce que vous n'avez pas fait, …

### Durée

40 minutes pour l’interview, 30 minutes pour le coaching
