---
title: "Script de visualisation d'objectif"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil est un script de base permettant de se préparer à travailler avec d’autres personnes.
- Fermez les yeux.
- Détendez-vous.
- Six respirations :
  - Respirez profondément et lentement :
  - Au moins six fois.
- Trois respirations :
  - Imaginez la couleur de la positivité.
  - Expirez la couleur de la négativité.
- Détendez les muscles autour de vos yeux.
- Concentrez-vous sur la façon dont vous souhaitez que votre vie soit.
- A quoi voulez-vous qu’un matin typique ressemble quand vous vous levez ?
- Combien de temps il vous faudra pour atteindre cet objectif (soyez réaliste) ?
- Avancez rapidement dans votre esprit.
- Imaginez-vous vous réveiller un matin typique une fois votre objectif atteint.
  - Que verrez-vous qui vous permettra de savoir que vous l'avez atteint maintenant ?
  - Qu’entendrez-vous ?
  - Que ressentirez-vous ?
  - Quelle est votre preuve ?
- Créez une image 3D claire et colorée.
- Remarquez la luminosité, les ombres, la profondeur des couleurs de votre image, la taille de l'image.
- Qu'est-ce qui est en arrière-plan ? Au milieu ? Au premier plan ?
- Obtenez une image ou une idée très claire de ce que vous voyez ou ressentez.
- Si vous connaissez l'objectif, décrivez-le ici.
- Ajoutez les sons :
  - Qu'allez-vous entendre (des voix, d'autres sons) ?
  - Quel est le volume ? La hauteur ? Le rythme ? Le rythme de ces sons ?
  - De quelle direction viennent-ils ?
  - Pouvez-vous entendre votre propre dialogue interne ? Que dites-vous ?
  - Quel ton de voix utilisez-vous pour vous parler (excité, félicitations) ?
  - Etes-vous impressionné d'avoir atteint votre objectif ?
- (Pause.)
- Que ressentez-vous maintenant en tant que futur vous ?
- Comment vous tenez-vous debout ? Assis ?
- En entrant dans une pièce, souriez-vous ?
- Que ressentez-vous dans votre estomac ? Votre poitrine ? Vos muscles ?
- Comment tenez-vous votre tête ?
- Comment parlez-vous ?
- Comment est votre vie maintenant ?
- (Pause.)
- Obtenez une idée très claire des changements que vous voulez réaliser dans votre vie actuelle.
- (Pause.)
- Avancez de six mois dans votre avenir.
- A quoi de la ressemble-t-il ?
- Comment est-ce de regarder six mois en en arrière et de réaliser que vous avez atteint votre objectif de l'époque ?
- A l'heure actuelle, à quoi ressembleront les obstacles rencontre de maintenant à l'avenir?
En ouvrant les yeux, vous vous sentirez prêt à vivre cet objectif.
