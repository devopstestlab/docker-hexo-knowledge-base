---
title: "Ne pas trop influencer l'équipe"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - conseil
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Face à la pression du temps qui avance, les managers peuvent ressentir de la pression pour prendre des décisions afin de faire avancer les choses au rythme voulu.

Cela peut pousser à exercer trop d'influence sur l'équipe, ce qui a pour conséquence de rendre l'équipe moins autonome.

Cette perte d'autonomie peut alors s'accompagner d'un sentiment d'impuissance, d'être sous-estimé. Cela peut conduire à la démotivation, au désengagement de l'équipe et à la perte de confiance envers le manager. Cela se traduit souvent par des réflexions du type : "Mais pourquoi faire des efforts si on n'est pas valorisés ?".

Ces sentiments se propagent alors relativement facilement et sont relativement durables.
