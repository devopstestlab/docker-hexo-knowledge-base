---
title: "Planification de l'action spatiale"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Quand une personne est très ancrée à la pensée négative, la déplacer vers un autre endroit de la « pièce » peut permettre de penser plus positivement.

Cet outil permet de visualiser un objectif et de planifier les étapes vers sa réalisation.

Il est très pratique pour les équipes qui produisent une vision ou démarrent un grand projet.

### Processus

- Démontrer le concept de visualisation d'une chronologie :
    - Préférez-vous voir l'avenir devant eux et le passé derrière ?
    - Ou préférez-vous avoir l'avenir à droite et le passé à gauche dans la pièce ?
    - Il n'y a pas de bonne ou de mauvaise réponse.
- Marcher vers l’avenir.
- Que ressentez-vous maintenant que vous êtes dans l’avenir et que vous avez réussi ?
- Le décrire en détail.
- Retourner vers l'espace où la situation actuelle existe.
- Décrire en détail le problème.
- Avancer à mi-chemin de la réalisation de leur vision.
- Expliquer ce qui se passe maintenant?
- Se déplacer de chaque côté du point à mi-chemin.
- Identifier les étapes et les actions pour progresser vers l'objectif.
    - Les noter.
- Résumer.
- Créer un plan d'action :
    - Y écrire dans chaque partie de la chronologie les actions.

| Situation actuelle | Etapes et actions immédiats | A mi-chemin | Etapes et actions intermédiaires | Futur |
|-|-|-|-|-|

### Conseils

En cas d’évaluation négative :
- Rappeler l’objectif.
- Ramener dans l'espace de l'avenir pour ressentir à nouveau le succès.
- Utiliser des mots ou des gestes pour montrer au coaché qu’il « laisse derrière » le jalon précédent.

Questions pouvant être posées :
- Si vous réussissiez, que verriez-vous, entendriez-vous ou ressentiriez-vous ?
- Que se passerait-il d'autre ?
- Que diraient les autres ?
- Quelles différences remarqueriez-vous ?

### Durée

45 minutes
