---
title: "Utiliser la persuasion au travail"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

La persuasion est une tentative honnête et bien intentionnée d'amener quelqu'un à penser ou à agir d'une manière particulière.

Persuader, ce n'est pas manipuler.

La manipulation est une tentative malhonnête d'amener quelqu'un à penser ou à agir d'une manière sans l'intention de vraiment l'aider.
- Se focaliser sur la création d’un excellent bilan de performance.
  - Plus vous êtes crédible, plus vous serez persuasif.
- Servir les autres autant que vous vous servez.
- Lorsque vous vous préparez pour une réunion ou une conversation, assurez-vous de penser aux gens autant que vous pensez à vos arguments.
  - Connaissez les personnes avec qui vous allez parler.
  - Réfléchissez à la manière d'adapter vos arguments pour eux.
  - Créez d'abord l'émotion.
  - Pour les aider à réfléchir, faites-les ressentir.
  - Créez des émotions positives en montrant des émotions positives.
  - Utilisez les indicateurs non verbaux de la passion (contact avec les yeux, sourires, mouvement ou tête, bras et corps).
  - Utiliser des exemples avec une image, une histoire ou une vidéo pour créer une émotion. Cela permet de dessiner des images mentales qui évoquent des émotions.
  - Très souvent, un compromis est nécessaire.
  - Une règle d'or : Résister au compromis si quelqu'un le fait tôt.
  - Faire des compromis vous aide à faire des progrès et vous donne la possibilité d'être persuasif plus tard.
  - Même arrivé à persuader votre interlocuteur, vous devez redérouler vos arguments plus tard : la persuasion est un processus utilisé au fil du temps, et non un événement ponctuel.
