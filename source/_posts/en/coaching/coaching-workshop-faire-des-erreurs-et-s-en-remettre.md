---
title: "Faire des erreurs et s'en remettre"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Des erreurs arrivent à tout le monde. Ce qui importe, c’est ce qui arrive après.

Lorsque vous comprenez les erreurs, vous comprenez comment réussir.
- Admettez que cette chose est arrivée.
- Reconnaissez vos émotions, que ce soit de l'embarras ou de la colère.
- Débarrassez-vous en.
- Réfléchissez à la raison de l'erreur pour en identifier la cause.
  - Que pouvez-vous faire différemment pour que cette erreur ne se reproduise plus ?
- Prenez rapidement et pleinement la responsabilité de votre erreur.
- Si nécessaire, présentez vos excuses.
- Nettoyer le gâchis que vous avez fait.
  - Ne laissez pas une petite erreur sans surveillance.
  - Si vous ne pouvez rien faire, signalez aux autres que vous êtes toujours intéressé à aider de toutes les manières possibles.
- Passer à autre chose.
- Faites un choix conscient pour le libérer et le lâcher.
- Produisez un travail de haute qualité pour que tout le monde oublie l'incident.
