---
title: "Objectifs rafraîchissants"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Fixer un objectif permet d’augmenter sa motivation pour l'atteindre.

Plus il y a de succès au début du processus, plus il y a de chances d'être motivé.

Si les succès diminuent, la motivation peut baisser.

Cet outil permet de revoir et d’actualiser les objectifs. Pour cela, il aide à identifier où se trouve l'obstacle.

### Processus

- Revoir la situation actuelle.
    - Quelque chose a-t-il changé qui pourrait interférer avec le résultat souhaité ?
- Revoir l'objectif.
    - Etait-ce au bon niveau ?
- Revoir les obstacles.
    - Qu'est-ce qui vous a empêché d'atteindre l'objectif ?
- Supprimer les obstacles.
- Vérifier que le coaché croit vraiment pouvoir atteindre l'objectif.
- Vérifier le niveau de désir du coaché d’atteindre l'objectif.
    - Est-ce quelque chose qui est important pour vous ?
    - Si le désir et la croyance sont encore assez forts :
        - Diviser l'objectif en sous-objectifs.
        - Définir des étapes claires.
- Faire un brainstorming pour éliminer les obstacles.
- S’assurer que les coachés ont identifié des récompenses en cas d’atteinte de l'objectif.

### Conseils

S’assurer que les obstacles sont réels, que ce ne sont pas des excuses.

S’assurer que le plan et l'objectif sont réalisables et ont des délais réalistes.

Expliquer l’importance d'avoir assez de croyance et de désir.
- Dans quelle mesure le coaché veut-il vraiment faire cela ?
- De qui vient l'idée d'atteindre cet objectif ?

### Durée

20 minutes
