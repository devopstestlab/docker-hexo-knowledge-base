---
title: "Fixer des attentes claires"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - conseil
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Pour aider l'équipe à s'approprier une tâche, les attentes doivent être claires :
- Exprimer la tâche à réaliser clairement, son périmètre en particulier.
- Indiquer clairement la date de réalisation attendue.
- Décrire clairement ce qui répondrait à vos attentes, éventuellement accompagné d'un exemple.
- L'équipe a besoin de comprendre en quoi cette tâche a de l'importance pour atteindre un objectif plus large. Expliquer en quoi cette tâche contribue à l'entreprise.
- Vous ne souhaitez pas micro-manager, mais vous voulez suivre l'avancement de la tâche. Précisez donc la manière dont vous attendez des feedbacks. Cela incite également l'équipe à réfléchir au sujet.
- Donner l'occasion à l'équipe de poser des questions après un temps de réflexion, le lendemain.
