---
title: "Priorisation par la grille importance-urgence"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet d’identifier et de prioriser les tâches importantes par rapport aux tâches urgentes.

Il est particulièrement utile quand le volume de tâches à réaliser est trop important.

### Processus

- Noter une tâche à accomplir par post-it.
- Remplir la grille suivante avec les post-its des tâches en considérant leur importance et leur urgence.

!(/images/priorisation-par-la-grille-importance-urgence.png)

- Quelles tâches devraient être terminées ?
- Lesquelles devraient être rejetées ? Quelles en seraient les implications ?
- Pour les tâches qui sont importantes mais pas urgentes, quand prévoyez-vous de les faire à l'avenir ?
- Pour les tâches qui sont à la fois importantes et urgentes, réalisez-les immédiatement.
- Priorisez l'ordre dans lequel les tâches doivent être effectuées.
- Pour les tâches qui ne sont pas importantes mais urgentes, planifiez quand et comment elles seront exécutées.

Il devrait apparaître que certaines tâches sont sans valeur ajoutée.

### Conseils

- Après cet atelier, naturellement se pose la question de la planification et des délais.
