---
title: "Gérer les émotions"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

L’Intelligence émotionnelle (EQ) correspond à votre capacité à ressentir les émotions chez vous et chez les autres à mesure qu'elles montent afin de pouvoir choisir de les utiliser de manière productive.

Elle vous permet de ressentir la tristesse ou la colère chez les autres, de développer votre capacité d'empathie ou encore de ressentir les tensions lors d'une réunion afin de pouvoir intervenir efficacement.

L’Intelligence émotionnelle vous aide à communiquer au bon moment et de la bonne manière.

Les autres apprécient plus l'expérience que vous apportez à l'équipe.

### Processus

- Evaluer votre Intelligence émotionnelle.
- S’engager à engager des activités de développement pour augmenter votre capacité dans ce domaine.
  - Identifier une ou deux personnes honnêtes avec vous.
    - C’est important car vous ne vous voyez pas comme les autres vous voient.
  - Leur demander au moins une façon de renforcer et d'améliorer votre intelligence émotionnelle au travail.
