---
title: "Techniques de feedback et exemples"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Le feedback permet d’aider le coaché à choisir les options à prendre et à changer ses comportements, actions et mentalités actuels.

Il permet d’aider à amorcer un changement personnel.

Cet outil repose sur l’utilisation du feedback lors de la conversation.

Les feedbacks doivent être clairs et constructifs, c’est-à-dire :
- spécifique, non général;
- descriptif, non évaluatif;
- adapté aux besoins perçus du coaché;
- souhaité par le coaché, non imposé à lui;
- opportun et en contexte;
- utilisable, c'est-à-dire dans l’un des domaines sur lesquels le coaché a un certain contrôle.

Le feedback se concentre sur :
- mettre en évidence des domaines dans lesquels le coaché peut être aveugle;
- Fournir des informations sur ses points forts ou ses domaines de développement;
- aider à surmonter un défi.
- valider le coaché en reconnaissant les progrès du coaché.

### Processus

- Choisir un domaine à examiner.
- Explorer la réalité actuelle :
  - Que ressent-il comme besoin de changer et pourquoi ?
  - Qu'est-ce qui indique qu'il veut et doit agir ou se comporter différemment ?
- Les détails de ce que le coaché veut découvrir sur lui-même :
  - Dresser une liste de choses que le coach devrait surveiller et écouter spécifiquement.
- De quelle manière le feedback doit-il être donnée (direct, en douceur) ?
- Quelles sont les limites en matière de feedback (pousser au delà de la zone de confort, …) ?
- Comment le coaché fait-il savoir s’il se trouve dans la zone d’inconfort ou pas ?
- Comment le coaché fait-il savoir si le feedback l’aide ou pas ?
- Explorer les mesures qui seront prises :
  - Que voulez-vous faire avec les feedbacks ?
  - Comment allez-vous l'utiliser ?
  - Quelles sont les solutions possibles ?
  - A quoi d'autre pensez-vous ?

### Conseils

- Faire des feedbacks le plus rapidement possible.
- Se mettre à la place du coaché : comment peut-il se sentir à propos des feedbacks ?
- Equilibrer le feedback : des feedbacks trop positifs ou trop négatifs minent la crédibilité et conduisent le coaché à les évacuer rapidement.
- Proposer de filmer la session, avec l’accord du coaché bien entendu.
- Lorsque vous mentionnez des lacunes dans le développement du coaché, équilibrez le feedback en mettant l'accent sur les points forts.
- Tenir compte de la situation particulière du coaché.
- Tenir compte des feedbacks visuels.
- Résumer régulièrement pour s’assurer de bien comprendre.
- Insister sur les points positifs.
- Transformer les feedbacks de « comment » en « comment ne pas le faire ».
- Prévenir les réactions comportementales difficiles afin de réduire les réponses défensives : « Il m'est difficile de dire que je n'ai pas pu vous observer lors de cette réunion, mais je me demande si vous êtes apparu comme ...».
- Mettre en évidence la logique reliant ce que le coaché a fait, la réaction des autres et les conséquences pour lui.
- Si le coaché cesse d'écouter les feedbacks ou commence à se défendre, vous y allez trop fort : reculez et recadrez.
- Si le coaché deviet défensif ou obstructif, chercher à découvrir pourquoi pour essayer de transformer cela en positif.

Conseils pour donner un feedback efficace :
- Analyser la situation actuelle : Qu'est-ce qui doit changer ? Pourquoi ?
- Décider des résultats et des objectifs que vous voulez du feedback.
- Enumérer les domaines nécessitant une discussion.
- S’assurer que le feedback est réalisable et mesurable.
- S’assurer que le moment est bien choisi.
- Créer le bon environnement.
- Se concentrer sur l'avenir.
- S’assurer que le coaché adhère aux feedbacks afin d’obtenir un accord.
- Eviter l'évaluation ou le jugement.
- Discuter d'abord des points positifs des domaines qui fonctionnent bien.
- Décrire le comportement lié à des exemples de situation spécifique.
- Décrire les domaines sur lesquels le coaché a besoin de feedbacks.
- Vérifier si le coaché a des idées sur ce sur quoi il doit travailler.
- S’adapter à la réceptivité, la tolérance pour recevoir des feedbacks.
- Travailler à explorer des alternatives plutôt que des réponses ou des solutions.
- Convenir d'actions spécifiques.

### Durée

20-40 minutes
