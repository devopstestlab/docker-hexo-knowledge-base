---
title: "Arête de poisson (fishbone)"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Arête de poisson (ou Ishikawa) permet d'analyser les causes profondes (root cause). Il consiste à regarder le problème sous différents angles.

L’arête de poisson, ou Ishikawa, permet d’aider à regarder le problème sous différents angles et de réaliser une analyse des causes profondes (root-cause).

### Processus

- Demander « Quoi ? », « Où ? », « Quand ? », « Qui ? » et « Pourquoi ? » quelque chose se passe.

!(/images/fishbone.png)

Exemple : « Nous ne savons jamais quand la release sera prête. »
- Qu'est-ce qui nous rend imprévisibles ?
  - Nous avons constamment des changements.
- D'où viennent les changements ?
  - Habituellement du PDG, qui est le visionnaire du produit, et un peu de nos utilisateurs, mais ce sont généralement des changements mineurs.
- Quel est le moment le plus critique ?
  - Lorsque le marketing veut des informations afin de préparer des campagnes et nous oblige à nous engager sur une certaine fonctionnalité plusieurs Sprints avant le lancement du produit.
- Qui peut influencer cela ?
  - Notre PDG, qui pourrait être plus présent lors de la release et faire ses commentaires à chaque Sprint Review.
- Pourquoi n'est-il pas présent à chaque Sprint Review ?
  - Il a rejoint les premiers avis, mais nous n'avions pas fourni beaucoup de fonctionnalités à ce moment-là, alors il a progressivement cessé de venir. Il est peut-être temps de l'inviter à nouveau.
