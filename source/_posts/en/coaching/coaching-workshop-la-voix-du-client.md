---
title: "La voix du client"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Inviter un client ou un stakeholder clé à se joindre à une réunion.

Partager :
- quelque chose qu'il aime vraiment au sujet de l'équipe,
- quelque chose qu'il aimerait que l'équipe fasse différemment.

Entendre ces choses du client en personne plutôt que d’un manager est souvent plus efficace.

Note : Les équipes virtuelles nécessitent en général plus d'activités de team-building que les équipes colocalisées.
