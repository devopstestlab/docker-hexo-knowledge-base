---
title: "Examen de la responsabilité"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

L’examen de la responsabilité utilise le mindfulness pour aider le coaché à réfléchir à ce qui s'est passé et à en tirer des leçons. Pour cela, il évalue les progrès et établit les niveaux d'engagement envers les objectifs.

Le principe est :
- De revoir les progrès vers les objectifs souhaités, afin de responsabiliser le coaché;
- De mettre en évidence l'apprentissage afin de pouvoir l’appliquer ensuite afin d’aligner les actions sur les résultats.

Cette technique permet au coaché de reconnaître des patterns de comportement et d'actions plus ou moins productifs, ce qui lui permet de faire des changements.

C’est l'espace à la réflexion et à l’évaluation entre les questions qui permet de faire comprendre au coaché ce qui est vraiment important pour lui.

### Processus

Au début d’une relation de coaching :
- Poser des questions sur l’objectif de coaching, du style :
  - Quels résultats veulent-ils ?
  - Quelles actions ont-ils déjà prises ?
  - Jusqu'où sont-ils allés avec ces actions ?
  - Quelles activités ont-ils encore ?
  - Pourquoi ?
  - Quelles activités ont-ils cessé de faire ?
  - Pourquoi ?
- Identifier ce qui compte dans ce qu'il a fait pour identifier le réel objectif.
- Identifier le niveau d'importance qu’il y accorde.

Une fois que la relation de coaching a déjà été établie :
- Passer en revue le résultat des actions auxquelles le coaché s'est engagé lors des sessions précédentes.
- Suggérer au coaché de prendre des notes. Cela crée un engagement physique envers le processus de revue.
- Poser des questions du style :
  - Qu'avez-vous fait ? Que n’avez-vous pas fait ?
  - Selon vous, quelles en étaient les raisons ?
  - Qu'avez-vous remarqué sur les modèles de comportement ?
  - Qu'avez-vous appris ?
  - Que voulez-vous faire avec ce que vous avez appris ?
  - Ce processus d'apprentissage vous convient-il ?
  - Quelles activités avez-vous réalisées depuis la dernière revue ? (revue par rapport aux actions / objectifs de la dernière session)
  - Quelles activités retardez-vous ?
  - Qu'est-ce qui vous arrête ?
  - Quels comportements avez-vous remarqués ?
  - Que voulez-vous renforcer ? Que voulez-vous arrêter ?
  - Qu'avez-vous appris depuis la dernière revue ?
  - A propos de votre entreprise ? A propos de vous ?
  - Quelles actions ou modifications aimeriez-vous apporter à votre plan (le cas échéan) ?
  - Comment ce processus de revue fonctionne-t-il pour vous ?
  - Ce qui fonctionne ? Qu'aimeriez-vous changer ?

### Durée

15-30 minutes
