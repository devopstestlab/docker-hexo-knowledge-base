---
title: "Eviter le principe de Peter"
date: 2020-05-11 23:02:01
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Le principe de Peter dit que tout le monde atteindra son niveau d'incompétence.

On suppose qu’une personne qui réussit dans un rôle peut connaître le même succès dans un rôle plus avancé. Cela peut se réaliser, mais pas toujours.

Par exemple, les compétences d’un développeur ne sont pas les mêmes que les compétences d’un manager.

### Processus

Si une personne souhaite évoluer vers un nouveau poste, envisager des alternatives :
- Peut-être voudrait-il garder le même emploi mais avec une petite augmentation de salaire ?
- Peut-être qu'il veut rester dans son rôle actuel mais élargir la portée de ses responsabilités techniques, ?

Cependant, si la personne veut vraiment faire une management, engagez la discussion pour évaluer son intérêt et ses connaissances sur le management.

Envisager de tester la personne afin d’avoir une idée sur la manière dont les choses pourraient se passer (poste de manager temporaire, remplacement d’une personne en congé de maternité, …).

Si vous pensez que vous êtes victime du principe de Peter :
- En parler.
- Faire un brainstorming avec votre manager sur l'utilisation d'une forme de formation ou de coaching des managers.
- Faire des changements de rôle pour aider le rôle à mieux répondre à ses compétences.
