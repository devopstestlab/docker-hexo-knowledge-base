---
title: "Cibler ce qui est important"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cette technique permet de considérer ce qui est important pour une personne dans sa vie à un moment donné. Elle permet d’aider le coaché à gérer, comprendre et résoudre des priorités conflictuelles.

### Processus

- Accrocher un modèle de cible de jeu de fléchettes au mur.
- Ecrire sur des post-its les éléments qui comptent pour le coach.
- Les positionner sur la cible (le centre représente la priorité la plus élevée).
- Poser des questions ouvertes et stimulantes pour faire réfléchir le coaché à ce qui est important pour lui dans la vie personnelle et professionnelle.
- Demander de réfléchir ce que cela lui dit sur les priorités de sa vie et sur les éléments les moins prioritaires.
- Mesurer les efforts et le temps nécessaire.
- Réfléchir à la décision initiale ou la contestation qui a été identifiée.
  - Comment le processus aide-t-il la décision ou la contestation ?
- Réfléchir à ce que cela signifie dans le contexte de « ce qui est important ».
  - Quelles sont les principales conclusions du client issues du processus ?

### Conseils

Vous pouvez aussi noter la catégorie et et la faire pointer vers la position appropriée sur la cible.

### Durée

30 minutes
