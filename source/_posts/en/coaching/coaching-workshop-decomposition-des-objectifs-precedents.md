---
title: "Décomposition des objectifs précédents"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Quand une personne n'atteint pas ses objectifs, cela vient souvent du fait que l'objectif n'est pas le bon ou qu’elle ne croit pas à cet objet.

La frustration vient parfois de la vitesse à laquelle les gens se dirigent vers l'objectif.

Cet outil permet d’aider les gens à réfléchir :
- à un objectif qu'ils ont atteint avec succès,
- à examiner les éléments clés qui ont fait le succès,
- à définir ce qui les a aidés à atteindre l'objectif.

### Processus

- Se souvenir d'un objectif que le coaché a atteint et dont il était fier.
- Penser aux détails de ce à quoi ressemblait le succès.
- Repenser à la manière dont il se sentait lorsque l’objectif a été atteint.
- Utiliser ces ressources (ressources internes, autres personnes, ressources matérielles, ...).
  - Quels points d'apprentissage le coaché a-t-il découverts ?
    - Développer avec les questions :
      - Qu'est-ce qui a vraiment bien fonctionné ?
      - Qu'est-ce qui vous a surpris ?
      - Que feriez-vous différemment ?
  - Aurait-il fait quelque chose de différent ?
  - Cette réflexion est-elle approfondie ?
  - Est-ce une expérience d'apprentissage positive ?
- Demander le coaché peut accéder à ces ressources et à ces points d'apprentissage pour atteindre ses objectifs actuels.
- Demander de préparer un plan d'action.

Tableau :
- Objectif ayant été atteint et dont vous êtes vraiment fier
- Ressources qui ont aidé à atteindre cet objectif
- Points d'apprentissage découverts
- Manière d’appliquer ces ressources et ces points d'apprentissage aux objectifs actuels
- Plan d'action

### Conseils

Inciter le coaché à utiliser ces points d'apprentissage et à implémenter les éléments réussis dans d'autres objectifs.

Tenir un registre des objectifs précédents.

### Durée

30 minutes
