---
title: "Plan de développement individuel"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Le plan de développement individuel (IDP - individual development plans) permet de planifier et de coordonner le développement des compétences.

Ils intègrent typiquement :
- Un inventaire des besoins de développement des compétences de l'organisation,
- Une évaluation des compétences actuelles disponibles dans l'organisation
- Une évaluation et une revue des désirs et des préférences de développement personnel.

Hiérarchiser ces besoins de développement de compétences.

Les coordonner avec le reste de l'équipe.

Etablir un plan détaillant comment et quand développer ces compétences (formation, mentorat, affectation à certains projets, ...).
