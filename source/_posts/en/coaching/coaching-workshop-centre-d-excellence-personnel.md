---
title: "Centre d'excellence personnel"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet d’améliorer la confiance, ce qui est utile par exemple pour améliorer ses compétences de présentation.

Le principe est de créer un espace pour aider à se concentration.

Il s’appuie sur les méthodes de visualisation.

### Processus

- Dans quelle mesure vous sentez-vous en confiance sur une échelle de 1 à 10 (1 étant faible) ?
  - Après plusieurs sessions, notez à nouveau votre niveau de confiance pour vérifier qu’il a augmenté.
- Penser à quelqu'un que vous connaissez qui semble confiant.
- Décrire en détail son apparence.
- Imaginer ce qu’elle ressent lorsqu'elle est aussi confiante.
- Reproduire sa position.
- Comment pensez-vous que cette personne se sent lorsqu'elle apparaît extrêmement confiante ?
- Pouvez-vous adopter ce sentiment ?
- Penser à un dialogue interne à répéter régulièrement du type « Je suis confiant, je me sens complètement en contrôle ».
- Visualisez que vous avez votre propre espace personnel devant vous.
  - Cet espace vous rendra confiants et puissants chaque fois que vous y entrerez.
  - Tout est « bien ».
- Quelle est la forme de cet espace ?
- A-t-il une couleur ?
- A-t-il d'autres fonctionnalités ?
- Imaginer que lorsque vous intervenez, une énergie puissante vous est projetée.
- Décrire comment cela se produit (exemple : à travers un faisceau qui vous excite ou une pincée de « poussière de fée »).
- Suivez une pratique complète.
- Visualisez la forme.
- Respirez profondément.
- Intervenez en adoptant le personnage de la personne que vous avez choisie.
- Ressentez la montée d'énergie.
- Préparez-vous à commencer votre présentation.
- Sortez.
- Répétez jusqu'à ce que la sensation soit suffisamment forte.

### Conseils

Décrire en détail la personne que vous aimeriez être.

Noter tout ce que vous pouvez sur la personne.

Vous pouvez poser ces questions :
- Comment vous sentez-vous dans votre espace personnel ?
- Comment pourriez-vous vous sentir encore plus énergique ?

### Durée

30 minutes
