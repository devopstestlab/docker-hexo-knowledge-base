---
title: "Changer les schémas de pensée négatifs"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Le changement des schémas de pensée négatifs permet d’aider les personnes dont la peur des conséquences empêche de faire quelque chose (rencontre avec le patron, revue client, …). Pour cela, cette technique aide à gérer ces conséquences de manière structurée et de réfléchir aux options. Cela permet de mettre dans un état d'esprit positif une personne avec des schémas de pensée négatifs.

### Processus

- Poser les questions suivantes :
  - Qu'est-ce qui vous rend anxieux ou inquiet (préciser la situation) ?
  - Indiquer la pire chose qui pourrait arriver.
  - Ce qui précède met-il la vie en danger ?
  - Si le pire devait arriver, comment le résoudriez-vous ?
- Poser la question supplémentaire :
  - « Pour améliorer le pire résultat possible, quelles sont les mesures spécifiques que vous prendriez ? »
  - « Si vous deviez prendre ces mesures, que ressentiriez-vous par rapport au résultat ? »
- Demander au coaché de s'imaginer dans un avenir où ce problème a été résolu.
- Demander de décrire ce qui de ce qu'ils font, entendent ou voient montre que le problème a été résolu. C'est ce qu'on appelle la concentration future.
- Pendant ce temps, observer le langage corporel du coaché, son intonation, son teint, …
- Evaluer à quel point vous êtes convaincu qu'ils ont fait le changement.
- Si vous percevez qu'ils sont toujours dans un état de pensée négatif, recommencer les étapes.
- Noter les réponses du coaché.

### Conseils

- Travailler sur un sujet à la fois.
- Traiter les questions rapidement afin de faire parler l'inconscient.
- Vous pouvez aussi choisir d’utiliser des questions supplémentaires :
  - Quelle serait la pire chose qui puisse arriver en faisant ou en disant cela ?
  - Quelles preuves avez-vous ?
  - Quelle logique y a-t-il dans cette réponse ?
  - Quelle serait la pire chose qui pourrait arriver si vous ne le faisiez pas ?
  - Qu'avez-vous à perdre ?
  - Qu'avez-vous à gagner ?
  - Que pouvez-vous apprendre de cette expérience ?

### Durée

10 à 30 minutes
