---
title: "Activité conjointe spéciale"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Voici un exemple :
- Faire des courses puis préparer le déjeuner ou le dîner ensemble.
- Discuter de l'apparition de schémas comportementaux exemplaires ou atypiques sur la manière dont l'équipe travaille.
