---
title: "Visualisation des objectifs"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

La technique de visualisation des objectifs permet d'activer une partie du cerveau qui vous permet alors de croire que vous avez déjà fait la chose en question. C'est en cristallisant la vision des objectifs que l'inconscient arrive à cela.

La visualisation permet d'augmenter la confiance dans les situations. Cela fonctionne mieux lorsque nous sommes dans un état de détente car l'inconscient peut stocker les informations dans un endroit facilement accessibles en cas de besoin. Une conséquence est que cela augmente les chances de pouvoir reproduire le succès dans la réalité.

### Processus

- Expliquer le but de la technique.
- S'assurer que le coaché a clairement défini son objectif.
- Demander de se détendre.
- Demander de visualiser le résultat.
- Tamiser les lumières.
- Vérifier que le coaché est détendu, les bras et les jambes décroisés.
- Lire la visualisation lentement.
- Demander au coaché de revenir lentement dans un état d'alerte.
- L'encourager à répéter régulièrement cette visualisation, cela permet d'accéder plus rapidement aux images.
- Demander au coaché s'il a suffisamment de visualisation pour pouvoir la recréer lui-même.

### Conseils

D'abord développer des relations profondes.

Rester imprécis : un langage vague accède à l'inconscient tandis qu'un langage précis accède à l'esprit conscient.

### Durée

15–20 minutes
