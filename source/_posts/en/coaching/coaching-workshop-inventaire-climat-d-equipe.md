---
title: "Inventaire climat d'équipe"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

L'inventaire du climat de l'équipe aide les managers à identifier les domaines où ils doivent s'améliorer en tant que leader et à quantifier ce qui fonctionne bien et ne doit pas être changé.

Cet outil est particulièrement bien adapté aux équipes nouvellement formées ou aux équipes dysfonctionnelles.

C’est un outil d'analyse perceptuelle qui :
- explore les performances d'une équipe à différents niveaux et
- aide à planifier des actions.

Si un comportement « de conformité publique et de défiance privée » survient, qui se matérialise par le fait que les divergences d'opinion ne sont pas diffusées par l'équipe lors de la session mais lors d’échanges privés, la pratique de l’outil peut se faire de manière anonyme.

### Processus

- Avant la session :
  - Distribuer le questionnaire « Inventaire climatique de l'équipe »
    - Placer une croix dans la case qui correspond le mieux à votre vision actuelle.

| | | | | | | | | | | |
|-|-|-|-|-|-|-|-|-|-|-|
| La communication n'est pas gratuite ; les gens retiennent des informations | | | | | | | | | | La communication est honnête, claire et opportune |
| Il y a une incertitude sur la vision | | | | | | | | | | La vision est claire |
| Un conflit existe au sein de l'équipe | | | | | | | | | | Il y a une coopération: tous les membres de l'équipe se soutiennent mutuellement |
| Les membres de l'équipe sont inquiets | | | | | | | | | | Les membres de l'équipe font confiance |
| Le processus décisionnel n'est pas clair; l'autorité et la responsabilité doivent être clarifiées | | | | | | | | | | Il existe des procédures claires pour la prise de décision |
| La confrontation des problèmes affectant l'équipe est évitée | | | | | | | | | | Il y a une confrontation aux problèmes affectant l'équipe |
| Il y a peu de revues des performances ou de leçons apprises | | | | | | | | | | Il y a des examens réguliers des performances et des enseignements tirés |
| Il y a un développement incohérent | | | | | | | | | | Il y a un engagement au développement à tous les niveaux |
| Il existe une confusion des rôles: il existe un croisement entre les rôles | | | | | | | | | | La clarté des rôles existe |
| Il y a conflit ou apathie envers les autres équipes | | | | | | | | | | Il existe une forte relation avec les autres équipes |

    - Remplir individuellement chaque questionnaire. Des commentaires peuvent être écrits au verso.
    - Retourner les questionnaires dans des enveloppes scellées.
    - Résumer les résultats dans un questionnaire commun.
- Pendant la session :
  - Parcourir les questionnaires avec les participants.
  - Aider les participants à trouver des thèmes.
  - Prioriser les domaines d'attention.
  - Elaborer un plan d'action en posant des questions :
    - Quelles sont, selon vous, les priorités sur lesquelles travailler au sein de votre équipe ?
    - Qu'avez-vous besoin de faire ?
    - Que voulez-vous que votre équipe fasse ?
    - Si vous amélioriez ce domaine, quelle différence verriez-vous ?
- Revoir ce plan d'action lors des sessions ultérieures.

### Conseils

- L'inventaire peut être réalisé de manière anonyme.
- Gardez la conversation tournée vers l'avenir.

### Durée

45 minutes
