---
title: "Préparation des idées"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Plus on réfléchit à la façon dont on présente son idée et prédit le type de réponses qu'on peut obtenir, plus on a de chances de réussir.

Cet outil aide à se préparer et à structurer une proposition.

Il permet d’aider le coaché à présenter une idée.

Il peut être utilisé :
- au début de la conception d'une idée afin de s’assurer qu’elle va fonctionner,
- juste avant une présentation si l’idée est entièrement élaborée.

### Processus

- Décrire l’idée.
- Ecrire le message clé.
- Ecrire dans un tableau :
  - L’idée :
    - Quel est le concept clé de l'idée ?
    - Quels sont les avantages de l'idée ?
      - Un avantage est ce que cela fera pour l'autre personne.
    - Quelles sont les conséquences de ne pas adopter l'idée ?
      - Réfléchir aux conséquences de la non-adoption de l'idée et à la manière dont ils pourraient la présenter lors de la présentation.
  - L’auditoire :
    - Quelle est la composition du public ?
      - Quelle est leur composition en termes de pouvoir et d'influence ?
      - Combien seront présents à la présentation ?
    - Que pensent actuellement les membres du public ?
      - Quelle connaissance préalable ont-ils du contexte de l'idée ?
      - Quels sont leurs positions et intérêts ?
    - Quelles questions auront-ils sur l'idée ? Quelle sera leur question « brûlante » ?
    - Quelles objections auront-ils à l'idée ?
    - A quels styles de personnalité vais-je présenter ?
      - Travailler sur la manière de présenter les informations en fonction des différents styles (exemple : personnes analytiques, personnes axées sur la réussite, aides, …).
  - Le pitch :
    - Déclaration d'ouverture : Comment impliquer le public et décrire clairement l'idée ?
    - Approche principale : Quoi et comment ?
    - Fermeture : Comment le rendre attrayant ?
- Encourager à faire un brainstorming sur les questions potentielles que les membres de l'auditoire peuvent avoir.

### Conseils

- Bien travailler sur la manière de présenter logiquement les informations.
- Se rappeler qu’il faut plus écouter que parler.
- Rassembler des informations sur l'historique de la proposition.

### Durée

45 minutes
