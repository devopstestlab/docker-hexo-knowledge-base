---
title: "Guide des autres au cours du Change Journey"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet d’aider les gens à comprendre leurs sentiments et leurs émotions lors d'un changement.

Il consiste à prendre du temps pour réfléchir à l’étape à laquelle on est afin d’envisager des stratégies d’adaptation ou des interventions qui aideront le cheminement vers un « engagement ».

### Processus

Dans le cas d’un individu :
- Dans un tableau, demander de résumer chacune des phases clés.
- Demander au coaché de décrire son parcours depuis le moment où il a identifié qu'un changement allait se produire.
- Noter :
  - les émotions,
  - les progrès réalisés à ce jour,
  - la vitesse à laquelle certaines phases ont été parcourues,
  - où se trouve l'obstacle.
- Demander de marquer avec un X la phase à laquelle il rencontre l'obstacle.
- Demander au coaché de comprendre ses propres obstacles personnels.
- Lui demander d'explorer les options pour avancer ou les surmonter.
  - Qui peut les soutenir ?
  - Comment ?
  - Quelles mesures pourraient-ils prendre pour obtenir ce soutien ?
- Lui demander de réfléchir à des moyens d'éliminer l'obstacle.

Dans le cas d’une équipe :
- Demander au coaché de marquer un X à la phase où il pense que chacun de son équipe est positionné sur le chemin.
- Poser les questions :
  - Pourquoi peuvent-ils se trouver à différents endroits ?
  - Comment cela peut-il se manifester dans leur performance au travail ? Le comportement de l'équipe ? La conduite individuelle ?
  - Comment peuvent-ils s’entraider ?
  - Que peut faire le manager pour encourager activement les gens à traverser les phases ?
  - Comment respecter le rythme et la position sur les phases tout en encourageant le mouvement ?
  - Comment valider l'estimation ?

### Conseils

- Considérer à la fois les aspects positifs et négatifs.
- Il peut être utile de positionner les autres pour éventuellement détecter des conflits, des opportunités, des obstacles et des catalyseurs pour que le changement fonctionne.
