---
title: "Coaching d'une équipe"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Aujourd'hui, un manager est plus un coach qu'un dictateur. Voici quelques conseils pour coacher une équipe :
- Soyez un exemple. En effet, l'équipe scrute vos comportements et les écarts que vous faites entre ce que vous attendez d'eux et ce que vous faites vous-même.
- Aidez votre interlocuteur à trouver les réponses par lui-même, tout en les développer en même temps. En effet, donner la réponse permet de gagner du temps à court terme, mais ça ne fait pas l'avancer. Par exemple, demandez : "Quelles sont vos options ? Qu'en dirait le reste de l'équipe ?", ...
- Facilitez l'amélioration de l'équipe en identifiant un ou deux sujets d'amélioration à cibler pour l'équipe. Par exemple, réduire l'inneficacité des réunions.
- Choisissez les moments pour le coaching, sachez vous mettre en recul par exemple si des tensions mettent du temps à se dissiper.
- Sachez identifier quand l'équipe n'a pas besoin d'un coach, laissez leur l'opportunité de régler le problème par eux-mêmes.
