---
title: "Les trois amigos"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Les "Trois amigos" (ou Three Amigos ou Le Story Kick Off huddles ou la Triade)permettent d’examiner un incrément de travail avant, pendant et après le développement du point de vue de plusieurs perspectives :
- Business : Quel problème essayons-nous de résoudre ?
- Développement : Comment pourrions-nous construire une solution pour résoudre ce problème ?
- Test : Que pourrait-il arriver ?

Cela permet d’équilibrer les collaborations des personnes ayant des perspectives différentes.

### Processus

- Inviter les personnes détenant ces différentes perspectives (pas forcément trois).
- Définir ce qu'il faut faire.
- S'entendre sur la façon dont elles savent quand cela est fait correctement.
- Formuler la compréhension partagée sous forme d’exemples d’incréments de travail.
- Examiner les incréments du produit qui ont été mis en œuvre pour s’assurer qu’il est correct dans ces différentes perspectives.

Cet atelier permet d’identifier les malentendus et la confusion très tôt.
