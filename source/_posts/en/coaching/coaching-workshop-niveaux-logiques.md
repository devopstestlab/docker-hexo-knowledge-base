---
title: "Niveaux logiques"
date: 2020-05-13 21:02:01
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet de mettre en évidence les blocages qui surviennent quand une personne cherche à réaliser une vision.

Ces blocages affectent la clarté et la réalisation de la vision.

Pour faire disparaître ces blocages, l’idée est d’explorer des choses qui ne sont pas nécessairement au premier plan de leur esprit.

Cet outil fonctionne aussi bien avec des individus que des groupes.

Il est particulièrement utile avec des personnes qui ne sont pas sûres de l'avenir.

### Processus

- Afficher au mur de grandes feuilles blanches avec le tableau des niveaux logiques suivant :

| Niveau | Questions | Concept | Notes |
|-|-|-|-|
| Environnement | Où sont vos contraintes ?<br>Où et quelles sont vos opportunités ? | Où les choses se passent-elles ?<br>Quand les choses se produisent-elles ? | |
| Comportement | Quels comportements spécifiques avez-vous qui vous soutiennent ?<br>Quels comportements ne vous soutiennent pas ?<br>Y a-t-il quelque chose à ajouter sur le comportement ? | Que fais-je ?<br>Que font les autres ? | |
| Capacités / compétences | De quelles ressources disposez-vous ?<br>Quelles stratégies vous aideront ? | Comment je fais les choses ?<br>Que puis-je faire ? | |
| Valeurs / croyances | Qu'est-ce qui vous motive ?<br>Que pensez-vous des autres ?<br>Qu'est-ce qui est important pour vous ?<br>Y a-t-il quelque chose à ajouter sur les valeurs et les croyances ? | Quelle est ma conviction et pourquoi? | |
| Identité | Qui êtes vous ?<br>Quel est votre but ou votre mission ?<br>Y a-t-il quelque chose à ajouter sur votre identité ? | Pourquoi suis-je ici ?<br>De quoi s'agit-il pour moi ? | |
| Vision | Que voyez-vous ou ressentez-vous pour l'avenir ?<br>Quoi d'autre ? | Comment puis-je me rapporter à l'avenir ?<br>Où vais-je ? | |

- Demander à l'équipe de se déplacer pour écrire des notes sur le tableau.
    - Une alternative est d’écrire un tableau pour chaque niveau sur une feuille séparée.
- Inviter l’équipe à commencer par le premier niveau.
- Pour passer d’un niveau à l’autre, poser des questions indirectes comme :
    - « Quoi d'autre ? »
    - « Autre chose ? »
- Noter ce que dit l’équipe.
- Une fois tous les niveaux traités, pose les questions :`
    - « Qu'est-ce qui vous a surpris ? »
    - « Quelle a été la chose la plus intéressante qui en soit ressortie pour vous ? »
- Discuter de chaque niveau.
    - Donner des feedbacks sur ce que l’équipe a réellement dit.
    - Discuter des implications.
- Préparer un plan d'action pour la suite.

### Conseils

Au préalable, expliquer que les questions sont délibérément vagues et que la réponse fournie sera la bonne.

Se tenir derrière les cachés pour qu’ils ne perdent pas leur fil conducteur.

N’essayez pas de passer trop rapidement au niveau suivant.

Les coachés peuvent devenir émotionnels à un certain niveau. Cela correspond normalement à un blocage.

Faites une pause à chaque niveau en posant des questions suivies d’une autre pause :
- « Autre chose ? »
- « Quoi d'autre ? »

Vous pouvez poser des questions indirectes (surtout pas spécifiques, ce qui ferait perdre leur flux) :
- Qu'est-ce qui vous gêne dans votre environnement ?
- Qu'est-ce qui vous aide dans votre environnement ?
- Qui d'autre êtes-vous ?
- Etes-vous quelqu'un d'autre ?

Le coaché peut parfois revenir à un niveau précédent.

### Durée

45 minutes
