---
title: "Le brainstorming inverse"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

La pratique du brainstorming inverse consiste à répertorier les moyens d'atteindre un objectif opposé à celui réellement souhaité.

Le processus est simple :
- Définir clairement le problème.
- Définir le problème opposé (« Comment ruiner … ? »),
- Générer des solutions à ce problème opposé.
- Comparer les solutions aux pratiques actuelles.
- Eliminer les pratiques nocives.

Exemple : pour le problème « Comment améliorer les relations avec les fournisseurs ? », la question pourrait être « Comment pouvons-nous ruiner au mieux notre relation avec les fournisseurs ? ».

Les idées générées sont très souvent celles actuellement en pratique.

Les bénéfices sont :
- Génération d’approches nouvelles et variées,
- Apporte de nouvelles informations sur les processus actuels qui doivent être modifiés ou supprimés.
