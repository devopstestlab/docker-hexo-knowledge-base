---
title: "Renforcement de la confiance"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Si vous pensez ne pas pouvoir faire quelque chose, vous n’arriverez pas à la faire.

Cet outil permet de montrer que le coaché a la confiance nécessaire pour faire ce qu'il veut. Pour cela, il met le coaché dans un état positif pour renforcer sa confiance.

### Processus

- En quoi manquez-vous de confiance (exemple : difficulté à trouver les mots à utiliser lors d'une réunion) ?
- En quoi consiste ce scénario ?
- A quoi ressemblerait ma position physique lorsque fait tout ce en quoi je manque de confiance ?
- Répondre de la manière requise par le scénario.
- Quel est mon niveau de confiance sur une échelle de 1 à 10 (10 = vraiment confiant) ?
- (Donner des feedbacks sur les mots utilisés, leur impact, le langage corporel, le ton de la voix, la confiance perçue.)
- Rejouer le scénario en utilisant ces feedbacks et en modifiant tout ce qui n'a pas fonctionné la fois précédente.
- Qu’est-ce qui a mieux fonctionné ?
- Qu’il est mon niveau de confiance ?
- Répétez ce processus autant de fois que nécessaire.
- Quand j’ai atteint un niveau de confiance d'au moins 8, rejouer le scénario une fois de plus en notant les mots exacts utilisés, le langage corporel et l'intonation.
  - Ceci pourra être utilisé comme ressource dans la vie réelle.

### Durée

30-45 minutes
