---
title: "Donner un feedback tourné vers l'avenir"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Très souvent, les feedback sur les performances se focalisent sur ce qui ne va pas et sur la manière dont les choses auraient dû être faites. En d'autres termes, il est axé sur le passé.

Les gens ressentent ces critiques comme honteuses.

Et quand les gens se sentent honteux et jugés, cela ralentit l’élan, et mène au désengagement.

On se met sur la défensive.

Mais les retours futurs sur les centres d'intérêt biffent cette critique personnelle en faveur de la détermination de ce qui est possible et de ce qui pourrait être amélioré à l'avenir.

Préférez des feedbacks du type "Ok, c'est décevant pour toi et moi." "Qu'est-il arrivé?".

Exprimer votre déception est bien réel.

Demander ce qui est arrivé vous donne l’occasion de rassembler des informations tout en permettant à votre employé de raconter son histoire et de s’épancher un peu.

Mais vous ne voulez pas vous contenter de laisser aller les choses : une question porte sur l’avenir : "Rassemblons nos esprits et faisons un brainstorming pour voir comment nous pouvons changer les choses."

Explorer les possibilités. Identifier les solutions.

Une fois la solution en marche, prévoir du temps pour la réflexion et poser quelques questions supplémentaires :
- "Que pouvons-nous faire à l'avenir pour nous assurer de ne plus nous retrouver ici?"
- "creusons vraiment et explorons les leçons tirées de toute l'expérience."

Une fois que vous avez compris le problème, prenez quelques minutes pour vous calmer et rassembler vos pensées.

Remettre votre chapeau de coaching.

Nous ne pouvons pas changer le passé mais nous pouvons influencer le futur.
