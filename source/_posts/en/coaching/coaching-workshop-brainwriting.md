---
title: "Brainwriting"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Le brainwriting (écriture cérébrale) permet de stimuler de nouvelles idées en partageant des pensées de façon linéaire.
- Un problème est identifié et partagé.
- Chaque participant génère trois solutions possibles au problème en question.
- Chaque participant envoie ses solutions à un autre participant.
- Le destinataire écrit trois idées supplémentaires inspirées de celles déjà écrites.
- Il envoie la liste à un autre membre de l'équipe.

Le processus est répété jusqu'à ce que chaque destinataire ait reçu ses idées d’origine.

Pour une équipe à distance :
- Utilisez l'email.
- Les textes sont insérés à chaque fois en début d’email.
- L'expéditeur doit être le dernier à recevoir la liste finale.
- Les emails peuvent être complétés avant la réunion virtuelle. Dans ce cas, pendant la réunion, les listes définitives sont partagées.
