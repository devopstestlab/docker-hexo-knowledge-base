---
title: "La matrice des compétences / désirs (The skill/will matrix)"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

La matrice des compétences / volonté (de Max Landsberg, dans "Le Tao du coaching", 2003) vous aide à évaluer les niveaux de compétence et de motivation des gens.

Vous pouvez ensuite adapter votre coaching à leurs besoins.

Le giagramme de matrice de compétence / volonté :

!(/images/la-matrice-des-competences.png)

La matrice est divisée en quatre quadrants, chacun reflètant un style de coaching adapté au niveau de compétence et de motivation de chaque personne.

Les quatre styles sont :
- Direct :
  - Public : faible compétence / faible volonté
  - Démarche :
    - Cela prend beaucoup de temps et nécessite beaucoup d'efforts.
    - Si vous gérez la personne :
      - Fixez des délais clairs.
      - Surveillez de près les progrès.
    - Si vous ne la gérez pas :
      - Encouragez-la à discuter des délais et des instructions avec son manager.
      - Encouragez-la à demander de l'aide si elle a des difficultés.
      - Envisagez d'utiliser le modèle GROW pour développer les compétences et augmenter la motivation.
- Exciter :
  - Public : compétences élevées / faible volonté
  - Démarche :
    - Identifiez les facteurs de motivation de la personne avec :
      - la théorie des facteurs de motivation et d'hygiène (de Herzberg),
      - la théorie de la motivation humaine (de McClelland),
      - la théorie des trois facteurs (de Sirota).
    - Faites un lien entre ces facteurs de motivation et ses motivations dans ses projets actuels.
    - Soulignez qu'il y en aura plus s'il donne un exemple d'effort et de travail acharné.
    - Si la personne remarque que des points de ses projets ou de son environnement de travail le démotivent, explorez comment supprimer ou réduire ces facteurs de démotivation.
- Guide :
  - Public : faible compétence / haute volonté
  - Démarche :
    - Rechercher des opportunités pour la personne de développer des compétences clés.
    - Lui donner la confiance nécessaire pour les appliquer.
    - Utilisez le modèle GROW.
    - Explorez les opportunités de développement.
    - L'encourager à réfléchir à son style d'apprentissage préféré pour tirer parti des opportunités d'apprentissage.
    - L'encourager à prendre des risques contrôlés pour apprendre de ses expériences.
    - Cette personne peut avoir besoin de conseils sur un problème spécifique.
    - Pour mettre en évidence l'impact du comportement de la personne et l'aider à le changer, utilisez l'outil Situation - Comportement - Impact.
- Délégué :
  - Public : haute compétence / haute volonté
  - Soyez moins un entraîneur, et plus un mentor ou une "caisse de résonance" pour ses idées.
  - Lui faire savoir que vous êtes disponible pour un brainstorming en cas de besoin.
  - L'encourager à adopter une approche de "coaching personnel" pour le développement personnel.
  - L'assurer clair sur vos responsabilités en tant que mentor.
  - Si vous êtes son manager, vérifiez qu'il n'y a pas de conflits d'intérêts.

### Processus

- Evaluez le niveau de compétences.
- Identifiez ses motivations.
- Tracez ses compétences et ses motivation sur la matrice.
- Identifier le style de coaching à utiliser.
