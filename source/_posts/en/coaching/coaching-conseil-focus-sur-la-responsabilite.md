---
title: "Focus sur la responsabilité"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Le modèle de responsabilité de Christopher Avery explique comment fonctionne la responsabilité, essentielle à Agile.

Le cerveau est habitué à prendre rapidement des décisions.

Exemple : en garant votre voiture, vous rayez accidentellement la voiture à côté de vous.

| Niveau | Description | Phrases type |
|-|-|-|
| Le déni | C’est la première solution trouvée par le cerveau. | « Ca ne s’est jamais produit. »<br>« Ca marche sur mon PC. » |
| Le blâme | Puisque vous n’êtes pas à l’aise avec le déni, votre cerveau trouve la solution de blâmer. | « C'est de sa faute ! »<br>« Je l’ai bien codé, c’est à cause des spécifications si ça ne marche pas. » |
| La justification | Comme le blâme n’est pas satisfaisant, le cerveau trouve une autre solution : la justification. | « Ca peut arriver à tout le monde »<br>« Dans le développement, nous rencontrons souvent des difficultés techniques, c'est comme ça. » |
| La honte | Vous le voyez comme votre propre faute. | « Ce n’est pas assez bon. »<br>« Tout est de ma faute. »<br>« C'est trop difficile. »<br>« Nous ne sommes pas assez bons. »<br>« C’est normal de demander des experts. » |
| L’obligation | Le résoudre comme si c'était votre obligation. | « Je dois le faire. »<br>« Nous faisons un Standup à cause de Scrum. » |
| Le départ | A tout moment, vous pouvez décider d'arrêter.<br>Personne ne vous oblige à être responsable. | « Ce n'est pas important pour moi. » |
| La responsabilité | C’est le dernier niveau du modèle de processus de responsabilité.Avec Scrum, cela correspond au cas où quand un bug est signalé, l'équipe le corrige et discute de ce qu'elle va changer afin que cela ne se répète pas. | « Que puis-je faire différemment la prochaine fois pour que cela ne m'arrive pas à l'avenir ? »<br>« Je changerai. » |
