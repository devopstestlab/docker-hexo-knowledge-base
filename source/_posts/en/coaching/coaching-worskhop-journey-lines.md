---
title: "Journey lines"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Un graphe journey line (ligne de parcours), créé par Tichy en 2002, représente le parcours professionnel d’une personne.

### Processus

Pendant 10 minutes environ, chaque personne trace les hauts et les bas de son parcours dans un graphe comme celui-ci :

!(/images/journey_lines.png)

La ligne tracée est en dent de scie. Chaque point haut ou bas comprend des notes sur la vie professionnelle ou personnelle.

Chaque personne présente son graphe à l'équipe.

Chacun prend des notes :
- les compétences utiles pour le projet,
- les talents potentiellement utiles,
- les expériences,
- les valeurs.

Chacun lit à haute voix ses notes et les fixe sur le graphe.

### Conseils

Cet atelier permet à chacun de connaître les antécédents des autres. Ainsi, chaque personne est affirmée pour ce qu’elle est et ce qu’elle apporte.
