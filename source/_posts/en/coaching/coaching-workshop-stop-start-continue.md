---
title: "Stop, Start, Continue"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

La pratique « Stop, Start, Continue » permet d'améliorer les relations de travail entre les membres d’une équipe ou les relations d’une équipe avec d’autres.

L’idée est de :
- donner et de recevoir des commentaires aux membres de l’équipe sur ce qu'ils font qui est utile aux autres et ce qui ne l'est pas,
- de recevoir des idées sur la façon dont ils peuvent s'améliorer.

Ces commentaires doivent être équilibrés : certains sont bons et d’autres mauvais. Il est important que tous les commentaires ne soient pas négatifs.

Pour résoudre des problèmes entre équipes, suivez ces étapes en équipe.

Cette pratique permet aussi d’aider une équipe à évaluer ce qu'elle fait bien et ce qu'elle souhaite améliorer.

### Processus

- Présenter les règles.
- Compléter le tableau « Stop, Start, Continue » :

| Stop | Start | Continue |
|-|-|-|
| M’envoyer des emails non liés au travail (blagues).<br>Arrêter de m’interrompre pendant les discussions. | Me dire ouvertement quand vous n’êtes pas d’accord avec moi.<br>Donner plus de crédit à vos idées. | Votre excellent travail technique.<br>Votre feedback honnête et bienveillant.<br>M’impliquer dans la résolution des problèmes qui nous affectent tous les deux. |

- Chaque participant répond dans un document distinct aux trois questions suivantes, pour chaque autre participant :
  - Qu’est-ce que j’aimerais que cette personne arrête de faire ?
  - Qu'est-ce que j'aimerais que cette personne commence à faire à l'avenir ?
  - Qu’est-ce que j’aimerais que cette personne continue à faire ?
  - Partager les documents Stop, Start, Continue avec la personne pour laquelle ils ont été remplis.
  - Elaborer des plans d'action :
  - Chaque participant doit décrire :
    - ce qu'il pense des commentaires qu'il a reçus,
    - ce qu'il prévoit d'en faire.
