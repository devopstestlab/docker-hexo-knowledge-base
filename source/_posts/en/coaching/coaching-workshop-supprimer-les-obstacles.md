---
title: "Supprimer les obstacles"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Techniques qui aident à éliminer les obstacles :
- Visualisation
  - Respirez profondément pour vous détendre.
  - Visualisez l'objectif que vous souhaitez.
  - Pensez à l'obstacle.
  - Permettez à votre inconscient de le visualiser comme une forme et une couleur.
  - Rendez l'image plus grande et plus colorée.
  - Imaginez-vous en train de détruire l'obstacle comme vous le souhaitez (exemple : le briser en morceaux, le pousser d’une falaise, …).
    - Utilisez un langage vague pour faire comprendre que tout choix est bon.
  - Imaginez mettre tout ce qui vous rappelle l'obstacle dans le panier d'une montgolfière.
  - Visualisez-la en train de s’envoler jusqu'à ce que l'obstacle ait l'air d'être complètement détruit.
- Défi de la perception du client
  - Comment décrivez-vous l'obstacle ?
    - Si c'est toujours avec des connotations négatives, décrivez-le différemment.
    - L’obstacle devrait sembler moins difficile.
- Briser l’obstacle
  - Décomposez l'obstacle en petits morceaux.
  - Ne vous attaquez qu'à un seul morceau à la fois.
  - Fixez de petits jalons pour atteindre l'objectif.
  - Donnez-vous des récompenses à chaque étape franchie.
  - Définissez des délais plus réalistes.
