---
title: "Mon travail préféré"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

L’atelier "Mon travail préféré"" permet de mieux se connaître et de mieux comprendre comment les gens aiment travailler.

Chaque participant discute du meilleur emploi qu'il ait jamais eu et pourquoi il l'aimait tant.

Noter ces commentaires sur un tableau blanc.

Cela fait apparaître des choses que chacun apprécie (exemple : l'autonomie, la variété, le respect des autres membres de l'équipe, ...).
