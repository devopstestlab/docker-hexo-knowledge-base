---
title: "Le modèle Discovery"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet d’aider à identifier les lacunes de développement et à élaborer un plan d'action.

Cela permet par exemple de savoir ce qui est requis dans un rôle qu’on n’a jamais joué.

Il permet de se voir sous de plusieurs angles différents :
- la situation actuelle,
- ce qui est attendu du coaché,
- la façon dont les autres perçoivent le coaché,
- les actions requises pour combler les lacunes.

Exemple : pour obtenir une promotion pour laquelle le coaché n’est pas prêt mais a du mal à comprendre pourquoi.

Cet outil permet d’identifier les lacunes importantes ou les « angles morts » (c’est-à-dire les comportements inappropriés que les autres peuvent voir mais que le coaché ne remarque pas en lui-même).

Modèle de découverte :

| Objectif | |
|-|-|
| Compétences et aptitudes | Critères de réussite de l'organisation |
| Quelle est votre propre perception de vos compétences et capacités actuelles ? | Qu'attend l'organisation de vous dans le rôle en discussion ? |
| Informations ou questions | Informations ou questions |
| Quelles sont selon vous vos forces ?<br>Quels sont selon vous vos axes de développement ?<br>Objectifs, plan de développement, apprentissage realisé | Vision, mission, stratégie, valeurs, objectif, normes culturelles, modèles de compétence, étude de marché, conversations avec les managers seniors, modèles de rôle de l'entreprise |
| Réalité vs perception | Actions |
| Comment êtes-vous perçu ?<br>Quelle est la réalité ?<br>Comment le savez-vous ? (preuve / observation) | Quelles actions sont nécessaires pour combler les lacunes mises en évidence ? |
| Informations ou questions | Informations ou questions |
| Auto-évaluation<br>Stimuler le client pour recueillir des informations<br>Evaluer des performances<br>Évaluer professionnelle<br>Observation et feedback<br>Coach pour offrir un feedback objectif | Quels sont les plus difficiles à accepter ?<br>Quels sont les plus critiques ?<br>Qu'est-ce qui ferait le plus de différence pour vous ? |

### Processus

- Identifier pourquoi le coaché veut l'objectif.
- Encourager à vérifier ses raisons.
- Le point de vue de l'organisation :
  - Que recherche l'organisation ?
  - Penser à un modèle (exemple : quelqu'un qui a déjà atteint le poste) :
    - Quelles sont les qualités et les capacités de cette personne ?
- Le point de vue des compétences et capacités :
  - Quelles sont vos compétences et capacités ?
  - Lesquelles sont très développées ?
  - Lesquelles ont besoin de développement ?
- Le point de vue réalité versus perception :
  - Comment êtes-vous perçu par rapport aux critères de réussite de l’organisation ?
  - Comment pouvez-vous vérifier ces perceptions ?
  - Analyser ce qui doit se produire pour que vous puissiez répondre aux critères de réussite de l'organisation.
- Le point de vue des actions requises :
  - Quelles sont les différences significatives ?
  - Identifier des actions pour combler les lacunes.
- Lister les objectifs et les priorités à court et à long terme.

### Conseils

- S’il y a plusieurs objectifs, prioriser les objectifs en fonction de :
  - Déterminez si les objectifs sont importants ou urgents.
  - Evaluez-les par rapport à une échelle (facile, modéré, difficile).
  - Evaluez-les par rapport à un critère de « rentabilité » gagnant-gagnant.
- Si le coaché ignore les lacunes, explorer ces points.
- Encourager à regarder plus loin (exemple : changement dans l'organisation, évolution du marché, …).
- Identifier les compétences et capacités que l'entreprise valorisera à long terme.

### Durée

45 minutes
