---
title: "Le modèle SLOBA pour renforcer la crédibilité"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Définition

Cette technique permet d'aider une personne à accroître sa crédibilité auprès d'une ou plusieurs personnes.

Elle est particulièrement utile lorsqu'il y a de nouvelles personnes dans une équipe.

### Processus

- Demander avec qui le coaché veut établir sa crédibilité.
- Etudier avec l'ensemble de ces personnes ce qu'elles savent actuellement dans les domaines suivants : la situation, les objectifs de partage des connaissances, ce que le coaché ​​pense créer la bonne impression, une photo d'eux démontrant une confiance appropriée
  - Public et situation :
    - Poser les questions suivantes :
      - A qui parlez-vous ?
      - De quel genre de personnes sont-elles ?
      - Quel genre de choses leur semble important ?
      - Par quoi sont-elles impressionnées ?
      - Quel genre de questions pourraient-elles poser ?
      - Quel est leur parcours ?`
      - Que savent-elles ?
      - Dans quel genre de situations interagissez-vous avec elles ?
      - Quel est votre rôle dans cette situation ?
      - Que voulez-vous accomplir ?
  - Ce que vous savez :
    - Poser la question "Quelles connaissances, expériences, qualifications et sagesse avez-vous qui sont pertinentes pour la situation et ces personnes ?""
    - Discuter lesquels le coaché souhaite partager avec le public cible.
  - Ce que vous faites :
    - Faire réfléchir le coaché sur :
      - sa première impression,
      - sa relation avec le public,
      - sa façon de communiquer,
      - son langage corporel,
      - sa façon d'influencer,
      - sa façon dont elle a interagi avec le public jusqu'à présent,
      - ce qu'il a déjà fait pour eux.
    - Demander quel genre de choses le coaché pense avoir un impact positif sur le public.
  - Niveau approprié et démonstration de confiance :
    - Poser les questions suivantes :
      - Comment démontrez-vous tout ce qui précède au public ?
      - Selon le public, quel est le niveau de confiance approprié ?
      - Comment cela pourrait-il s'exprimer ?
      - Comment le coaché se sent-il en confiance ?
      - A partir de ces informations, comment avoir l'impact en toute confiance que vous souhaitez ?
- Représenter les deux diagrammes de champ de force développés par Kurt Lewin :
  - Forces et limites :
    - Ce sont les choses qui se passent dans la tête du coaché, qui l'aideront à atteindre ou à freiner la réalisation des objectifs :
    - Les forces sont les choses déjà mises en place qui aident à atteindre les objectifs de crédibilité (exemple : la confiance, les qualifications, les connaissances de spécialiste, un résultat clair).
    - Les limiteurs sont les choses qui empêchent d'atteindre les objectifs de crédibilité (exemple : le manque d'expérience, fait une mauvaise première impression, le manque de connaissances de l'entreprise.
  - Opportunités et bloqueurs :
    - Ils sont dans l'environnement de travail du coaché.
    - Les opportunités sont des choses qui vont se produire ou que le coaché ​​pourrait faire se produire qui peuvent aider ​​à renforcer la crédibilité auprès du public cible (exemple : une prochaine réunion, un nouveau projet, un nouveau manager).
    - Les blocages sont des choses externes qui retiennent le coaché (exemple : aucun nouveau projet en vue, pas de financement pour la formation, mauvais manager, impossible d'obtenir un poste).
- Faire inscrire dans la zone Actions un plan d'action pour :
  - augmenter les forces et les opportunités,
  - réduire l'impact ou atténuer les limiteurs et les blocages.

### Conseil

Le modèle SLOBA peut être actualisé avec le temps au fur et à mesure que les actions sont menées.

### Durée

30 minutes
