---
title: "Accepter les critiques"
date: 2020-06-05 20:04:21
tags:
  - management
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Lors de la réception d’une critique, se demander si la personne qui la fait a raison.

En cas de doute, poser des questions (on parle d’enquête négative) : "Dites-m'en plus sur ...".

Cela permet :
- de découvrir le problème réel,
- se défouler un peu.

Se reposer la question de savoir si la personne a raison :
- si c’est le cas, remercier la personne pour ses critiques et en tirer des leçons;
- si ce n’est pas le cas, vous pouvez :
  - l'ignorer et dire : "Oui, vous avez probablement raison, peu importe."
  - prendre à partie la personne : "Non, je pense que c'est une critique injuste."
