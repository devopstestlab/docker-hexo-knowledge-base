---
title: "Renforcer la résilience"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Il est de plus en plus souvent admis que la résilience est une compétence essentielle, afin de pouvoir maintenir des niveaux élevés d'énergie, de confiance et de performance. Cet outil permet de renforcer les caractéristiques de résilience.

Le principe est :
- de se concentrer sur les forces identifiées,
- de les maximiser lorsqu'ils font face à des circonstances difficiles.
- de prendre conscience de ses lacunes en matière de résilience.

Cet outil aide à faire face au changement, aux attentes des autres et à aider lors d’une perte de confiance en soi.

### Processus

- Identifier l'état actuel de la situation à laquelle le client est confronté.
- Identifier son niveau d'inconfort ou d'anxiété.
- Clarifier les stratégies envisageables afin de briser les barrières et les blocages.
- Poser les questions du tableau et noter ses réponses.
- Explorer la manière dont le coaché peut tirer le meilleur parti des forces identifiées.
- Explorer sur trois ou quatre domaines.
- L’aider à identifier son niveau d'engagement à tester leurs meilleures options.

### Conseils

- Se concentrer sur ce qui est pertinent tout en convenant de ce qu'ils veulent faire en travaillant avec un apprentissage supplémentaire.
- Beaucoup de gens ne reconnaissent pas leurs lacunes en matière de résilience. Dans ce cas, vous pouvez poser des questions difficiles et tester toutes les hypothèses.
- Les gains les plus importants proviennent souvent de discussions sur des points ou des observations qui ne sont pas immédiatement soulevés.
- Les scores sont moins importants que la réflexion qui est générée à partir de la grille.

Evaluation de la force actuelle des caractéristiques de résilience : Identifier le niveau d'utilisation de chacune de ces caractéristiques de résilience, en particulier lorsque vous êtes sous pression et que vous subissez des niveaux de changement élevés (4 = Toujours, 3 = Fréquemment, 2 = Parfois, 1 = Rarement) :

| | 4 | 3 | 2 | 1 |
| ***Dans quelle mesure :*** |  |  |  | |
| Vous examinez votre véritable objectif |  |  |  | |
| Vous développez des objectifs et des résultats spécifiques |  |  |  | |
| Vous vérifiez ce que vous appréciez le plus et ce qui est important |  |  |  | |
| Vous contestez positivement ce qui pourrait être réalisé |  |  |  | |
| **Score Objectif (Purpose) :** |  |  |  | |
| ***Dans quelle mesure :*** |  |  |  | |
| Vous demandez de l'aide ou de l'aide à votre entourage |  |  |  | |
| Vous puisez dans l'expertise des autres avec des idées particulières |  |  |  | |
| Vous cherchez et parlez avec des personnes spécifiques qui peuvent penser différemment |  |  |  | |
| Vous recherchez et sondez les ressources que vous n'utilisez pas normalement |  |  |  | |
| **Score Réseau (Networking) :** |  |  |  | |
| ***Dans quelle mesure :*** |  |  |  | |
| Vous explorez de nouvelles options et alternatives |  |  |  | |
| Vous suivez ce qui a été accompli jusqu'à présent |  |  |  | |
| Vous vous concentrez sur ce qui est nécessaire pour l'achèvement |  |  |  | |
| Vous mettez plus d'énergie pour aller de l'avant immédiatement |  |  |  | |
| **Score Détermination :** |  |  |  | |
| ***Dans quelle mesure :*** |  |  |  | |
| Vous vous concentrez sur ce à quoi ressemblera le succès |  |  |  | |
| Vous identifiez ce qui est réalisable |  |  |  | |
| Vous clarifiez ce dont vous avez besoin de vous-même et des autres |  |  |  | |
| Vous vous rappelez ce qui a fonctionné pour vous dans le passé |  |  |  | |
| **Score Réalisme positif :** |  |  |  | |
| ***Dans quelle mesure :*** |  |  |  | |
| Vous souhaitez obtenir des commentaires des autres |  |  |  | |
| Vous regardez ce que vous avez appris jusqu'à présent |  |  |  | |
| Vous explorer des sources de nouvelles connaissances ou compétences |  |  |  | |
| Vous identifiez ce que vous pouvez contrôler ou influencer |  |  |  | |
| **Score Conscience de soi :** |  |  |  | |
| ***Dans quelle mesure :*** |  |  |  | |
| Vous entreprenez une activité pour augmenter votre énergie |  |  |  | |
| Vous trouvez le meilleur moyen de vous détendre et de recharger vos batteries |  |  |  | |
| Vous regardez comment vous pouvez utiliser au mieux votre temps disponible |  |  |  | |
| Vous considérez les forces vous avez que vous n'utilisez pas |  |  |  | |
| **Score Auto-gestion :** |  |  |  | |
