---
title: "Eviter une inflation de politiques"
date: 2020-06-04 22:04:21
tags:
  - management
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

La bureaucratie, bien nécessaire, doit avoir des limites, sous peine d’empêcher toute amélioration, tout changement.

La bureaucratie ne doit pas empêcher l’innovation.

Lorsqu’on rencontre un problème, un réflexe peut être de fier une nouvelle politique, une nouvelle règle. Il est préférable de rechercher une solution.

Et pour éviter qu’il n’y ait trop de règles, une solution peut être que pour en créer une nouvelle, une autre soit éliminée.
