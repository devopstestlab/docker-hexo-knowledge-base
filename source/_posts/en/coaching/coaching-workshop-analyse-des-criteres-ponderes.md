---
title: "Analyse des critères pondérés (weighted criteria analysis)"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

L’Analyse des critères pondérés (Weighted Criteria Analysis) permet de fournir des données objectives pour aider une équipe à prendre une décision.
Exemple : constitution d’une équipe avec les critères suivantes :
-	maîtrise de l’anglais,
-	maîtrise des technologies de collaboration, etc …
-	compétences techniques,

### Processus

- Lister les critères dans chacun des espaces de la colonne « Critères ». Les critères décrivent les objectifs souhaités.
- Attribuer un poids à chaque critère pour indiquer son importance (1 = le moins important, 10 = le plus important).
- Inscrire ce poids dans la colonne « Poids ».
- Lister les options identifiées par les participants dans les colonnes « Alternatives ».
- Evaluer les alternatives par rapport aux critères pondérés, pour chaque option par rapport à chaque critère :
  - Poser la question « Dans quelle mesure cette alternative satisfait-elle à ce critère ? ».
  - Donner un score (10 pour une satisfaction complète, 0 pour pas du tout).
  - Inscrire ce score dans la moitié supérieure de la case appropriée.
    - Calculer le score pondéré en multipliant le score par le poids du critère.
    - L’inscrire dans la moitié inférieure des cases.
    - Calculer le score pondéré total en additionnant les scores pondérés pour chaque alternative.

Plus ce score est élevé, plus l’option se rapproche des critères de réussite identifiés.
