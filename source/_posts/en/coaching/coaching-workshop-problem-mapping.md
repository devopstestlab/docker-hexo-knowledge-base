---
title: "Problem mapping"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Les problèmes ne sont jamais aussi simples qu'ils le paraissent.

Il est toujours utile de prendre des perspectives différentes pour essayer de trouver la cause première (root cause) du problème.

Examiner le problème sous différents angles permet de trouver de nouvelles idées et perspectives pour atteindre l’objectif.

### Processus

!(/images/problem-mapping.png)

- Les flèches indiquent :
  - où se trouvent les clients,
  - où ils veulent être à l'avenir,
  - comment y arriver.
- Ecrire l’objectif ou la solution souhaitée au présent (« Je veux… »).
- Dessiner la carte fléchée :
  - Commencer par l’orientation « futur ».
  - Poser des questions et encouragez les clients à compléter chaque quadrant.
  - Sonder en demandant « Autre chose ? »
- Penser au passé :
  - Identifier la façon de supprimer l'obstacle dans « passé ».
  - Pour les obstacles difficiles à surmonter, demander :
    - Quelles ressources avez-vous utilisé pour vous aider ?
    - Qu'est-ce qui vous a retenu dans le passé ?
    - Comment s'assurer que le même problème ne vous retiendra pas à l'avenir ?
- Penser au présent :
  - Quelles ressources sont actuellement à votre disposition ?
  - Et les finances ?
  - Y a-t-il autre chose à quoi vous devez penser dans le contexte de ce problème ?
- Réfléchir positivement aux autres problèmes que vous êtes en train de résoudre :
  - Comment le faites-vous ?
  - Quelles ressources avez-vous que vous pourriez utiliser à l'avenir ?
- Compléter le plan d'action en utilisant les éléments des quadrants.

### Conseils

- Il y a plus à dire sur certains quadrants que d'autres.

### Durée

20-30 minutes
