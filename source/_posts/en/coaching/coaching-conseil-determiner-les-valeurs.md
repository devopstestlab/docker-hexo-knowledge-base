---
title: "Déterminer les valeurs"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Les valeurs vous disent qui vous êtes et ce qui est important pour vous.

Comprend les valeurs des autres permet de comprendre ce qui motive leur comportement et pourquoi ils se sentent parfois en désaccord les autres.

La priorité de leurs valeurs peut changer au cours de la vie. Les valeurs elles-mêmes peuvent changer.

Utilisez ce tableau de valeurs possibles pour aider à identifier les valeurs :
- Acceptation
- Affection
- Aider les autres
- Amour
- Amusement
- Apprentissage
- Autonomie
- Avancement
- Aventure
- Beauté
- Changement
- Commande
- Compétence
- Compétitivité
- Confiance
- Contrôle
- Coopération
- Courage
- Créativité
- Célébrité
- Dignité
- Direction
- Défi
- Développement personnel
- Elégance
- Epanouissement personnel
- Excellence
- Excitation
- Famille
- Feedback
- Fidélité
- Grâce
- Harmonie
- Honnêteté
- Humour
- Indépendance
- Innovation
- Intégrité
- Invention
- Joie
- Justice
- Liberté
- Louange
- Paix intérieure
- Participation
- Plaisir
- Puissance
- Reconnaissance
- Relation amicale
- Respect de soi
- Responsabilité
- Richesse
- Résolution de problème
- Réussite (Achievement)
- Sagesse
- Santé
- Soins
- Spiritualité
- Succès
- Sécurité
- Sécurité économique
- Unicité
- Utiliser mes capacités
- Vitalité
- Vérité
