---
title: "Impact mapping"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

La cartographie d'impact, souvent utilisée pour le développement de produits, permet de planifier l'adoption d’Agile.

Cette technique permet :
- de communiquer clairement les hypothèses, ce qui permet d’aider les équipes à aligner leurs activités sur les objectifs commerciaux globaux,
- de prendre de meilleures décisions de roadmap.

### Processus

Pour créer une carte mentale (mind map), répondez aux questions suivantes :
- Définir l’objectif : Pourquoi fait-on ça ?
  - Définir un objectif SMART (specific, measurable, achievable, and agreed, realistic, and timed - spécifique, mesurable, réalisable et en accord, réaliste et limité dans le temps).
- Identifier les acteurs : Qui peut produire l'effet désiré ?
  - Qui peut le soutenir ?
  - Qui peut l’entraver ?
  - Qui en sera touché ?
- Déterminer comment vous souhaitez qu’ils changent d’approche : Comment le comportement des acteurs devrait-il changer ?
  - Comment les acteurs identifiés dans l'étape précédente peuvent aider à atteindre l'objectif ? Vous empêcher de réussir ?
- Examiner ce que vous pouvez faire pour soutenir l'effet souhaité : Que pouvons-nous faire pour soutenir l'impact ?
  - Que pouvez-vous faire pour réaliser les résultats souhaités et fournir les livrables ?
- Voter avec des étoiles pour les options individuelles concernant l'impact attendu.
- Réfléchir à la façon dont ces actions influencent votre carte.
  - Certains peuvent soutenir d'autres actions; certains peuvent faire le contraire.

Exemple : Améliorer la culture Agile, responsabiliser les gens, augmenter la motivation et stimuler la proactivité.
- Définir l’objectif.
  - Il doit être quelque chose que vous seriez fier de réaliser.
  - Le rendre mesurable afin de savoir quand il sera fait.
- Identifier les acteurs :
  - Une équipe de Scrum Masters, des membres de l'équipe et des managers.
- Déterminer comment vous souhaitez qu’ils changent d’approche :
  - Les Scrum Masters devraient lancer de nouvelles initiatives et ne pas se préoccuper de l’équipe et de ses obstacles.
  - Les managers devraient arrêter le micro-management de l'équipe car cela nuit à la culture.
- Examiner ce que vous pouvez faire pour soutenir l'effet souhaité :
  - Organiser une session Agile Friday pour partager des expériences.
- Présenter le modèle communautaire à l'entreprise.

Ce Mind Map n'est jamais terminé : mettez-la régulièrement à jour.

!(/images/impact_mapping.png)
