---
title: "Analyse du champ de force (Force Field Analysis)"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

L’analyse du champ de force permet de déterminer quelles forces aident et lesquelles entravent l'équipe.

Cette pratique repose sur le brainstorming, une analyse de cause à effet et une analyse pondérée (weighted analysis).

### Processus

Process (cas d’une réunion d'équipe virtuelle) :
- Définir ensemble les finalités souhaitées : une déclaration unique, spécifique et clairement écrite (exemple : « pour développer une bonne relation de travail avec les vendeurs »).
- Tracer une ligne au milieu du tableau blanc de haut en bas pour séparer le tableau blanc en deux parties égales.
- Intitulez la partie de gauche « Aide » et celle de droite « Entraves ».
- En équipe, identifier des éléments de chaque côté.
  - Exemple : Si vous inscrivez à droite « Sous-effectif », vous pourriez écrire du côté :
    - « Aide » : Données complètes montrant que votre équipe est sérieusement en sous-effectif par rapport à des équipes virtuelles similaires effectuant un travail similaire.
    - « Entrave » : Manque de compréhension de la part du top management.
- Pondérer les différents facteurs qui peuvent être influencés.
- Tracer des flèches plus longues pour les facteurs les plus importants ; tracer des flèches plus courtes pour les facteurs moins importants.

Le diagramme suggère une ligne de conduite.

Afin d’intégrer les variables les plus importantes de chaque côté, une réunion entre l’équipe et le top management semble être une bonne solution.

!(/images/force_field_analysis.png)
