---
title: "Le processus d'alignement"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cette technique permet d'aider à résoudre des expériences passées d'une personne et qui l'affectent toujours.

Cette technique se déroule dans le silence : donnez des instructions mais sans feedback verbal.

Elle provient du domaine de la PNL (programmation neurolinguistique).

Cette technique permet donc d'aligner le "soi" de la personne (ses expériences, compétences et connaissances actuelles) avec un moment de son passé où il n'a pas pu gérer une situation pour obtenir un résultat positif pour lui-même.

Ce genre de situation se concrétise par des phrases du type "J'ai toujours été comme ça".

Cette technique peut provoquer des réactions émotionnelles fortes.

### Process

1) Mise en place de l'outil

- Faire énoncer par la personne ce qu'elle veut - et pas ce qu'elle ne veut pas.
- Créer un objectif clairement défini.
- Faire parler du conflit qu'il a vécu.
  - Identifier ce qui se passe habituellement.
  - Demander ce qu'il en pense.
  - Demander de parler de la dernière fois que cela s'est produit.
  - Demander de raconter toute l'histoire.
- Faire des comparaisons entre leur réalité actuelle et leur nouvel objectif.
- Enoncer explicitement ce que la personne veut à la place.
- Identifiez ce qui ne fonctionne pas actuellement.
- Paraphraser le conflit en utilisant les mots de la personne.
- L'inviter à corriger cette paraphrase pour s'assurer qu'il reflète exactement ce que la personne vit.

2) Process dans la tête de la personne

Parler d'une voix douce et lente pendant au moins deux minutes afin d'atteindre un état de relaxation ciblée.
- Informer la personne que la suite va se passer en silence dans sa tête.
- Lui demander de suivre les instructions sans parler.
- Indiquer qu'il est normal de ressentir certaines émotions.
- Demander de remarquer les émotions qui se produisent sans se juger.
- Demander de s'asseoir confortablement et de se détendre, les yeux fermés.
- Demander de se concentrer sur la position de leur corps.
- Demander de se concentrer sur sa respiration.
- Demander à détendre ses muscles, à approfondir sa respiration, à se concentrer sur son inspiration, son expiration et sur la sensation de l'air qui passe par le nez ou la bouche.
- Demander de remonter le temps jusqu'au premier souvenir.
- Indiquer qu'il est normal de prendre son temps.
- Demander d'indiquer quand c'est fait en levant le doigt.
- Demander de créer dans son esprit une projection de l'événement.
- Demander de suspendre la scène au début.
- Demander de décider à quel âge cela s'est produit.
- Demander de démarrer la scène et de revivre cet événement.
- Demander de lever le doigt quand ce sera fait.
- Demander de revenir dans la scène en tant qu'un autre moi mais à l'âge adulte, avec ses compétences, connaissances et ressources actuelles.
- Demander de lever le doigt quand ce sera fait.
- Demander au moi adulte de communiquer avec le moi plus jeune pour lui donner des conseils et un soutien.
- Demander que le moi adulte de dire ce dont le moi jeune a besoin pour avoir l'esprit tranquille.
- Demander que les deux moi soient honnêtes l'un envers l'autre.
- Indiquer à la personne que lui seul sait ce qu'il faut dire pour donner confiance en soi au jeune moi.
- Après, demander au moi adulte de faire un câlin au plus jeune.
- Demander de dire ce qui est important pour lui permettre de guérir.
- Demander de ramener les deux mois.
- Demander d'ouvrir les yeux lorsqu'il est prêt.
- Indiquer à la personne qu'elle n'a pas besoin de comprendre et qu'elle a juste à faire confiance à son inconscient.
- Indiquer que le processus de guérison peut commencer : les choses sont différentes maintenant, la personne peut être ce qu'elle veut être; les gens l'accepteront et le respecteront.
- Indiquer à la personne que son inconscient continuera à travailler pour le réparer et l'aider à atteindre ses objectifs.
- Indiquer qu'il n'est pas nécessaire de parler de ce qui s'est passé dans son esprit. S'il le souhaite quand même, il faudrait en parler une autre fois.

### Conseils

Vous pouvez utiliser les temps de silance pour vérifier où vous en êtes dans le processus.

Rester silencieux pendant les périodes de silence.

Si la personne pleure, la laisser sans s'impliquer ni offrir de réconfort pendant le déroulement du processus.

Cette technique ne doit être utilisée que s'il y a un bon niveau de confiance entre vous et la personne.
