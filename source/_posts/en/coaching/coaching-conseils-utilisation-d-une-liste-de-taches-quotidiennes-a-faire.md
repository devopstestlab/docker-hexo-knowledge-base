---
title: "Utilisation d'une liste de tâches quotidiennes à faire"
date: 2020-06-05 22:04:21
tags:
  - coaching
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

La liste de tâches quotidiennes à faire se limite à cinq ou six choses urgentes à faire dans la journée. En rayant ces choses à faire chaque jour, vous vous sentez mieux.

A ce moment-là, vous pouvez ajouter des choses à faire plus importantes mais moins urgentes.

Idéalement, ces listes devraient être préparées la veille.
