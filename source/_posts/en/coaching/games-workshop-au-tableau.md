---
title: "Au tableau"
date: 2020-06-07 21:02:32
tags:
  - coaching
  - games
  - agile
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

## Informations pratiques

- Durée : 1h30
- Nombre de participants : 4 à 20 (formez des groupes de 4 à 6)

## Speech de présentation

"Dans ce jeu, vous allez construire un management visuel."

## Processus

Préparation :
- Imprimer la notice et la fiche de réalisation (http://www.agilex.fr/jeu-au-tableau).

Déroulement :
- Former des groupes.
- Leur fournir la notice de construction.
- Expliquer le contexte :

```
Voici une notice décrivant la situation d’avancement d’un projet à un instant t. Vous devez créer une représentation visuelle de cette notice sous la forme d’un tableau d’avancement lisible et créatif (avec des colonnes du style "Todo", "Doing" et "Done").
```

- Itération 1 :
  - Demander de construire le tableau en laissant libre cours à leur imagination (20 min).
  - Débriefer en groupe : chaque groupe présente son tableau (5 min) :
    - Pendant 2 min, les autres participants essaient de comprendre le tableau sans aucune aide.
    - Pendant les 3 autres min, le groupe répond et explique son tableau.
    - Cela permet de s'assurer que le tableau est compréhensible et lisible.
- Itération 2 : Faire une seconde itération (20 min).

## Feedback

- A la fin de l’atelier, vous aurez déjà brassé beaucoup de bonnes pratiques.
- Que retenez-vous de la construction d’un management visuel ? Qu’est-ce qui est le plus lisible selon vous, la notice ou votre tableau ?
- Quelles sont les bonnes idées avec lesquelles vous repartez ?

## Apprentissage

L’apprentissage émergera surtout de l’expérience vécue.

Néanmoins, vous pouvez appuyer sur l’importance de la lisibilité, de la simplicité du tableau.

## Sources

http://www.agilex.fr/jeu-au-tableau/
