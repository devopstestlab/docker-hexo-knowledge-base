---
title: "Implémenter le changement"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Pour implémenter le changement, il est important d’implémenter un environnement sûr.

La résistance est un comportement fréquent dans le changement. Ne pas trop forcer.

Avant tout, il faut bien comprendre pourquoi vous voulez changer la manière de travailler :
- Identifier les goulots d'étranglement actuels.
- Identifier les points forts actuels.
- Identifier les attentes du changement.

La Roue permet d'entamer une discussion plus large au sein de l'entreprise.

### Processus

- Examiner la situation actuelle pour chaque segment. Le milieu du cercle signifie « pas bon », les bords « grand ».
- Evaluez vos attentes dans le changement dans six mois
- Dessiner la Roue de l’Agilité :

!(/images/implementer-le-changement.png)

- Comparer les Roues d’Agile de chaque membre de l'équipe : sont-elles similaires ou différentes ?
- En discuter pour comprendre les raisons.
- Identifier les actions à entreprendre.

#### Modifier le comportement

Le framework ORSC décrit le changement comme un horizon : « L’horizon est la ligne entre le connu et l'inconnu ; c’est à la limite de ce que nous savons de nous-mêmes. Chaque fois que vous essayez un nouveau comportement ou une nouvelle idée ou perspective, vous traversez un horizon (Edge). Tant que les équipes et les individus grandissent et changent, il y aura toujours de nouvelles frontières et de nouveaux horizons à explorer ».

Chaque changement consiste à traverser plusieurs horizons. Pour cela, le coaching est important.

#### Huit étapes pour un changement réussi

Le process de l’implémentation du changement réussi en huit étapes passe par trois étapes :
- Préparer le terrain.
- Décider quoi faire.
- Fait en sorte que ça se produit.
- Le faire coller.

##### 1 - Créer un sentiment d'urgence

Tant qu’il n’y a pas de douleur dans les processus actuels, il n’y a pas besoin d’amélioration. C’est pourquoi il est important de créer un sentiment d'urgence.

Pour qu’il y ait changement, il faut une bonne motivation et une bonne raison.

Présenter les opportunités et les menaces.

##### 2 – Guider l’équipe

Au niveau individu, il est difficile de changer qui que ce soit. Pour cela, il faut s’appuyer sur des gens passionnés, bons communicants et leaders, que l’on appelera « early adopters ».

Encouragez-les à faire partie de votre équipe.

Diversifiez le groupe. Ce groupe doit être d’au-moins trois personnes.

##### 3 - Changer la vision

Créer une vision et une stratégie de changement.

Présenter le changement clairement et simplement pour que les gens le comprennent.

S’assurer que tous les membres de l’équipe de changement puissent l'expliquer en moins de cinq minutes.

L’objectif n’est pas Agile, mais quelque chose comme plus de flexibilité, une qualité accrue et une meilleure satisfaction client.

##### 4 - Comprendre et adhérer

La vendre aux autres malgré leurs craintes.

Ecouter, comprendre leur contexte et les remplir d'enthousiasme.

Faire preuve de patience car certaines personnes ont besoin de beaucoup de temps pour accepter une nouvelle idée.

##### 5 - Habiliter les autres à agir

Donner aux autres les moyens d'agir :
- Supprimer les obstacles afin de faciliter le changement.
- Constituer des équipes auto-organisées.
- Reconnaître les personnes qui ont pris des mesures pour changer.

##### 6 - Victoires à court terme

Utiliser des jalons pour célébrer en cours de route : démontrez fréquemment le succès.

Rendez-les simples pour pouvoir 

Célébrer ces succès tôt.

"C'est difficile et parfois même épuisant, mais nous allons y arriver."

Adapter la stratégie en fonction de l’évolution.

Etre transparent sur les échecs.

##### 7 – Ne pas lâcher.

Ne pas déclarer un changement réussi trop tôt, sous risque pour les personnes qui n’ont pas fait tout le chemin de revenir à leurs anciennes habitudes.

A partir du moment où l’on déclare le changement réussi, certaines personnes ne vont plus faire d’efforts. Or quelque soit le niveau atteint, les changements ne s'arrêtent jamais.

##### 8 – Créer une nouvelle culture

Faites de la nouvelle façon de travailler une partie intégrante de votre culture. L’équipe se définit par cette nouvelle manière de travailler.
