---
title: "Coaching pause-café"
date: 2020-05-14 20:02:45
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet d’aider les gens à résoudre des problèmes. Il est basé sur ORACLE et sur le brainstorming mais peut être utilisé dans un délai plus court.

Le principe est d’aider à voir qu’il faut assumer une part de responsabilité dans la situation et de montrer que vous pouvez faire quelque chose.

### Processus

- Quel est le problème ?
    - Donner des exemples particuliers et du contexte.
- Quel est le résultat ?
    - Dessiner une image aussi précise que possible de la façon dont les choses se passeraient si le problème était réglé.
    - Se concentrer sur le résultat plutôt que d'essayer de résoudre le problème.
    - Noter les idées.
- Quels sont les obstacles qui se situent entre le problème et le résultat ?
    - Regrouper-les en trois groupes sur des post-its :
        - les blocages qui existent chez ce client (exemple : manque de compétences, faible motivation);
        - les blocages qui existent dans d'autres (exemple : client anxieux, manager stressé);
        - Les blocages de la situation (exemple : ressources insuffisantes, retard).
- Faire un brainstorming sur les solutions pour contourner ces blocages et les étapes suivantes possibles.
    - Etudiez les avantages et les inconvénients de chaque solution trouvée.
    - Evaluer le succès potentiel de chacune sur une échelle de 1 à 10 (1 = le moins de succès; 10 = le plus grand succès).
    - Noter ces solutions pour atteindre le résultat.
- Concevoir une approche, un plan d’actions et un calendrier.
- De quel autre soutien pourriez-vous avoir besoin ?

### Conseils

Rester tourné vers l'avenir.

Si vous avez du mal à trouver des idées, le coach peut ajouter des suggestions, mais à la fin.

### Durée

15 minutes
