---
title: "Le framework Cynefin"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Le framework Cynefin, développé par David Snowden, permet de classer les problèmes et les situations afin de décider de l'approche à adopter.

Cynefin classe les problèmes en cinq domaines :
- Evident (obvious),
- Compliqué (complicated),
- Complexe (complex),
- Chaotique (chaotic),
- Désordre (disorder).

#### Evident (Obvious)

Lorsque le problème est simple, la solution est évidente.

Dans un tel cas, la démarche se résume à :
- reconnaître la situation,
- la catégoriser,
- appliquer une solution prédéfinie.

Exemples : l’intégration continue, les sprints, le Daily Scrum, …

#### Compliqué (Complicated)

Si le problème n’est pas si simple, une analyse.

Cette analyse permet de classer le problème.

Les experts proposent une solution.

Les bonnes pratiques sont éventuellement complétées.

Exemples : l’organisation du board Kanban, la création des éléments du Backlog, l’analyse des root causes,

#### Complexe (Complex)

Si le problème n’est ni simple ni compliqué, une analyse ne résout pas le problème.

Cela nécessite d’inspecter et d’adapter, comme le fait Scrum : vous tenez compte des expérimentations faites dans les sprints pour vous adapter pour les sprints suivants.

Un domaine peut être complexe mais avoir des situations simples ou compliquées.

#### Chatoque (Chaotic)

Le domaine chaotique est imprévisible : les tentatives de contrôle sont vouées à l’échec. Cela nécessite de trouver des solutions originales, non habituelles.

Exemple : Un bug critique de production nécessite l’arrêt de l’activité. Il doit être corriger immédiatement. La solution peut ne pas être la meilleure mais être fonctionnelle. Une fois corrigé en production, une meilleure solution peut être recherchée.

#### Désordre (Disorder)

Le domaine du désordre permet difficilement de classer la situation. Le réflexe est d’adapter l’approche habituelle, mais elle échoue souvent.
