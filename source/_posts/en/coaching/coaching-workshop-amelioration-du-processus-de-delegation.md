---
title: "Amélioration du processus de délégation"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet d’aider les gens à analyser leur hésitation à déléguer. La plupart du temps, cela est à cause de la perception du manager, qui peut être :
- « Personne ne peut accomplir cette tâche aussi bien que moi. »
- « Si je délègue cela, je perdrai le contrôle. »
- « La dernière fois que j’ai délégué, je n’ai pas obtenu ce que je voulais et je ferais donc mieux de le faire moi-même. »
- « Il est plus rapide de le faire moi-même que de prendre le temps de l'expliquer à quelqu'un d'autre. »

Le principe est d’encourager les managers à surmonter leurs craintes de déléguer et à examiner attentivement le succès de la délégation.

Un plan écrit aide cela.

### Processus

- Evaluez vos compétences de délégation, en particulier les raisons que vous donnez pour ne pas déléguer à d'autres.
- Discuter de ces raisons :
  - Sont-elles réelles ou perçues ?
  - Comment pouvez-vous surmonter ces problèmes ?
- Remplir le formulaire d’amélioration du processus de délégation :

| | Personne 1 | Personne 2 | Personne 3 |
|-|-|-|-|
| Quelle tâche dois-je déléguer ? | | | |
| Quel impact cela aura-t-il sur moi si je le délègue ? | | | |
| Qu'est-ce qui est critique dans cette tâche ? | | | |
| De quelle formation ou coaching ai-je besoin pour pouvoir exécuter la tâche ? | | | |
| Quelle est la chose la plus importante que j'ai apprise par le passé lors de cette tâche ? | | | |
                      
- A quel membre de l'équipe auquel puis-je déléguer le travail ?
- Quel serait l'impact sur sa charge de travail ?
- Réfléchir au type de formation ou de coaching dont le membre de l'équipe aura besoin pour effectuer cette tâche.
- Quelle est la chose la plus critique à propos de la tâche ?
  - Est-ce l'exactitude ou la garantie qu'elle est réalisée dans les délais ?
  - Est-ce que ce soit qu’elle contienne suffisamment de contenu ?
  - Comment vais-je transmettre ce message au membre de l'équipe ?
- Analyser mon expérience de la tâche.
  - Quelle est la chose la plus importante que j’ai apprise en l'exécutant (exemple : Ai-je pris un raccourci ? Une tâche spécifique devait-elle être effectuée en premier pour la faciliter ?
  - Communiquer ces points d'apprentissage au membre de l'équipe.
- Lors de la prochaine session :
  - Revoir le plan d'action pour vérifier les progrès.
  - Passer en revue les points d'apprentissage.

### Conseils

Etre très honnête sur les raisons pour lesquelles vous n’avez pas délégué la tâche au membre de l'équipe auparavant.

Questions pouvant être posées :
- Lorsque vous déléguez une tâche, quel type de résultats obtenez-vous ?
- Dans quelle mesure êtes-vous confiant que le membre de votre équipe offrira la bonne qualité ?
- Lorsque vous avez délégué une tâche, à quelle fréquence vérifiez-vous comment va votre membre d'équipe ?
- Qu'est-ce qui vous fait penser que votre façon de procéder est la bonne ?

### Durée

30 minutes
