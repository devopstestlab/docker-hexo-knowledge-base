---
title: "Priorisation des valeurs"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet de prioriser les valeurs d’une personne.

Cela permet de connaître les valeurs fondamentales d’une personne, ce qui influe la manière dont les gens prennent leurs décisions.

### Processus

- Inscrire vos valeurs dans la colonne de gauche.

| Valeurs | a - Apprendre | b - Intégrité | c - Changement | Total |
|-|-|-|-|-|
| a - Apprendre | | | | |
| b - Intégrité | | | | |
| c - Changement | | | | |

- Répéter ces valeurs dans le même ordre sur la ligne supérieure du tableau.
- Par exemple, si "apprentissage" est l'une de leurs valeurs, ils mettront "apprentissage" dans la case a de la colonne de gauche et la case a dans la ligne supérieure.
- Comparer tour à tour chaque valeur dans la colonne de gauche avec toutes les autres valeurs le long de la ligne supérieure.
    - Si la valeur a est plus importante que la valeur b, inscrire une coche.
    - Si elle est moins importante, inscrire une croix.
- Poser les questions du type :
    - La valeur est-elle plus importante pour vous que la valeur b ?
    - La valeur est-elle plus importante pour vous que la valeur c ?
- Additionner le score pour chaque valeur.
- Les valeurs avec les scores les plus élevés sont les valeurs considérées comme les plus importantes.

### Conseils

En cas de difficulté pour choisir entre deux valeurs, donner la première réponse qui vient à l’esprit.

Si ça ne marche toujours pas, demander : « Si c'était une question de vie ou de mort, que choisiriez-vous ? »

### Durée

20-30 minutes
