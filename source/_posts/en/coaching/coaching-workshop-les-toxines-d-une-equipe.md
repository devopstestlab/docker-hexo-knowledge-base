---
title: "Les toxines d’une équipe"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Voici quelques toxines que le Scrum Master devrait aider l’équipe à les identifier et à apprendre à se rendre respo,nsables de ne pas les utiliser.
- Blâme :
  - Il peut être tentant d’essayer de ne pas être responsable des défauts.
  - Type de phrase : « C'est la faute du Product Owner qui n’a pas bien écrit les User Stories. »
  - Il faudrait plutôt dire : « Cette User Story n'était pas bien écrite. Que faisons-nous pour que cela ne se reproduise plus ? »
- Défensivité (Defensiveness)
  - La défensive commence souvent en réponse au blâme.
  - « Ce n'est pas de ma faute ! »
  - Cela peut aussi arrive lorsque quelqu'un suggère un changement (exemple : changement de la durée des sprints).
- Mur en pierre (Stonewalling)
  - Le « mur de pierre » correspond à la situation où on répète encore et encore sa propre idée et on n’écoute pas les arguments des autres (exemple : réunion d’estimation, fractionnement des prises de décision).
- Mépris (Contempt)
  - Le mépris est différent de l'ironie, qui est un sens de l'humour.
  - Au-delà d’une certaine ligne, l’ironie devient sarcasme. Cela est particulièrement critique dans les équipes travaillant à distance.
  - Les phrases du type  « Oui, vous n'avez jamais fait d’erreur. » ne font qu’empirer la situation.
