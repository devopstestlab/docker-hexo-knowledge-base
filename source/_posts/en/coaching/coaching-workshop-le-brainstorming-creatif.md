---
title: "Le brainstorming créatif"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Le brainstorming créatif permet de stimuler la génération d'idées en changeant un élément dans la définition du problème.
- Définir le problème (que l’on appelle le "vrai problème").
- Définir un problème imaginaire en changeant un élément dans la définition du vrai problème.
- Générer des solutions au problème imaginaire.
- Appliquer les solutions au problème imaginaire au vrai problème pour voir si certaines sont applicables.
- Sélectionner les solutions finales.

Exemple :
- Le vrai problème : Problème majeur de rétention des employés
- Le problème imaginaire : Comment éliminer les chaussettes perdues dans le processus de lavage des vêtements.
- Une des solutions : Attacher les chaussettes par paire.
- Appliqué au vrai problème, la solution deviendrait par exemple : Instituer un programme de mentorat pour les nouvelles embauches.
