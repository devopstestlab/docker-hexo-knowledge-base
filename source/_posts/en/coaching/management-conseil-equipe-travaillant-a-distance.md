---
title: "Management d'une équipe travaillant à distance"
date: 2020-06-06 22:04:21
tags:
  - management
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Il est très important d’aider les équipes qui travaillent à distance à rester connectées et engagées.

Le choix de la technologie est important : logiciel de collaboration en ligne (Skype, Teams, …), email, téléphone. La communication doit être fiable et performante.

Renforcer le feedback sur l’avancement du travail de l’équipe et sur la vie de l’entreprise : email, vidéo, chat vidéo en direct, …

Créer des occasions de rencontre en face à face pour créer des relations solides (conférences, …).

Prendre un feedback de l’équipe une ou deux fois par an pour identifier des actions de formation qui pourraient l’aider à être plus efficace :
- types d'informations partagées,
- modes de communication utilisés,
- fréquence des communications,

Donner suite aux trois premières suggestions à tester.
