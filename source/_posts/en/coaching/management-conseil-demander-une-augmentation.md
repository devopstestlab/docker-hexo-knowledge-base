---
title: "Demander une augmentation"
date: 2020-06-06 22:04:21
tags:
  - management
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Vous ne devez pas simplement attendre une augmentation ni en demander une de temps en temps.

Pour les plus performants, la direction est plus proactive en termes d’augmentation. Mais elle peut parfois ne pas y penser ou les budgets peuvent être vraiment très tendus.

L’idée est donc d’établir un plan clair qui vous permettra de gagner l’augmentation.

Créer un dialogue informel et permanent sur une future augmentation.

Parler de vos performances et de vos attentes tous les deux ou trois mois lors de discussions plus formelles.

Discuter de votre évolution avec votre manager de temps en temps.

Comprendre comment il perçoit vos progrès.

Transmettre une idée claire de vos attentes.

Si vous pensez que vos performances ont dépassé vos attentes six à neuf mois depuis votre dernière augmentation, ne pas demander directement d'augmentation immédiatement mais :
- Exprimer votre désir d'obtenir une augmentation dans les six mois environ.
- Travailler avec lui pour définir les nouvelles compétences ou étapes spécifiques que vous devez atteindre.

Si vous recevez l'augmentation, c’est parfait. Si ce n’est pas le cas, votre CV a été étoffé, ce qui simplifiera une mutation ou un travail dans une autre organisation.
