---
title: "Comment admettre qu'on s'est trompé"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Lorsque nous avons tort, le réflexe est de ne pas l’admettre car :
- nous ne supportons pas l'idée que nous pourrions avoir tort,
- nous préférons éviter les conflits.

C’est plus facile de se taire et d'espérer que personne ne remarquera notre erreur ou de blâmer quelqu'un d'autre.

Or la vraie confiance et les conversations profondes ne peuvent se produire si vous ne pouvez pas admettre que vous vous êtes trompé.

Reconnaître avoir fait une erreur présente des avantages :
- Cela permet de créer des liens avec quelqu'un.
- Cela vous retire un fardeau.

### Conseils

- Reconnaître rapidement que vous vous êtes trompé (n’attendez pas des jours).
- Rencontrer la personne en privé : c’est une preuve de respect.
- Etre très précis sur ce que vous admettez et pourquoi vous avez dit ce que vous avez dit ou fait ce que vous avez fait.
- Expliquer brièvement pourquoi vous pensez avoir commis une erreur.
- Si nécessaire, s’excuser et faire amende honorable.
- Faire savoir que vous ne vouliez pas de mal et vous espérez certainement être plus prudent à l'avenir.
- Les remercier.
- Leur dire que vous appréciez leur écoute et leur compréhension.
- Sourire.
