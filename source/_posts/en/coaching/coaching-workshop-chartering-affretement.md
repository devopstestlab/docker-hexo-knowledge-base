---
title: "Chartering (affrètement)"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Cette pratique consiste à discuter de l'objectif de l'équipe.
- Demander à chaque participant de préparer une phrase de trois à cinq mots qui capture la raison principale de l'existence de l'équipe.
- Chaque participant partage sa phrase.
- Rechercher les points communs.
- Rechercher les points de désaccord qui doivent être résolus.
- Demander à un volontaire d'écrire une brève charte d'équipe qui incorpore les accords dans une déclaration.

Lors d’une autre réunion :
- Finaliser la déclaration en choisir bien les mots.
- Demander aux participants d'apposer leur signature sur la charte.

Afficher la charte sur le site Web ou dans le wiki.
