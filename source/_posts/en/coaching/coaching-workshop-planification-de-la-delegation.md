---
title: "Planification de la délégation"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Une délégation efficace nécessite d’avoir communiqué une planification pour s’assurer que ce qui doit être fait est fait, quand et comment.

Cet outil permet d’aider le manager à décider quoi déléguer et comment, lui libérant ainsi du temps pour faire d'autres choses.

### Processus

- Posez les questions :
  - Ramenez-vous chez vous du travail ?
  - Travaillez-vous plus longtemps que ceux qui vous entourent ?
  - Annulez-vous le droit au congé ou ne prenez-vous pas votre plein de quota de congé annuel ?
  - Travaillez-vous le week-end ou appellez-vous au bureau le week-end ?
  - Avez-vous accumulé des emplois inachevés ?
  - Devez-vous en faire plus avec moins de personnel ou moins de ressources ?
  - Demandez-vous où votre journée est passée ou ce que vous avez réellement accompli ?
- Examinez le nombre de réponses « oui ».
  - S’il y en a plus de quatre, certaines de vos tâches sont probablement déjà déléguées à d'autres.
- Identifiez les principales tâches et sous-tâches de votre travail.
- Si ces sous-tâches sont importantes, décomposez-les davantage jusqu’à identifier les tâches clés de votre travail.
- Ecrivez chaque tâche sur un post-its.
- Créez un tableau :

| Les autres | Les autres avec développement | Personne d'autre |
|-|-|-|
| | | |
		
- Dans la colonne 1, lister les sous-tâches que les autres membres de leur équipe sont capables de faire immédiatement.
- Dans la colonne 2, lister les sous-tâches que d'autres membres de l'équipe pourraient faire avec un certain développement.
- Passez en revue les notes qui restent :
  - S'agit-il vraiment de sous-tâches que personne d'autre ne pourrait faire ?
  - Déplacez les post-its restants dans la colonne 1 ou 2.
  - Déplacez ceux qui restent dans la colonne 3.
- S’il y a plus de notes dans la colonne 3, cela peut signifie que les connaissances sont concentrées dans une personne, ce qui peut présenter plusieurs problèmes.
- Placer des noms sur les sous-tâches.
- Sur chaque sous-tâche, attribuer une date pour le transfert des connaissances nécessaires à sa réalisation.
- Produire un plan de coaching individuel pour arriver au niveau requis.
- Remplir la feuille de planification de la délégation :
  - Quelle est la mission que vous souhaitez déléguer ?
  - Quelles sont les tâches impliquées dans la réalisation de la mission ?
  - A quoi ressemblera le résultat final ?
  - Quelle est la date d'achèvement souhaitée ?
  - Quel est le budget ?
  - Quelles sont les contraintes connues ?
  - Quelles méthodes alternatives avez-vous pour accomplir ces tâches ?
  - A qui souhaitez-vous déléguer la mission ?
  - Quel est le niveau de préparation de l'employé à assumer la tâche sur une échelle de 1 (le plus bas) à 5 (le plus élevé) ?
  - Quel est le niveau d'autorité que vous avez l'intention d'investir dans l'employé (autorité limitée, consultation sur les décisions avant de prendre des mesures, …) ?
  - Qu'est-ce qui vous dit que l'employé peut effectuer le travail selon la norme que vous attendez ?
  - Quel est le lien entre cette mission et les autres priorités que vous avez données à l'employé ?
- Suivre le framework de délégation :
  - Feedbacks :
    - Quelle différence cela ferait-il pour ma délégation?
    - Pourquoi avez-vous choisi cette personne ?
    - Avez-vous vérifié la charge de travail de la personne .? L’avez-vous aidé à redéfinir les priorités ?
    - Quelles sont les objectifs ?
    - Quels sont les livrables ?
    - Comment la tâche s'intègre-t-elle dans une vue d'ensemble ?
    - Quels sont les problèmes dans l'exécution de la tâche ?
    - Quels sont les pièges potentiels ?
  - Solliciter des commentaires :
    - La personne est-elle prête à assumer des responsabilités ?
    - Donner l'occasion de poser des questions.
    - Permettre à la personne de comprendre comment elle accomplira la tâche.
    - Expliquer à la personne comment exécuter la tâche si elle est totalement nouvelle pour elle.
    - Etablir des contrôles, des budgets et des délais.
    - Convenir quand et comment les examens auront lieu.
    - Assurez-vous que la personne a suffisamment d'autorité.
- Remplir la fiche de contrôle de la délégation efficace :
  - Savez-vous exactement ce que vous souhaitez déléguer ?
  - La personne a-t-elle la formation et l'expérience nécessaires pour effectuer la tâche ?
  - La personne est-elle disposée à faire la tâche ?
  - La personne vous a-t-elle décrit correctement l'objectif, signe qu’elle a bien compris les actions, les résultats et les normes dont vous avez besoin ?
  - La personne sait-elle exactement où et comment obtenir les ressources dont elle pourrait avoir besoin ?
  - La personne a-t-elle l'autorité nécessaire pour obtenir les ressources dont elle pourrait avoir besoin ?
  - La personne sait-elle quoi faire si quelque chose d'imprévu se produit ?
  - Laissez-vous la personne seule pour continuer ?
  - Utilisez-vous un langage clair et précis pour communiquer clairement le contexte de la mission et pourquoi la personne a été sélectionnée pour le poste ?
  - Avez-vous la capacité de bien vous engager, d'écouter et de discuter des avantages pour la personne et d'inviter sa réponse à l'initiative ?
  - Etes-vous en mesure de clarifier le résultat souhaité et d'encourager le questionnement ?
  - Travaillez-vous en collaboration avec le personnel pour discuter de ce qui doit être fait et comment ?
  - Etes-vous d'accord sur la portée de la mission (les méthodes de travail, les dates, le lieu, les budgets, les mesures de performance) ?
    - Avez-vous discuté et convenu du niveau d'autorité à attribuer à la mission ?

### Conseils

Vous pouvez aussi poser les questions suivantes :
- Ce travail doit-il être fait par vous ?
- Quelles sont les conséquences de la réalisation de cette tâche par quelqu'un d'autre ?
- Si vous faites ce travail, que devrez-vous retarder ou abandonner ?
- Que se passerait-il si ce travail n'était pas fait ?
- Qui d'autre pourrait faire ce travail ?
- De quoi cette personne aurait-elle besoin pour mener à bien cette tâche ?
- Quelle est l'importance de ce travail par rapport à votre rôle, vos tâches clés, vos objectifs et votre développement ?
- Qui d'autre est impliqué ou affecté par ce travail, et quelle est leur importance pour eux ?
- Si vous déléguez cette tâche, que feriez-vous du temps que vous libérez ?
- Comment allez-vous arrêter de vous retrouver dans cette situation ?

Selon le principe de Pareto, 80% de l'effet provient de 20% de la cause. Il peut se décliner comme ceci : 80% du temps permet de réaliser 20% des résultats.

Tenir un journal des temps recenscant les pertes de temps : les interruptions, le désordre, les visiteurs sans rendez-vous, les déplacements inutiles, l’email, les formalités administratives, la procrastination.

Encourager les membres de leur équipe à faire de même. Pour cela, utilisez la feuille de planification de la délégation pour amener cette demande de manière positive.

### Présentation

60 minutes
