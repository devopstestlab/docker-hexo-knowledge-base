---
title: "Eviter le blâme"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

Le blâme est naturel, mais il n'est pas productif.

Quand quelque chose ne va pas et nous avons contribué à provoquer cela, nous avons une tendance à blâmer les autres.

Le blâme est donc un processus partiellement vrai mais totalement improductif.

Quand un problème survient, en pratique, plusieurs sont impliquées, y compris soi-même. Mieux vaut donc prendre des responsabilités personnelles.

Considérez que ce qui vous arrive est créé en partie par vous.

Nommez le blâme, admettez votre rôle. Cela permet de devenir plus productifs.

Respirez profondément.

Formulez vos feedbacks de manière positive.

Recherchez les causes profondes. La réponse est très souvent un outil, un processus, une machine ou une autre ressource.

Parler des causes profondes permet de résoudre plus rapidement les problèmes et le rendre plus rare.
