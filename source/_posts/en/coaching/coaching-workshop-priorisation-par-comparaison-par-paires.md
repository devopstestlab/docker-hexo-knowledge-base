---
title: "Priorisation par comparaison par paires"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet d’identifier la priorité, c’est-à-dire l’important de l'urgent.

Pour cela, il aide à trier les tâches dans un ordre de priorité.

En pratique, c’est utile :
- lorsque vous vous sentez dépassé par le volume de tâches à réaliser;
- redonner un sentiment de contrôle sur les tâches à réaliser.
Cet outil utilise l'approche de comparaison par paires.

### Processus

- Les tâches doivent être triées par ordre de priorité.
- Identifier les tâches à problème à terminer.

| Tâches | a | b | c |
|-|-|-|-|
| a - Téléphoner au client | | | | |			
| b - Préparer une évaluation | | | | |
| c - ... | | | | |

- Dans la première colonne, inscrire les tâches.
- Reporter cette liste de tâches en haut.
- Chaque cellule est comparée avec la tâche figure dans la ligne et la colonne :
  - Si ces tâches sont identiques, alors la tâche à réalisée est simple.
  - Si elles sont différentes, inscrire celle qui est la plus importante.
- Compter la fréquence de chaque tâche.
- Les tâches les plus importantes sont celles avec la fréquence la plus élevée.

### Conseils

Ca peut être une bonne idée d’écrire les tâches au fur et à mesure de leur exécution.

En cas d’hésitation, inciter à prendre une décision à l’instinct.

### Durée

30 minutes
