---
title: "Briser l'impasse"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Définition

Cette technique permet de briser l'impasse dans laquelle on peut se trouver en identifiant une série d'étapes plus intermédiaires qui renforcent la confiance pour s'attaquer au sujet plus large.

Elle permet d'établir un plan d'action.

Cela se produit quand quelqu'un dit qu'il n'a pas le temps, d'aregnt ou d'énergie.

### Process

- Ecrire les mots "temps", "argent" et "énergie" en haut d'une feuille pour les garder en tête.
- Inviter le client à mettre de côté l'idée qu'il n'a pas assez de temps, d'argent ou d'énergie.
- Enoncer l'objectif d'une manière positive.
- Le noter sur la feuille, à droite.
- Poser la question "Pouvez-vous le faire aujourd'hui ?".
  - Si la réponse est "oui", le travail est terminé.
  - Sinon, poser la question "Que doit-il se passer en premier ?".
    - Identifier un maximum de quatre activités qui doivent se produire en premier.
    - Les dessiner sous forme de branches.
- Pour chacune de ces activités, reposer les questions jusqu'à ce que vous arriviez à une liste d'activités qui peuvent être faites aujourd'hui ou qui peuvent être programmées.

A la fin, vous obtiendrez une carte visuelle montrant le chemin pour atteindre l'objectif final.
- Obtenir un engagement à agir de la part de la personne : "Que doit-il se passer maintenant ? Quelle est la première étape ?".

Si vous remarquez des liens entre plusieurs actions, vous pouvez le faire remarquer.

### Durée

< 30 minutes
