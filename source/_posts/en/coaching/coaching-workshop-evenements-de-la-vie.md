---
title: "Evénements de la vie (life events)"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Le temps passe vite et nous ne sommes pas toujours conscients des choix que nous avons faits ou des événements qui ont eu un impact sur ces choix.

Cet outil permet de faire ressortir ces moments clés de votre vie pour comprendre l'impact qu'ils ont eu et comment vous pouvez les utiliser.

Le principe de cet outil est de puiser dans le subconscient en se posant la question « Quels événements ont façonné qui je suis ? ».

### Processus

- Cartographie des événements de la vie
    - Repenser au premier événement important dont vous vous souvenez.
    - Quel age aviez-vous ?
    - Quel était l'événement ?
    - Quel impact cela a-t-il eu sur vous ?
    - Quel était l’événement suivant important ?
    - Continuez jusqu'à votre âge actuel.

| Evénement | Age | Impact |
|-|-|-|
| | | |
| | | |
| | Age actuel | |

- Réfléchir sur ces événements
  - Quelles idées avez-vous ?
  - Comment pouvez-vous utiliser ces informations pour vous aider à prendre des décisions ?
  - Que leur dictez-vous sur ce qui est important pour vous ?
  - Répondre aux questions :
    - Qu'est-ce ou qui a influencé les décisions que vous avez prises ?
    - Quels thèmes ou modèles dans votre carrière ou votre vie à ce jour pouvez-vous identifier ?
    - Que vous disent ces thèmes ou ces modèles ?
    - Quel contrôle pensez-vous avoir eu sur votre vie ?
    - Vos expériences passées révèlent-elles quelque chose sur vos qualités personnelles, vos attitudes ou vos ambitions ? Si oui, quoi ?
    - Qu'est-ce qui vous a le plus aidé pendant les moments difficiles de votre vie ?
  - Quelles sortes de choses vous ont motivé ou démotivé ?
- Planification de carrière

### Conseils

- Le coaché peut être inspiré par d'autres réflexions et événements ajoutés après la session.

### Durée

60 minutes
