---
title: "Le sociogramme"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

L’analyse de sociogramme permet de documenter graphiquement les interactions des équipes lors d'une réunion et de discuter de leur bonne santé ou non.

### Processus

- Dessiner un cercle représentant l'équipe.
- Inscrire le texte « Tout ».
- Sur les bords, dessiner un cercle plus petit pour chaque membre de l'équipe.
- Y inscrire le nom de chaque membre.
- Chaque fois qu'un membre de l'équipe parle pendant la réunion, marquer le diagramme :
  - La première fois que la personne parle, dessiner une flèche de la personne qui parle :
    - à la personne à qui elle s'adresse,
    - au cercle « All » s’il ne s’adresse à aucune personne en particulier.
  - Si l’interaction ultérieure reprend, tracer une ligne hachée sur la flèche.

A la fin de la réunion, montrer le diagramme à l'équipe et posez des questions telles que :
- Quels sont les patterns que nous pouvons voir dans le sociogramme ?
- Certaines personnes sont-elles régulièrement exclues de nos conversations ?
- Certaines personnes interagissent-elles plus souvent avec certaines autres personnes ? Pourquoi ?
- Que montre le diagramme sur les leaders informels de l'équipe ?
- Certaines personnes sont-elles ignorées ou négligées lors de certaines discussions ?
- Nos interactions changent-elles en fonction du sujet dont nous discutons ? Comment ? Pourquoi ?
- Avons-nous des dynamiques liées à l’origine, à l’éducation, au travail, au genre, etc … ?

Par exemple, dans ce schéma, Bob a parlé à tout le monde (« Tous ») trois fois, à Mei deux fois et à Kathy trois fois. La seule personne qui lui a parlé directement était Carlos.

!(/images/sociogram.png)

Nous pourrions poser les questions suivantes :
- Cela signifie-t-il que Carlos a parlé à plusieurs personnes, mais que personne ne lui a parlé ?
- Devrions-nous nous inquiéter que Kathy n’ait fait aucun commentaire ?
- Est-il important que Mary et Mei se parlent six fois sans jamais s'adresser à quelqu'un d'autre ?
