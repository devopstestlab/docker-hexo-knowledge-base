---
title: "Brainstorming"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Le brainstorming permet de développer des solutions créatives.

Le principe est de se concentrer sur un problème et de proposer autant de solutions que possible.

Chaque idée est enregistrée, peu importe à quel point elle peut paraître bizarre, irrationnelle ou sans rapport avec le problème discuté.

Cette pratique permet aux participants de se nourrir les uns des autres.

Intégrer des personnes responsables des tâches connexes et des personnes extérieures afin de favoriser des idées et des perspectives nouvelles.

Idéalement, il devrait y avoir de cinq à dix participants.

### Processus

- Définir clairement le problème.
- Revoir les règles du brainstorming :
  - Toutes les idées sont valables, peu importe qu’elles soient bizarres ou sans rapport.
  - Alimentez chaque idée des autres.
  - Ne pas évaluer les idées avant la fin de la session.
    - Générer autant d'idées que possible.
    - Les noter sur des post-its accrochés au mur.
  - Voir une idée est ce qui permet souvent d’en générer une autre.
  - Ne pas reformuler ce qui est dit.
  - Poser des questions pour clarifier le sens de chaque idée.
  - Combiner des idées similaires.
  - Supprimer les idées redondantes.
  - Evaluer les idées restantes.
  - Eventuellement utiliser le processus de « multivote » :
    - Attribuer à chaque participant de trois à cinq votes.
    - Voter pour chaque idée de la liste.
    - Comptabiliser les votes.

Il est possible d’organiser une deuxième session de brainstorming après un certain temps.
