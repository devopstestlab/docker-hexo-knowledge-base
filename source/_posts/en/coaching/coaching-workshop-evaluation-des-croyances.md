---
title: "Evaluation des croyances"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

La confiance en soi permet d’aider à persévérer pour atteindre les objectifs.

Cette technique permet de comprendre les croyances qui se cachent derrière l'objectif, ce qui permet de comprendre ce qui est possible et réalisable.

Les croyances limitantes sont alors des obstacles qu’il s’agira de lever.

Les croyances limitatives peuvent refaire surface :
- Si le coaché dit qu'il ne peut pas atteindre l’objectif,
- Si le coaché s'engage à agir mais ne le fait pas.

### Processus

Sur une feuille d'évaluation des croyances :
- Ecrire l'objectif ou le résultat souhaité pour lequel il a une croyance faible, négative ou limitative.
- Ecrire le but ou le résultat à atteindre.
- Evaluer le degré de croyance dans le but par rapport à chacun des énoncés ci-dessous sur une échelle de 1 à 5 (1 = le plus bas degré de croyance, 5 = le plus haut) :
  - L'objectif est souhaitable et en vaut la peine.
  - Je pense qu'il est possible d'atteindre l'objectif.
  - Je crois que l'objectif est approprié et écologique.
  - J'ai les capacités nécessaires pour atteindre l'objectif.
  - Je sens que je mérite d'atteindre l'objectif.
- Tenir compte des questions suivantes :
  - Question 1 :
    - Que devez-vous savoir de plus ?
    - Que devez-vous ajouter à votre objectif ?
    - Que devez-vous croire afin d'être plus confiant ?
  - Question 2 :
    - Qui serait votre coach / mentor pour cette croyance ?
  - Question 3 :
    - Quel message ou conseil ce coach / mentor aurait-il pour vous ?
- Evaluer le degré de croyance dans l'objectif pour chacun des énoncés.
- Discuter des résultats en se concentrant sur les énoncés où les notes sont faibles.
- Demander ce qui a poussé le coaché à opter pour cette note particulière.
- Demander de répondre aux questions 1 à 3.
- Vérifier si l'une des notes a changé.

### Conseils

S’assurer que le coaché ne choisit pas des notes moyennes.

Demander de donner une réponse instinctive plutôt qu’analysée.

Pour découvrir ce qui se cache sous les croyances, posez les questions :
- Qu'est-ce qui vous empêche de sentir que vous méritez l'objectif ?
- Comment pourriez-vous rendre l'objectif plus approprié ?
- Qu'est-ce qui rendrait l'objectif plus souhaitable ?

### Durée

15-30 minutes
