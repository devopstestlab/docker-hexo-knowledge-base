---
title: "Auto-coaching"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

L’auto-coaching permet de se coacher en utilisant le principe d'ORACLE.

Le principe est d’encourager les gens à sortir d'un état émotionnel en se concentrant sur le fait qu'ils ont un problème qui doit être traité maintenant.

Pour cela, ce framework rappelle ce qu'ils doivent faire pour résoudre le problème.

### Processus

Partie 1 - Isoler le problème défi et identifier les options
- Identifier le dilemme (situationnel ou développemental) ?
- Posez une question qui précise ce que vous voulez réaliser.
- Noter les options sur des post-its.
- Identifier un plan et des actions pour résoudre le dilemme.

Partie 2 : Utiliser l'intuition personnelle pour vérifier que c’est adapté
- Décider si le plan d'action vous convient en utilisant votre intuition :
  - Penser à un moment de votre vie où vous avez pris une bonne décision ou vous êtes senti totalement en paix avec vous-même.
  - Se détendre, fermer les yeux et revivre cette situation avec tous vos sens.
- Noter ce que vous ressentez dans votre corps et vos sentiments.
- Penser à un moment où vous avez pris une décision qui n'était pas positive ou qui a mal tourné.
- Revivre mentalement une fois de plus cette situation.
- Remarquer où cette sensation s'installe dans votre corps (à l’estomac, sensation de lourdeur, …).
- Réfléchir en visualisant que vous avez déjà pris la décision. Notez ce que vous ressentez.

### Durée

10-40 minutes
