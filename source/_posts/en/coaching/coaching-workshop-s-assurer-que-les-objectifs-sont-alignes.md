---
title: "S'assurer que les objectifs sont alignés"
date: 2020-05-13 22:04:21
tags:
  - coaching
  - workshop
categories:
  - [coaching]
primary_category: coaching
primary_category_display_name: "Coaching"
---

### Présentation

Cet outil permet de comprendre pourquoi les gens qui ne savent pas si elles veulent vraiment un objectif ou qui échouent continuellement à atteindre leurs objectifs.

L’idée est que pour pouvoir atteindre un objectif, il faut vraiment le vouloir et en avoir la capacité.

### Processus

Répondre aux questions :
- Désir (Desire) :
  - Qu’est-ce que je désire ?
  - A quel point je le veux (sur 10) ? Si ce n’est pas 10/10, pourquoi ?
- Croyance (belief) :
  - Qu'est-ce que je crois? Dois-je croire que cela en vaut la peine?
  - Quelle est ma croyance sur 10? Sinon 10/10, pourquoi pas?
- Capacité (ability) :
  - Quelle capacité ai-je déjà pour y parvenir ?
  - Quelle capacité dois-je développer ?
- Répondre à la question : « Qu'est-ce que cela m'apprend sur mon objectif ? »
- Discuter des résultats.
   - Doit-il faire quelque chose pour augmenter son score ?
   - Doit-il repenser l'objectif ?
     - Dans ce cas, l’aider à réécrire l'objectif.
- Parcourir à nouveau les questions pour voir s'ils sont plus alignés.

### Conseils

- Ne pas influencer.
- Pour obtenir plus d'informations, demander « ce qui en ferait un 100 % ».

### Durée

30 minutes
