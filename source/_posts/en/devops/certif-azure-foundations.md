---
title: "test"
date: 2020-05-11 22:22:31
tags:
  - devops
  - cloud
  - azure
categories:
  - [devops]
primary_category: devops
primary_category_display_name: "DevOps"
---

# Concepts de base

La Haute Disponibilité signifie que les VMs peuvent changer rapidement en fonction des demandes de traitement.

La Fault Tolerance décrit la manière dont Azure s’assure qu’il n’y a pas d’interruption de service (zero downtime) pour les services fournis par Azure.

La reprise après sinistre (disaster recovery) utilise les métriques :
- time-to-recovery (temps de récupération) : quel est le temps nécessaires à la reprise ?
- « recovery point » (points récupération) : point du temps auquel les données sont reprises

La Scalability fait référence au scaling out et au scaling up.

L’Elasticité est la capacité d’augmenter et de diminuer rapidement le traitement et les ressources des ordinateurs.

L’Agilité signifie la capacité de développer rapidement, de tester et de lancer des applications.

En utilisant le bon budget, une société peut faire de gros changements :
- CapEx (Capital Expenditure) achète du matériel et paie à l’avance les achats en une fois.
- OpEx (Operational Expenditure) correspond aux frais courants nécessaires pour faire tourner le business. Cela correspond aux dépenses sur le Cloud. Le prix basé sur la consommation permet de ne payer que ce qui est utilisé.

Infrastructure-as-a-Service :
- infrastructure = serveurs actuels
- Scaling rapide
- Pas de propriété de matériel (VMs, serveurs, réseaux, bâtiments)
- fournit les serveurs, le stockage et le réseau comme service

Platform-as-a-Service :
- sur-ensemble de IaaS
- Supporte le cycle de vie des applications Web
- Evite les licenses de logiciel
- + middleware, outils
- inclut aussi le middleware comme les bases de données

Software-as-a-Service :
- fournit un service managé
- Paiement de frais d’accès pour l’usage
- Pas de maintenance ni de fonctionnalité « latest »
- + Apps
- exemple : Office 365.

Serverless :
- Azure Functions
- Extreme PaaS

Les différents types de modèle d’architecture sont :
- Cloud privé : C’est un cloud qui tourne sur votre propre matériel dans un emplacement de votre choix. Vous bénéficiez des avantages du cloud public mais vous pouvez le verrouiller. Ce type de cloud nécessite beaucoup de travail.
- Cloud public : C’est Azure, AWS, GCP. Ils ne nécessitent aucun coût pour la mise en oeuvre mais un coût mensuel d’utilisation. Vous avez peu de contrôle sur les services et l’infrastructure.
- Cloud hybride : Ce type de Cloud bénéficie des avantages des clouds privés et publics. Il peut cependant être complexe.

# Architecture Azure

## Région appairée

Chaque région est appairée dans une même zone géographique, sauf dans le sud du Brésil.

Si la région primaire est en panne, vous pouvez basculer sur la région secondaire.

Une seule région dans une paire de régions est mise à jour à la fois.

Certains services utilisent les régions appairées pour la réplication.

## Region

Une région est un ensemble de Data Centers déployés dans un périmètre défini et connectés par un réseau régional dédié à faible latence. Autrement dit, c’est un ensemble de Data Centers qui sont proches de manière à ce que le Data center dans lequel se trouvent les données n’est pas important. La latence est le temps nécessaire pour que les données circulent.

Pour minimiser la latence, choisissez la région la plus proche de vos utilisateurs.

Certaines fonctionnalités ne sont pas dans toutes les régions. De plus, le prix de ces services change d’une région à l’autre.

## Availability Zone

Dans une région, chaque zone a des installations différentes (alimentation, refroidissement, réseau) afin de protéger les données des pannes.

## Groupes de ressources (Resource Groups)

Les Resource Groups aident à structurer l’architecture Azure en regroupant des ressources.

Ces ressources peuvent être ajoutées à tout moment. Elles peuvent aussi être déplacées d’un groupe à l’autre.

Une ressource ne peut exister que dans un seul Resource Group.

Un Resource Group peut regrouper des ressources de plusieurs régions.

Un Resource Group bénéficie d’un contrôle d’accès pour les utilisateurs.

Les ressources d’un groupe peuvent interagir avec les ressources d’un autre groupe.

Un Resource Group se trouve dans un endroit précis : il y stocke des méta-données sur ses ressources.

## Resource Manager

Le gestionnaire de ressources (Resource Manager) permet de gérer les groupes de ressources (Resource Group) : déploiement, gestion et surveillance des ressources du groupe.

Le déploiement des ressources est toujours consistent.

Pour éviter un conflit des ressources, vous pouvez définir des dépendances entre elles.

Le Resource Manager bénéficie d’un contrôle d’accès pour les utilisateurs.

Les tags sur les ressources facilitent leur identification. Ils permettent aussi de facturer les groupes de ressources.

# Compute

## Virtual Machines

`Virtual Machines` met à disposition des machines virtuelles (Infrastructure-as-a-Service - IaaS) : vous gérez tout sauf le matériel.

`Azure blueprints` permettent de rendre les VMs conformes aux guidelines d'une société.

Azure fournit des recommendations d’amélioration concernant la sécurité, la haute disponibilité et les performances.

Vous pouvez choisir la quantité de RAM, le nombre de CPUs et le système d’exploitation (Windows ou Linux).

Les avantages sont :
- contrôle sur l’environnement ou la machine,
- permet l’installation d’applications spécifiques,
- permet de déplacer des ressources et des VMs de votre on-premise vers Azure.

Les inconvénients sont :
- éviter d’utiliser les VMs si un autre service Azure répondant à votre besoin existe déjà
- nécessite de faire la maintenant (mises à jour, patches, problèmes de sécurité)

Equivalent AWS : `EC2`

## Scale sets

Un Scale Set est un ensemble de VMs identiques. Il permet de créer et supprimer des VMs automatiquement pour une application.

Les avantages des scale sets :
- Permet de gérer plusieurs VMs identiques en utilisant un Load Balancer
- La Haute Disponibilité permet de s’assurer que dans le cas où une VM s’arrête, les autres VMs du Scale Set continuent à fonctionner.
- L’Auto Scaling permet de répondre automatiquement à la date en ajoutant ou en retirant des VMs depuis le Scale Set.
- Supporte jusqu’à 1000 VMs das un seul Scale Set.
- Pas de coût supplémentaire.

Equivalent AWS : `Auto Scaling`

## App Services

Les `Web Apps` sont des sites web et des applications en ligne hébergées sur Azure :
- Elles tournent sous Windows ou sous Linux.
- Elles supportent .NET, Java, Node.js, PHP, Python, Ruby.
- Supporte l’auto scaling et le load balancing
- Azure Integration simplifie le déploiement.

Les `Web Apps for Containers` permettent de déployer et d’exécuter des applications mises dans des containers dans Azure :
- Toutes les dépendances sont embarquées dans le container.
- Elles permettent un déploiement n’importe où avec une expérience consistante.
- Elles sont fiables entre les environnements.

Les `API Apps` permettent d’exposer et de connecter un backend de données :
- Elles n’ont pas d’interface utilisateur.
- Elles permettent de se connecter à d’autres applications par programme.

Equivalent AWS : `Elastic Beanstalk`

## Azure Container Instances

Azure Container Instances permet de lancer des containers pour traiter des charges de travail (workload). Cela permet de faire des économies en traitant les données à la demande. Il n’est donc pas nécessaire de gérer de VMs.

Equivalent AWS : `ECS`

## Azure Kubernetes Service

Azure Kubernetes Service permet d’orchestrer et de gérer les images de cotnainer et les applications. Il utilise des clusters et des pods pour faire le scaling et le déploiement des applications.

Ce service permet d’héberger et d’exécuter des containers sur Azure. Ce service permet de réutiliser votre architecture basée sur des containers en les gérant dans Kubernetes. Ce service permet d’utiliser les autre services Azure. Il est supporté dans toutes les régions Azure. Il supporte aussi les installations on-premise grâce à Azure Stack.

Equivalent AWS : `EKS`

## Azure Container Registry (ACR)

Azure Container Registry conserve les images de container valides. Il gère les fichiers et les artifacts pour les containers. Il est utilisé par `Azure Container Instances` et `Azure Kubernetes Service`. Il utilise les fonctionnalités d'identitié et de sécurité d'Azure.

Equivalent AWS : `ECR`

## Azure Functions

Azure Functions permet d’exécuter une simple fonction de calcule une fois puis de s’arrêter. C’est une offre serverless. C’est le plus petit service de calcul d’Azure. Elle peut être appelée via une simple URL. Elle permet d’éviter à installer ses propres applications et à gérer le serveur.

Ses avantages sont :
- La fonction n’est exécutée que quand il y a des données à traiter.
- Permet de faire des économies puis que vous ne payez que quand la fonction est utilisée.
- Résilience : si la fonction échoue, les autres instances ne sont pas impactées.

Equivalent AWS : `Lambda Function`

# Réseau

## Subnets

Les subnets :
- permettent de regrouper des ressources pour simplifier la vue d’ensemble.
- optimisent l’allocation d’adresses aux ressources

`Network Security Groups` permet de sécuriser les subnets.

## Virtual networks

`Virtual networks` (VNet) est un réseau virtuel. Il appartient à une seule région. Les ressources de ce VNet doivent se trouver dans la même région.

Il inclut une portée d’adresses IP et de subnets.

Un VNet appartient à un seul abonnement (subscription). Un abonnement peut avoir plusieurs VNets.

Les avantages du VNet :
- le scaling en simplifiant l’ajout de VNets et d’adresses
- la Haute Disponibilité grâce au VNet peering, Load Balancer et VPN Gateway
- l’isolation grâce aux subnets et aux Network Security Groups.

Un subnet est un réseau plus petit faisant partie d’un VNet.

Equivalent AWS : `VPC`

## Load Balancers

Le Load Balancer permet de distribuer le traffic provenant d’Internet ou du réseau local (inbound flows) qui arrive sur le frontend (point d’accès où le traffic arrive) du Load Balancer vers les instances du backend pool (ce sont les instances de VM recevant le traffic) en fonction des règles et des sondes de bonne santé (health probes), qui permettent de s’assurer que les instances de backend peuvent recevoir les données.

Equivalent AWS : `ELB` (layer OSI 4 : `NLB`)

## Virtual network gateway

`VPN Gateway` permet de connecter les réseaux virtuels Azure à d’autres réseaux virtuels Azure ou à des réseaux locaux clients (site à site). Il permet aux utilisateurs de se connecter à Azure via un tunnel VPN (point à site).

Le VPN Gateway permet de connecter le réseau Azure avec le réseau on-premise de manière sécurisée.

??? C’est une gateway de réseau virtuel spécifique qui comprend au moins deux VMs dédiées : VNet Gateway + VPN Gateway (rôle joué par un VPN).

Ce service envoie les données cryptées entre Azure et le réseau on-premise.

Ce service nécessite un subnet d’Azure Gateway, un tunnel de sécurité et une gateway on-premise.

Equivalent AWS : `VPN Gateway`

## Application gateway

Une Application Gateway est un Load Balancer de niveau supérieur :
- Au lieu de fonctionner au niveau des adresses IP et des ports, il fonctionne au niveau des requêtes HTTP.
- Il distribue le traffic entrant selon l’URL et les entêtes de l’hôte.
- Le traffic d’une même session peut être géré sur plusieurs serveurs.
- Il est adapté à la plupart des autres services Azure.

Ses avantages sont :
- Auto scaling en fonction de la quantité de traffic reçu
- cryptage possible jusqu’au backend
- amélioration de la résilience aux pannes en se répartissant sur plusieurs AZs
- permet d’héberger jusqu’à 100 sites web

Equivalent AWS : `ELB` (layer OSI 7 : `ALB`)

## CDN

Le CDN (Content Delivery Network) stocke une version cachée de l’application sur un noeud Edge. C’est un réseau distribué de serveurs qui peuvent délivrer du contenu Web proche des utilisateurs.

Les avantages sont :
- amélioration de l’expérience utilisateur et de la performance de l’application
- scaling pour s’adapter aux pics de traffic
- protection de l’instance de serveur backend principal contre les charges importantes
- les serveurs Edge servent les requêtes le plus proche des utilisateurs : il y a donc moins de traffic envoyé au serveur hébergeant l’application

Le cache est une collection de copies temporaires des fichiers d’origine. L’objectif principal est d’optimiser la vitesse de l’application. Quand  une copie expire, une nouvelle est demandée.

Origin Server est l’emplacement d’origine des fichiers. C’est la copie maître de l’application.

Equivalent AWS : `CloudFront`

# Stockage

Un `Blog` se trouve dans un container, qui se trouve dans un compte de stockage.

Les types de `Blog` sont :
- `block` : stocke des données texte et binaire sous forme de blocs (jusqu’à 4,7 To); un bloc est un stockage général de données;
- `append` : blocs optimisés pour les opérations d’ajout (adapté par exemple aux logs);
- `page` : stocke des fichiers de jusqu’à 8 To; il est possible d’accéder à n’importe quelle partie du fichier à n’importe quel moment (exemple d’usage : disque dur virtuel).

Le pricing dépend du type d’accès :
- `Hot` : accès fréquent; temps d’accès plus bas; coûts d’accès plus élevé
- `Cool` : coûts de stockage plus bas; temps d’accès plus élevé; les données restent au moins pendant 30 jours
- `Archive` : les coûts de stockage les plus bas; temps d’accès plus élevés

Equivalent AWS : `S3`

## Disk

`Disk` est un disque attaché généralement à un VM.

Les disques managés sont managés par Microsoft. Il y a une garantie sur la taille et la performance.

Il est possible de migrer la taille et le type de disque facilement.

Les types de disque sont :
- `HDD` : disque dur; coût faible; adapté aux backups
- `Standard SSD` : standard pour la production; fiabilité plus élevée; scalabilité; latence plus faible que HDD
- `Premium SSD` : plus rapide; meilleure performance; latence très faible; adapté aux charges de travail critiques
- `Ultra Disk` : pour les charges de travail (workloads) les plus demandées, les plus intensives; jusqu’à 64 To

Equivalent AWS : `EBS`

## File

`File` permet de mitiger les solutions de stockage de fichier on-premise. Il est utilisé pour avoir un stockage partageable en Haute Disponibilité et super résilient.

`File` répond aux problématiques suivantes :
- quantité de stockage limitée
- temps et resources dépensées sur la maintenance des backups
- difficulté de garder en sécurité les données tout le temps
- le partage de fichier peut être difficile parmi plusieurs équipes et organisations

Les avantages de `File` sont :
- partage d’accès au stockage Azure File sur des machines
- fournit un accès à l’infrastructure on-premise
- managé
- résilient : les pannes réseau et d’alimentation n’affectent pas le stockage

Il y a deux types de scénarios :
- hybride : complète ou remplace la solution de stockage de fichiers on-premise existant
- left and shift : déplace les stockages de fichier existants et les services liés vers Azure

Equivalent AWS : `EFS`

## Storage archive access tier

`Storage archive access tier` est un moyen très bon marché de stocker une quantité massive de données. C’est aussi un type de stockage `Blog`.

Les Policies, la législation et la récupération (recovery) peuvent être des exigences pour l’archivage des données.

Le tier Archive est le moyen de stockage le moins cher, il est durable, crypté (donc sécurisé) et stable. Il est adapté aux données avec un accès peu fréquent.

C'est un stockage bon marché permet de débloquer du stockage on-premise Premium.

Equivalent AWS : `Glacier`

# Bases de données

## Cosmos DB

`Cosmos DB` est une base de données multimodèle distribuée globalement prenant en charge les modèles de données clé-valeur, documents, graphiques et colonnes.

Elle peut être facilement réparti sur plusieurs régions. L'ajout d’une région se fait avec un seul clic. La synchronisation entre les régions est continue.

- scalabilité automatique
- ressources infinies
- seules les ressources utilisées sont facturées
- intègre du Machine Learning pour :
    - suggestions sur comment optimiser et améliorer les performances des instances Azure SQL
    - avertissements (warnings) en cas de dégradation d’instances et si quelque chose d’inhabituel se produit

Attention : les coûts peuvent augmenter rapidement.

Equivalent AWS : `DynamoDB`

## Azure SQL

Azure SQL est une base de données Microsoft SQL stable et managée compatible avec les serveurs SQL on-premise.

Les avantages :
- scalable
- peut gérer des bases de données de jusqu’à 100 To
- bénéficie des fonctionnalités de sécurité d’Azure

Equivalent AWS : `RDS`

## Azure Database for MySQL

MySQL est une base de données relationnelle Open Source mature et stable.

Ses avantages sont :
- managé par Microsoft Platform-as-a-Service)
- permet de se focaliser sur les besoins métiers au lieu des serveurs et réseaux
- supporte de nombreux langages
- Haute Disponibilité
- supporte les fonctionnalités de Azure Security
- gère automatiquement les patchs de base de données, les backups automatiques et la surveillance

Equivalent AWS : `RDS`

## Azure Database for PostgreSQL

PostgreSQL est une base de données relationnelle Open Source similaire à MySQL adaptée aux besoins des entreprises (applications financières, aux gouvernements et aux usines pour lesquelles les temps d’arrêt sont désastreux).

Ses avantages sont :
- dispose de nombreuses extensions (JSONB, fonctions géospatiales, ...)
- scaling horizontal grâce aux datasets distribués
- fournit des recommandations de performance en fonction de l’usage de la base de données
- managé par Microsoft

Equivalent AWS : `RDS`

## Database Migration Service

`Database Migration Service` permet de migrer tout type de base de données vers Azure SQL.

Il intègre des guides complets pour la migration. Supporte les bases de données non Microsoft.

Equivalent AWS : `Data Migration Service`

# Authentification et autorisation

## Active Directory

Attention : Active Directory est autre chose qu'Azure Active Directory.

`Azure Active Directory` (AAD) est obligatoire dans un compte Azure. Un compte Azure doit avoir au moins un utilisateur.

Equivalent AWS : `IAM`

### Tenant

Un locataire (Tenant) représente une organisation. C’est l'instance d’Azure Active Directory qui est fournie lors de l’inscription à Azure.

Chaque Tenant est distinct et complètement séparé des autres Tenants.

Chaque utilisateur dans Azure ne peut appartenir qu’à un seul Tenant.

Les utilisateurs peuvent être invités par d’autres Tenants.

### Subscription

L'abonnement (subscription) permet de facturer ensemble toutes les ressources d’un abonnement.

Vous pouvez avoir plusieurs abonnements pour un Tenant afin de séparer les coûts.

Si un abonnement n’est pas payé, toutes les ressources et les services associés sont arrêtés avec l'abonnement.

`Azure Active Directory` permet de gérer les utilisateurs dans un Cloud hybride.

L’authentification Multi-Factor est une couche de sécurité supplémentaire utilisant :
- quelque chose que vous connaissez,
- quelque chose que vous avez,
- quelque chose que vous êtes.

# Solutions

## Internet of things

IoT est un réseau de devices connectés qui fonctionnent sans intervention humaine.

### IoT Hub

`IoT Hub` est une solution PaaS qui fournit plus de contrôle sur la collecte et le traitement des feeds de données IoT.

Il est managé et sécurisé.

Il permet un déploiement facile et prend en charge le scaling et l'authentification.

Equivalent AWS : `IoT`

### IoT Central

`IoT Central` est une solution SaaS qui simplifie et accélère l’implémentation de la solution IoT intégrant des connecteurs pré-construits prêts à utiliser, des templates et des dashboards.

Aucun codage n’est nécessaire : vous recevez des flux des devices et vous vous focalisez sur les métriques et la valeur métier.

## Big Data

Un Data Lake est une grande quantité de données. On parle de Big Data.

Les avantages du Big Data sont :
- vitesse et efficacité du traitement de grandes quantités de données
- réduction des coûts de stockage et de traitement
- permet une meilleure prise de décision grâce à un traitement des données immédiat et à une analyse en mémoire
- meilleure compréhension de ce que les clients veulent pour fournir de nouveaux produits et services

### Data Lake Storage

`Data Lake Storage` permet de stocker massivement des objets `Blog` de manière évolutive et sécurisée.

Equivalent AWS : `EMR`

### Data Lake Analytics

`Data Lake Analytics` permet de faire de l’analytique de données sur un Data Lake. Il supporte le traitement en parallèle. Il n’y a rien à installer.

Equivalent AWS : `Kinesis Analytics`

### HDInsights

`HDInsights` permet de déployer et gérer des clusters Hadoop dans Azure.

Ce service Hadoop managé est basé sur les frameworks Open Source Spark, Hive, LLAP, Kafka, Storm, R, ...

Il permet de traiter facilement et rapidement des quantités massives de données.

Equivalent AWS : `EMR`

### Databricks

`Databricks` est une plateforme d’analyse basée sur Apache Spark. Elle fournit une puissance de calcul permettant de traiter des datasets sur plusieurs ordinateurs en parallèle. Il s’intègre aux autres services Azure Storage.

Equivalent AWS : `EMR`

### Synapse Analytics

`Synapse Analytics` permet d'exécuter rapidement des requêtes complexes sur plusieurs pétaoctets de données grâce au traitement massivement parallèle (MPP).

Il permet de faire du reporting et de l’analyse de données. Il est utilisé par Azure SQL Data Warehouse. Il dispose d’un langage SQL.

Equivalent AWS : `Redshift`

# Machine Learning/AI

## Machine Learning

`Machine Learning` permet d’apprendre, de déployer, d’automatiser et de gérer des modèles de Machine Learning.

Le modèle définit ce que l'application de Machine Learning apprend. C’est un ensemble de règles décrivant comment utiliser les données fournies. Les modèles trouvent des patterns basés sur les règles.

Les règles et les modèles permettent d’entraîner l’implémentation d’IA.

L’extraction de connaissance (Knowledge Mining) utilise `Search services` pour trouver des idées dans vos données (relations, connexions géographiques, …).

Azure intègre des applications intégrées que vous pouvez utiliser pour le Machine Learning et l’IA (services cognitifs, services bot).

Equivalent AWS : `SageMaker`

## Cognitive Services

`Cognitive Services` permet de reconnaître, d'identifier et de sous-titrer des vidéos et des images de manière automatique.

Les applications prennent des décisions en fonction du contenu (langage vulgaire anomalies, ...).

Equivalent AWS : `Rekognition`

## Services speech

`Services speech` permet de transcrire une conversion speech-to-text automatique.

Equivalent AWS : `Transcribe`

## Machine Learning Studio

`Machine Learning Studio` permet de créer un environnement de travail supportant les outils Azure Machine Learning.

Il comprend des modules intégrés. Adapté aux scénarios comme l’analyse du sentiment sur Twitter, le groupement de photos et les recommendations de film.

## Machine Learning

`Machine Learning` permet de créer espace de travail pour développer des applications d’IA utilisables presque n’importe où. Azure reconnaît les tendances dans l’application et crée les modèles pour vous.

# Serverless

Serverless ne signifie pas qu’il n’y a pas de serveur mais qu’il n’y a pas besoin de gérer le serveur.

## Functions App

`Functions App` permet de développer des fonctions serverless qui sont appelées quand une tâche est réalisée. Elle meurt ensuite. Elle n’est exécutée qu’une seule fois pour chaque appel.

C'est le premier service serverless d’Azure.

Equivalent AWS : `Lambda Functions`

## Logic Apps

`Logic Apps` permet de créer des applications distribuées à l’aide de connecteurs fournis en connectant les applications, les données et les équipements.

Il permet de connecter des systèmes dans et hors de la plateforme Azure. Ce service intègre des applications et des flux de données, des services et des systèmes.

Il automatise la planification, les tâches d’orchestration et les processus.

Aucun codage n’est nécessire pour démarrer.

Equivalent AWS : `Function Steps`

## Event Grid

`Event Grid` permet de router des événements entre les applications à l’aide d’un modèle de publication/abonnement.

C’est un service serverless facile à utiliser.

Equivalent AWS : `SNS`

## DevOps

DevOps intègre les services suivants :
- `Boards` : tableau de bord des tâches à réaliser, des timelines, des issues, des plannings, ...
- `Pipelines` : build et test automatique du logiciel
- `Repos` : stockage du code source de l’application de manière sécurisée d’une manière managée
- `Test Plans` : conception des tests des applications
- `Artifacts` : partage des applications et des bibliothèques de code avec d’autres équipes dans et hors de l’organisation

Equivalent AWS : `CodeDeploy`, `CodeBuild`, `CodeCommit`, `CodePipeline`

## Azure DevTest Labs

`Azure DevTest Labs` permet de créer des environnements pour le test et le développement à partir de templates de déploiement.

Il n’y a pas de surprise sur les coûts.

Equivalent AWS : `AWS Device Farm`

# Sécurité

## Firewall

Firewall est un parefeu qui permet de contrôler les données entrantes et sortantes d’un réseau basé sur des règles.

Les règles définissent le type de traffic qui peut et ne peut pas accéder au device ou au service derrière lui.

Il existe des variations : ce sont des versions de matériel et de logiciel qui peuvent être adaptés à tout type et taille de réseau.

Equivalent AWS : `WAF`

## DDoS Protection Service

`DDoS Protection Service` permet de protéger des attaques DDoS sans interruption temps d’arrêt.

Equivalent AWS : `Shield`

## Network Security Group

`Network Security Group` (NSG) permet de protéger un subnet ou une machine virtuelle. Il fonctionne comme un firewall de ressource. Il peut être attaché à un Virtual Network, un subnet ou une interface réseau (Network Interface).

Les règles de traffic entrant et sortant permettent de déterminer qui peut accéder aux ressources attachées.

Equivalent AWS : `Security Groups`

## Application Security Groups

`Application Security Groups` (groupes de sécurité d’application) permettent de protéger les machines virtuelles en les regroupant par fonction des applications plutôt que par leurs adresses IP.

Ils groupent les machines virtuelles et les VNet pour appliquer des règles de sécurité.

## Security Center

`Security Center` permet d’évaluer la sécurité de manière automatique en émettant des alertes de menace : les vulnérabilités des applications, leurs écarts par rapport aux meilleures pratiques, ...

`Security Center` est adapté aux architectures hybrides et s’intègre avec d’autres fournisseurs de cloud.

Il intègre des politiques (policy) et des métriques de conformité. Il intègre un score de sécurité afin de favoriser une grande hygiène de sécurité.

Chaque machine virtuelle a un agent installé qui envoie les données.

Pour l’utiliser :
- définir des politiques (policies) pour surveiller les ressources : une policy est un ensemble de règles permettant d’évaluer une ressource; il existe des policies prédéfinies
- protéger les ressources en surveillant les policies et le résultat de ces politiques
- répondre aux alertes de sécurité : l’investigation aboutit à la définition de nouvelles politiques pour prendre en compte l’alerte

Equivalent AWS : `Inspector`

## Key Vault

`Key Vault` permet de gérer, de créer et de contrôler les clés de chiffrement stockées dans des modules de sécurité matériels (HSM).

`Key Vault` permet de conserver les secrets d’une manière isolée. Il s’appuie sur du matériel. Microsoft ne peut pas accéder à ces secrets. Les applications ne peuvent pas y accéder. Ce service bénéficie d’un scaling global.

Equivalent AWS : `KMS`, `CloudHSM`

## Azure Information Protection

`Azure Information Protection` permet de classer et de protéger les documents et les emails en appliquant des labels. Ces labels peuvent être appliqués automatiquement à l'aide de règles et de conditions.

Ce service permet de partager les informations (documents, emails, données en dehors du réseau de l’entreprise) de manière sécurisée :
- les données sont classées en fonction de leur sensibilité, soit manuellement, soit automatiquement en utilisant des policies
- trace ce qui arrive aux données partagées et révoque l’accès si nécessaire
- partage les données en toute sécurité de manière à contrôler qui édite, visualise, imprime et transfert ces données
- intègre le contrôle d’accès aux documents dans les applications communes comme Microsoft Office

## Advanced Threat Protection (ATP)

Advanced Threat Protection (ATP) permet de :
- surveiller l’activité et les informations des utilisateurs (y compris les permissions et mes adhésions aux groupes),
- enregistrer le comportement normal et la routine de l’utilisateur : les activités hors de cette routine sont tracées comme suspicieuses
- suggérer des changements pour se conformer aux bonnes pratiques de sécurité afin de réduire les risques

ATP identifie des activités suspectes et des attaques sur toute la chaîne cybercriminelle (Cyber-Attach Kill-Chain) :
- recherche d’informations d’un utilisateur sur d’autres utilisateurs (reconnaissance),
- identification et flag des tentatives de deviner les credentials d’un utilisateur (brute force),
- flag des tentatives d’un utilisateur de gagner plus de privilèges (exemple : login d’un autre utilisateur) (augmentation de privilèges).

Equivalent AWS : `Guard`

# Privacy, compliance et trust

## Azure Policy

`Azure Policy` permet de créer, affecter et gérer des stratégies (policies) sur les ressources. Cela permet de s'assurer qu'elles restent conformes aux normes et aux SLAs de l’organistion.

Equivalent AWS : `Organisations`

## RBAC

`RBAC` (Role-Based Access Control) permet de définir un accès utilisateur à une ressource. Il permet de fournir un accès minimum nécessaire aux ressources pour s’assurer que seuls les utilisateurs avec un accès valide peuvent gérer les ressources. Il est important d’être explicite sur les utilisations et l’accès, par exemple :
- permettre un accès à l’application à certaines ressources
- permettre à un utilisateur de gérer les ressources dans un groupe de ressources.

Equivalent AWS : `Organisations`

## Rôle

Un rôle est une combinaison de :
- Principal de sécurité (Security Principal) : c’est un objet qui représente un utilisateur ou un groupe qui peut accéder à la ressource
- Définition du rôle (Role Definition) : c’est une collection de permissions (lecture, écriture, suppression)
- Scope (Scope) : ce sont les ressources auxquelles l’accès s’applique; spécifier le rôle qui peut accéder à une ressource ou à un groupe de ressources

## Verrouillage des ressources

Le verrouillage des ressources permet de s’assurer que les abonnements, les groupes de ressource ou les ressources ne peuvent être ni modifiés ni supprimés.

Ce verrouillage peut être :
- `delete` : vous ne pouvez pas supprimer un objet verrouillé,
- `read-only` : vous ne pouvez pas faire de changement à un objet.

Pour pouvoir effectuer des actions qui sont verrouillées, il est nécessaire auparavant de retirer le verrouillage.

## Blueprints

`Blueprints` permet de créer des abonnements "gouvernés" (gérés) afin d'aider à concevoir des environnements conformes aux normes organisationnelles et aux meilleures pratiques.

Ce service repose sur des templates pour la création de ressources Azure.

Ils peuvent être utilisés pour :
- les templates de ressource
- RBAC
- les policies
- les échantillons pour les régulations communes

## Advisor

`Advisor` permet d'analyser la sécurité et la configuration des ressources.

Equivalent AWS : `Trusted Advisor`

## Monitor

`Monitor` permet de collecter des données de télémétrie depuis les ressources. Vous pouvez analyser ces données pour maximiser les performances et la disponibilité et pour identifier les problèmes.

La plupart des services Azure alimentent les données de télémétrie dans Azure Monitor.

Les services on-premise peuvent également y envoyer des données.

Il est centralisé et entièrement managé.

Il intègre un langage de requêtage.

L’utilisation du Machine Learning permet de prédire et de reconnaître des problèmes plus rapidement.

Ses avantages sont :
- maximise les performances et la disponibilité,
- identifie les issues.

Equivalent AWS : `CloudWatch`

## Service Health

`Service Health` informe de tout incident planifié et non planifié sur Azure. Il intègre un dashboard personnalisé pour mettre en évidence les problèmes de service affectant les ressources.

Des alertes personnalisées permettent d’être notifié des interruptions de service planifiées ou non planifiées.

Ce service trace les alertes et les issues en temps réel et fournit un rapport complet une fois résolus.

C’est un service entièrement gratuit.

Equivalent AWS : `Service Health`

## Azure Compliance Manager

Azure Compliance Manager permet de gérer les conformités. Il fournit des recommandations pour la conformité (GDPR, ISO, NIST, …) et les ressources.

Voici les principales régulations de l’industrie :
- GDPR (General Data Protection Regulation) : L’objectif principal est de protéger les individus et le traitement de leurs données. Il redonne un contrôle des données personnelles à l’individu, plutôt que la société ne le possède. Les sociétés doivent implémenter de nombreux outils pour contrôler leurs données.
- Standard ISO : Il existe de nombreuses catégories ISO, comme la « compliance with quality and customer satisfaction »; la plus commune est ISO 9001:2008.
- NIST (National Institute of Standards and Technology) : Elle se focalise sur l’industrie de technologie. Développé principalement pour les agences fédérales américaines. Une conformité avec NIST signifie une conformité avec de nombreuses régulations fédérales américaines.

Il affecte des tâches de conformité aux membres de l’équipe et suit le progrès.

Il fournit un score de conformité afin de viser un score parfait de 100%.

Vous pouvez uploader et stocker de manière sécurisée des documents pour prouver la conformité.

Vous pouvez obtenir des rapports de données de conformité à fournir aux managers et aux auditeurs.
- Azure Governement Cloud fournit des Data Centers dédiés aux agences gouvernements américaines :
    - Si vous être une agence gouvernementale américaine ou si vous êtes en contrat avec le gouvernement américain, vous pouvez accéder à des Data Centers de régions dédiées.
    - Vous avez la garantie que seuls les membres du personnel filtrés de nos autorités fédérales, étatiques et locales ont accès.
    - Conformité : Assurez la conformité avec les agences gouvernementales américaines requises et l'approbation du Department of Defense de niveau 5.
    - Vous bénéficiez des avantages du cloud Azure standard: haute disponibilité, évolutivité et ressources gérées.
- Région de Chine :
    - Le Data Center est localisé physiquement en Chine et n’a aucune connexion hors de Chine, y compris les autres régions Asuez.
    - Les données client sont conservées en Chine. Certains services Azure ne fonctionnement par entièrement.
La conformité n’est pas négociable.

## Azure Privacy

`Azure Privacy` fait partie du noyau d’Azure et de ses produits.

Il comprend :
- `Azure Information Protection` : il classifie, labélise et protège les données basées sur la sensibilité des données.
- `Azure Policy` : Il définit et renforce les règles pour garantir la privacy et les régulations externes.
- le guide GDPR pour aider à répondre et se conformer aux requêtes privées GDPR.

## Trust Center

Trust Center fournit ce qui permet à Microsoft de garder votre confiance. Il permet de comprendre l’effort de Microsoft sur la sécurité, la privacy, le GDPR, l’emplacement des données, la conformité, …

## Service Trust Portal

`Service Trust Portal` fournit l’accès aux rapports d’audit, aux guides de conformité et aux documents d’approbation délivrés à Azure.

Equivalent AWS : `Artifact`

# Prix

## Subscriptions

Toutes les ressources appartiennent à une Subscription.

Un compte Azure peut avoir plusieurs subscriptions. Cela aide à organiser qui paie quoi.

Plusieurs utilisateurs peuvent être administrateur pour la facturation. Cela permet de séparer les responsabilités.

Les cycles de facturation sont soit de 30 jours, soit de 60 jours.

Il y a plusieurs types d’offre.

Il est possible de gérer de manière groupée :
- les Subscriptions
- les accès, les politiques et les conformités

Vous maintenez la facturation associée avec le bon budget.

Imbriquez la gestion des groupes avec la hiérarchie et les relations indiquées.

Il n’est pas possible d’utiliser Azure sans acheter des services et des produits, mais cela peut être fait de différentes manières.

Equivalent AWS : `Organisations`

## Cost Management

Certains services sont gratuits, d’autres le sont partiellement à partir d’un certain seuil, d’autres un certain temps seulement.

`Cost Management` permet d'optimisezr les coûts du Cloud tout en maximisant le potentiel du Cloud. Il fournit un rapport des coûts actuels et projetés. Ce service est gratuit. Il fournit aussi des recommandations sur comment économiser les coûts et les analyser. Il permet aussi d’optimiser les ressources actuelles pour économiser de l’argent et surveiller les coûts des services Azure.

Le calcul des coûts sur une infrastructure mature est quasiment impossible.

Différents critères influencent sur les prix :
- la taille des ressources
- le type de ressource
- l’emplacement
- la bande passante

??? Les données entrantes (ingress) est gratuite entre deux Billing Zones, mais pas les données sortantes (egress). Les données entrantes et sortantes au sein d’une même Billing Zone sont gratuites.

Equivalent AWS : `Cost Explorer`

## Pricing Calculator

Le calculateur de prix (Pricing Calculator) permet de :
- choisir parmi tous les services Azure disponibles
- fournir une estimation du coût mensuelle
- sélectionner les propriétés de ressource (exemple : machine virtuelle)
- exporter l’estimation pour une analyse et utilisation ultérieures

Equivalent AWS : `Pricing Calculator`

## Total Cost of Ownership Calculator

Le Total Cost of Ownership Calculator permet d’obtenir une estimation des économies totales que vous pouvez réaliser en déplacement vos ressources on-premise vers Azure. Paur cela :
- estimer les économies sur une période de temps en utilisant Azure
- partager le rapport complet avec les stakeholders

Equivalent AWS : `Total Cost of Ownership Calculator`

# Best practices

## Les limites de dépense

Certaines comptes Azure avec des crédits mensuels à utiliser ont des limites de dépense par défaut.

Quand il n’y a plus de crédit, soit vous laissez en l’état, soit vous retirez la limite entièrement.

Les comptes « pay-as-you-go » n’ont pas de limite de dépense.

## Les quotas

Un quota est une limite sur une propriété d’un service Azure (exemple : 100 namespaces maximum pour Event Hub).

Ce quota est nécessaire pour que Microsoft puisse maintenir son niveau de service élevé mais il peut être augmenté sur demande à Microsoft.

## Les tags

Les tags sont attachés à une ressource ou à un groupe de ressources. Ils n’ont pas d’impact fonctionnel. Vous pouvez en utiliser n’importe quel valeur.

??? Ils permettent de protéger les données sensibles en utilisant les tags lors de la définition des rôles.

Les tags permettent d'exécuter des batch facilement en spécifiant les tags des ressources à utiliser.

Les tags permettent de filtrer les ressources par projet ou par client par exemple.

C’est une bonne pratique de créer une liste de tags utilisés qui comprend la description, le nom du tag, les valeurs potentielles.

## Le pay-as-you-go

Le pay-as-you-go est cher. Les instances réservées vous permettent de faire de grosses économies.

## Advisor

`Advisor` fournit des conseils de bonnes pratiques sous forme de recommandations à partir d'une analyse de la sécurité et de la configuration des ressources Azure.

# Support

## Plans

Tous les plans de support comprennent :
- un accès 24h/24
- la documentation et les livres blancs Azure
- un accès aux forums pour pouvoir poser des questions et répondre aux questions
- `Advisor` qui fournit des recommandations de bonnes pratiques
- un accès à `Service Health` pour indiquer les problèmes actuels et les opérations de maintenant planifiées sur Azure

Les plans disponibles sont :

| | Développeur | Standard | Professional Direct | Premier |
|-|-|-|-|-|
| Prix | $ | $$ | $$$ | $$$$ |
| Support technique | Heures de bureau<br>Email | 24h/24<br>Email, téléphone | 24h/24<br>Email, téléphone | 24h/24<br>Email, téléphone |
| Cas de support | Illimité | Illimité | Illimité | Illimité |
| Configuration Azure | Guide de dépannage | Guide de dépannage | Guide de dépannage | Guide de dépannage |
| Temps de réponse | Sev. C < 8 heures | Sev. C < 8 heures<br>Sev. B < 4 heures<br>Sec. A < 1 heure | Sev. C < 4 heures<br>Sev. B < 2 heures<br>Sec. A < 1 heure | Sev. C < 4 heures<br>Sev. B < 4 heures<br>Sec. A < 1 heure |
| Support architecture | Guide général | Guide général | Guide général | Guide général |
| Support opérations | | | Intégration (onboarding), revues | Revues techniques, reporting, Manager de compte technique |
| Formation | | | Webinars | A la demande |

Plus vous payez, plus vous avez de bénéfices et plus les temps de réponse sont faibles.

## Tickets

Un ticket correspond à une requête au support. Il  possède un identifiant unique. Un ticket correspond à un et un seul problème.

Pour soumettre un ticket :
- créez un ticket sur le portail Azure,
- renseignez les détails sur le issue,
- choisissez le type de ticket.

Le ticket est alors traité par l’équipe support.

## Canaux

Les canaux de support sont :
- la documentation Azure
- les forums
- les médias sociaux

## Knowledge Center

Le Knowledge Center permet de regrouper les réponses aux questions communes. Vous ne pouvez pas ajouter de nouvelle question. Vous pouvez par contre faire une recherche par catégorie, par produit ou par texte libre. Toutes les réponses ne sont pas dans Knowledge Center.

## SLAs

Les accords de niveau de service (Service Level Agreements - SLA) :
- permettent de garantir la confiance dans la disponibilité et la fiabilité des service,
- permettent de contracter entre vous et Azure,
- varient en fonction du nombre et de la variété des services et de la région utilisée,

Il y a plusieurs SLAs mais en général un par produit.

Il n’y a pas de SLA pour les produits et services gratuits.

## Cycle de vie des services

Les clients peuvent fournir des feedbacks de valeur.

Les services Azure peuvent être en preview privée ou publique avant de devenir globalement disponibles.

Les étapes sont :
- La Preview (http://preview.portal.azure.com) fournit :
    - privé : seulement disponible aux clients spécifiques invités par l’équipe produit derrière le service
    - public : disponible à tous les clients Azure; donne un accès aux fonctionnalités preview dans le portail Azure
- disponibilité générale : disponible à tous les clients Azure comme service normal, y compris SLA. Les services deviennent généralement disponibles quand ils sont prêts. Des services peuvent sortir graduellement dans certaines régions d’abord.

`Azure Updates` permet d’avoir une vision sur les previews, les nouvelles fonctionnalités et les mises à jour.

## S'entraîner à la certification

https://practice-exam.acloud.guru/az-900-microsoft-azure-fundamentals?_ga=2.89860504.2009693110.1594797649-2095911007.1593330759
