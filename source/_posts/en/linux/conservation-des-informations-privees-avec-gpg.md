---
title: "Conservation des informations privées avec GPG"
date: 2020-05-18 07:21:21
tags:
  - linux
categories:
  - [linux]
primary_category: linux
primary_category_display_name: "Linux"
---

PGP (Pretty Good Privacy) peut être utilisé pour :
- signer numériquement les emails,
- crypter du contenu (emails, licences, ...).

GPG est ouvert et gratuit.

Le site Web `https://howsecureismypassword.net` fournit des informations sur la sécurisation des mots de passe.

## Installation

Vérifier que GPG est installé :

```
grep install /var/log/dpkg.log | grep gpg
```

Pour obtenir plus d'informations sur les dates d'installation des autres packages :

```
grep install /var/log/dpkg.log
```

Pour afficher la liste des options disponibles :

```
gpg --dump-options
```

Certaines options font référence à des RFCs (Request For Comments) :

```
gpg --dump-options | grep rfc
```

Pour regarder la RFC 4880 :

```
gpg --rfc4880
```

Pour activer la fonctionnalité de compatibilité OpenPGP, ajouter le flag `--rfc4880` ou `--openpgp`.

Vous devez taper un message pour crypter le texte.

Un sous-répertoire `~/.gnupg` est créé avec des fichiers :
- un fichier de configuration (`gpg.conf`),
- deux fichiers "keyring" binaires : ils sont vides et ne seront remplis que lorsque vous aurez importé des clés.

## Création d'une paire de clés

Pour générer une nouvelle paire de clés :

```
gpg --gen-key
```

- Taper les clés que vous voulez générer par GPG. Des fichiers sont alors créés dans le sous-répertoire `~/.gnupg`.
- Choisir les clés RSA par défaut (`(1) RSA and RSA (default)`).
- Choisir la valeur par défaut à `What keysize do you want?` (`2048`).
- Entrer 0 comme date d'expiration : les clés n'expireront jamais.
- Fournir le nom et l'email.
- Laisser le commentaire vide.
- Taper une passphrase.
- Saisir des données aléatoires.

La clé est maintenant créée.

De nouveaux fichiers binaires ont été créés dans le répertoire `~/.gnupg` :

```
ls -l ~/.gnupg
```

Seul le fichier `.conf` est lisible.

Pour générer une clé "blindée" en ASCII correspondant à l'identifiant unique (`uid`) de la commande précédente :

```
gpg --armour --export chris@binnie.tld > pubkey.asc
```

`pubkey.asc` est la clé utilisée pour le partage avec les autres.

## Cryptage d'un message

Pour supprimer une clé :

```
gpg --delete-key you-are-outta-here@binnie.tld
```

Pour crypter un message :

```
gpg --import chris-pubkey.asc
```

La clé importantée est ajoutée au "keyring".

- Fournir le message secret dans le fichier `in.txt`.
- Encrypter ce message avec une clé publique en spécifiant la clé publique à utiliser avec l'option `-r` :

```
gpg --encrypt -r chris@binnie.tld --armour < in.txt -o out.txt
```

Un fichier `out.txt` est créé avec le message encrypté.

## Décryptage d'un message

Décrypter un message :

```
gpg --decrypt out.txt
```

- Entrer la passphrase pour déverrouiller la clé privée.

La passphrase devient facilement la partie la plus faible du processus de sécurisation.

Lors d'un échange en clair sur le réseau, votre passphrase peut être facilement identifiée en sniffant le réseau.

## Utilisation des signatures numériques

Importer la clé publique dans le "keyring" :

```
gpg --import chris-pubkey.asc
```

Pour envoyer un message lisible, signer le message avec GPG :

```
gpg --sign in.txt
```

- Entrer le mot de passe.

Le fichier `in.txt.gpg` est créé.

Le message n'est donc plus secret. Mais il est entouré d'une enveloppe (la signature numérique) que seul GPG comprend. Il permet donc de prouver sa provenance.

Pour spécifier un nom de fichier en sortie, utiliser `--output signed.sig`.

Ce message secret n'est plus un secret même s'il est enveloppé, compressé et présenté en mode binaire.

Il est maintenant possible de vérifier l'identifié de l'expéditeur du message et l'horodatage.

Pour dire aux gens qu'un email vient bien de moi ou pour ajouter un message sur un forum web afin de confirmer rapidement et simplement son authenticité et son intégrité :

```
gpg --clearsign textfile
```

Vous devez entrer votre passphrase pour signer à nouveau le document.

Un fichier de sortie non binaire est généré sous forme de fichier texte.asc.

Les inconvénients de la signature numérique sont :
- Les documents signés sont un peu lourds.
- L'utilisateur final doit faire des manipulations sur le document signé pour pouvoir le lire.

La solution est d'utiliser des "signatures détachées" :

```
gpg --output ouputfile.sig --detach-sig inputfile
```

Pour vérifier le message :

```
gpg --verify textfile.sig textfile
```
