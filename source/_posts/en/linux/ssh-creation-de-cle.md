---
title: "Création d'une clé SSH"
date: 2020-05-11 22:22:31
tags:
  - linux
categories:
  - [linux]
primary_category: linux
primary_category_display_name: "Linux"
---

# Génération et utilisation de paires de clés avec `ssh-keygen`

Les clés SSH reposent sur :
- une partie publique : elle peut être placée sur des serveurs,
- une partie privée : elle est conservée sur vous ou sur votre ordinateur portable.

Pour afficher l'adresse IP de votre hôte :

```
ip a
```

ou :

```
hostname -I
```

# Générer une clé RSA

Générer une clé RSA (Rivest-Shamir-Adleman) :

```
ssh-keygen -b 4096 -C "Example RSA Key"
```

Copier la clé RSA sur un second hôte :

```
ssh-copy-id 192.168.0.10
```

Depuis cet autre hôte, vérifier que vous pouvez accéder au premier hôte :

```
ssh 192.168.33.11
```

# Généré une clé Ed25519

Générer une clé Ed25519:

```
ssh-keygen -t ed25519 -C "Example Ed25519 key"
```

Copier la clé RSA sur un second hôte :

```
ssh-copy-id -i .ssh/id_ed25519.pub 192.168.0.10
```

Tenter de se connecter par SSH depuis le second hôte vers le premier :

```
ssh 192.168.33.11 -i .ssh/id_ed25519
```

Par défaut, les fichiers de clés publiques et privées se trouvent dans le dossier caché .ssh du home directory de l'utilisateur :
- la moitié publique se trouve dans des fichiers d'extension `.pub` (c'est ce fichier qui est placé sur les hôtes distants),

```
cat .ssh/id_rsa.pub
```

- la moitié privée se trouve dans des fichiers sans extension.

```
cat .ssh/id_rsa
```

## Le fichier `authorized_keys`

En se connectant à votre hôte distant, SSH valide l'ID de clé fournie par rapport à la liste de clés autorisées dans le fichier `authorized_keys`.

```
cat .ssh/authorized_keys
```

Il suffit d'ajouter manuellement les clés publiques dans le fichier `authorized_keys` sur l'hôte distant.

Les parties publiques des clés (fichiers *.pub) ont la permission 644 (lecture/écriture, lecture, lecture - `-rw-r--r--`).

Les parties privées des clés ont la permission 600 (lecture/écriture, aucune, aucune - `-rw-------`).

## Utilisation d'une phrase secrète

Il est recommandé de générer une clé avec une phrase secrète (passphrase).

En cas de besoin, vous pouvez révoquer une clé privée. Pour cela, il suffit de :
- retirer la clé publique de l'ensemble des hôtes autorisés,
- générer une nouvelle paire.
