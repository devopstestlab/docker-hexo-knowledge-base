---
title: "Identifier le code innefficace"
date: 2020-05-13 22:04:21
tags:
  - other
  - guideline
categories:
  - [others]
primary_category: others
primary_category_display_name: "Others"
---

Modularisation cycliquement dépendante :
- Cela arrive quand les abstractions sont fortement dépendantes les unes et des autres et deviennent fortement couplés.
- Cela peut être une dépendance directe ou indirecte.
- La solution est soit de refactorer le code dépendant, soit d'utiliser l'injection de dépendance pour découpler les implémentations.

Exemples :
- la "god line" : c'est une ligne de code qui fait beaucoup de choses
- la "bloated method" (méthode gonflée) : Cette méthode a trop d'actions. La réutilisabilité, la maintenance et la lisibilité est réduite.

L'encapsulation non exploitée :
- arrive quand le code client utilise des `if-else` ou des `switch` pour vérifier le type d'un objet au lieu d'exploiter la variation de type existante,
- repérer les longues séries de `switch` qui déclenchent différents comportements pour différents types d'objet,
- La solution est d'utilise des méthodes polymorphiques dans l'objet et de découpler le code de la vérification du type de l'objet.

Exemple :
- Les paramètres de méthode excessifs

Le `code smell` est une structure de code qui indique la violation de principes de conception fondamentaux et a un impact négatif sur la qualité de la conception.

Le `code smell` ce sont les warnings et les indicateurs pointant des inefficacités et des faiblesses potentielles.

Il peut être ignoré sans casser un programme, mais le laisser donne un désavantage au projet.

Il peut conduire à un logiciel fragile après une release et à des bugs en cascade.

## Le code dupliqué

Avec du code dupliqué, un changement de code nécessite des modifications en de multiples endroits. Ce sont autant de chances de bugs ou de crash si un emplacement n'est pas mis à jour.

La solution est de centraliser le code dupliqué et de limiter les changements de code à un seul endroit.

## Design smells

Le "Design smells" concerne les problèmes d'architecture :
- abstractions inappropriées,
- modularisation,
- dépendances malsaines.

### Exemple : Encapsulation déficiente

L'encapsulation déficiente concerne l'accessibilité des membres de la classe dans le code.
Les habitudes se mettent en place tout en apprenant à programmer et parfois ne partent jamais.
C'est une mauvaise compréhension de la manière dont les classes et leurs membres peuvent communiquer.

# Identification du code smells

## Au niveau des méthodes

Au bas niveau :
- nom des identifiants :
`var name` est trop court.
`var usernameInputFromFormField` est trop long.
`var usernameField` est correct.
- méthodes
- problèmes dans les conventions de nommage et contenu "bloated"

### Trop de paramètres de méthode

Cela correspond à une méthode qui a un trop grand nombre de paramètres. La solution est de décomposer les paramètres en utilisant des objets intermédiaires.

Exemple :

```
func registerNewUser(firstName: String, lastName: String, age: Int, birthday: String, yearlySubcription: Bool) {
	// ...
}
```

La solution est de grouper des données :

```
func registerNewUser(userData: User) {
	// ...
}

var newUser = User()
registerNewUser(userData: newuser)
```

### Trop de données de retour

```
func registerNewUser(userData: User) -> (success: Bool, userId: Int, accountType: String) {
	// ...
}

var newUser = User()
var (success, userId, accountType) = registerNewUser(userData: newUser)
```

La solution est de grouper les données. Si cela n'est pas possible, cela signifie qu'il s'agit d'une méthode "bloated" :

```
func registerNewUser(user: User) -> (result: RegistrationResult) {
	// ...
}

var newUser = User()
var result = registerNewUser(userData: newuser)
```

### God line

```
var viewTransparency = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 100, height: 100))).alpha
```

La solution est de rechercher les actions qui peuvent être découpées :

```
var origin = CGPoint(x: 0, y: 0)
var size = CGSize(width: 100, height: 100)
var newView = UIView(frame: CGReect(origin: origin, size: size))
var transparency = newView.alpha
```

### Conseils

- Se focaliser sur ce que le code fait, pas sur comment il le fait.
- Trouver un endroit idéal pour les conventions de nommage.
- Trouver le but principal de vos méthodes.

## Au niveau des classes

### Goldilocks zone

Les classes surdimensionnées ("oversized classes") sont des objets "god" qui essayent de faire trop de choses.

La solution est de les décomposer en plus petites classes.

Les classes sous-dimensionnées n'encapsulent pas beaucoup de données ni de comportement.

La solution est de les incorporer dans une classe déjà existante.

### "Envie de fonctionnalité" (Feature envy)

`Feature envy` correspond au cas où une classe utilise trop de méthodes d'une autre classe. Cela conduit à une dépendance et à du code fragile.

Il y a plusieurs solutions :
- transférer du code à la classe dépendante,
- refactorer pour partager des membres de classe,
- utiliser la technique de l'injection de dépendance.

### Intimité inappropriée ("inappropriate intimacy")

L'intimité inappropriée correspond au cas d'une classe dépendante de l'implémentation d'une autre classe. Cela se produit généralement quand une classe fait référence à des membres privés d'une autre classe.

Les solutions sont :
- refactorer le code,
- créer une implémentation partagée.

### Utilisation litérale ("literal usage")

L'utilisation litérale correspond au cas de l'utilisation de valeurs codées en dur. Cela conduit à des erreurs et rend difficiles les modifications.

```
class Post {
	var maxComments = 120000
}
```

Les solutions sont :
- transformer ces litérales en constantes,
- les stocker dans un fichier de ressources.

```
class Post {
	static let maxComments = 120000
}
```

### Agrégation de données ("data clumping")

L'agrégation de données consiste à passer les mêmes valeurs de données primitives.

La solution est de créer un modèle de données ou un objet et de la passer au lieu de passer ces valeurs.

## Au niveau de l'application

### La chirurgie au fusil de chasse ("shotgun surgery")

La chirurgie au fusil de chasse consiste à dupliquer du code. Les conséquences sont que les mises à jour nécessitent la modification de blocs de code répétés dans de multiples emplacements. Cela conduit à des erreurs et à des bugs.

La solution consiste à stocker le code répété en un seul endroit.

### La complexité artificielle ("contrived complexity")

La complexité artificielle correspond au cas où une complexité excessive a été utilisée, par exemple en utilisant des design patterns pour une tâche simple, juste parce qu'il existe.

Le surcoût n'est pas nécessaire.

# Les smells de conception

## Identification des mauvaises abstractions

Les catégories de "design smells" :
- abstractions,
- encapsulation,
- modularisation,
- hiérarchie.

Une abstraction traite des caractéristiques essentielles d'un objet, qui définissent ses limites dans le code.

### Abstractions manquantes

- Des litéraux et des chaînes encodées sont utilisées au lieu d'un modèle de données.
- Aussi référé comme "data clumping" et "privimetive obsession"

La solution est de groupes les types de données de primitive dans leur propres abstractions.

### Abstractions multi-facettes

Les abstractions multi-facettes arrivent quand une abstraction a plus d'une responsabilité.

```
Classe : encodage, décodage, écriture des données, lecture des données
```

La solution est de :
- décomposer le code en des abstractions séparées,
- se focaliser sur une seule responsabilité.

```
Classe A : encodage, décodage
Classe B : écriture des données, lecture des données
```

### Abstractions dupliquées

Les abstractions dupliquées correspondent à des classes ou des interfaces ayant le même nom et/ou le même comportement.

La solution est :
- une revue de code,
- un refactoring.

Rechercher les responsabilités de classe similaires et les refactorer dans une seule abstraction.

### Abstractions incomplètes

Les abstractions incomplètes correspondent à des classes ou des interfaces qui ne remplissent pas totalement une responsabilité. Cela peut arriver quand une classe parent a des méthodes optionnelles ou remplaçables ("override").

Les classes dérivées ne sont ainsi pas forcées de remplir entièrement leur responsabilité.

## Mauvaise compréhension de l'encapsulation

Une encapsulation place dans des compartiments les éléments d'une abstraction, séparant l'interface de l'implémentation.

### Enpcasulation déficiente

L'enpcasulation déficiente arrive quand l'abstraction de l'implémentation et/ou des membres sont exposés ou pauvrement protégés. Cela conduit à la vulnérabilité des données et à une mauvaise utilisation du comportement.

La solution est de s'assurer que :
- les abstractions ne sont accessibles que quand elles ont besoin de l'être,
- leurs membres sont protégés.

### Encapsulation effrenée ("unrestrained encapsulation")

L'encapsulation effrenée survient quand l'état d'abstraction est globalement visible. Cela peut conduire à la corruption de données et à un abus de comportement.

Ce smell est proche de l'enpcasulation déficiente.

La solution est d'éviter d'utiliser l'accessibilité globale.

### Encapsulation non exploitée ("unexploited encapsulation")

L'encapsulation non exploitée arrive quand du code utilise des instructions `if-else` ou `switch` pour vérifier le type d'un objet au lieu d'exploiter une variable de type existante.

Surveiller les longues instructions `switch` qui déclenchent des comportements différents pour des tyles d'objet différents.

Les solutions sont :
- utiliser des méthodes polymorphiques dans l'objet,
- découpler le code de la vérification de type d'objet.

## Modularisation incorrecte

Un système est considéré modulaire s'il est décomposé en modules cohésifs et faiblement couplés.

### Modularisation insufissante

La modularisation insufissante arrive quand une abstraction n'a pas atteint sa décomposition idéale. Cela peut arriver dans des hiérarchies de classe profondément imbriquées ou de classes "bloated".

Les solutions sont :
- rechercher une base d'abstraction ou un but essentiel,
- décomposer le code à ce niveau de détail.

### Modularisation brisée

La modularisation brisée survient quand des données ou des méthodes qui devraient être groupées ensemble sont répandues dans plusieurs abstractions.

Cela est similaire à l'agrégaton de données.

La solution consiste à se concentrer sur la refonte de comportements et d'informations similaires.

### La modularisation cycliquement dépendante

La modularisation cycliquement dépendante arrive quand les abstractions sont fortement dépendentes les unes des autres et deviennent fortement couplées.

Cela peut être une dépendance directe ou indirecte.
Les solutions sont :
- refactorer le code dépendant,
- utiliser l'injection de dépendance pour découpler les implémentations.

## La mauvaise gestion de la hiérarchie

Les hiérachies sont des abstractions classées ou ordonnées.

### La hiérarchie polygone

La hiérarchie polygone correspond à des abstractions de base héritées à plusieurs reprises, formant ainsi des formes de polygone.

La solution est de retirer l'héritage redondance pour rationaliser la hiérarchie d'abstrations.

### La hiérarchie brisée

La hiérarchie brisée survient quand une abstraction de base et une abstraction dérivée ne partagent pas une relation "est-un" ("is-a") comme elles le devraient.

Cela arrive en général quand l'héritage est utilisé au lieu de la composition.

Cela est particulièrement dangereux puisqu'il permet des comportements partagés.

La solution est de remplacer l'héritage par la composition ou par la structure de délégation.

### La hiérarchie complexe

La hiérarchie complexe survient quand les graphes ou les cartes de hiérarchie sont trop emmêlés : elles sont alors trop larges, trop profondes, trop déséquilibrées.

Cela est prévalent dans les bases de code qui sont passées de développeur à un autre.

La solution est de décomposer les hiérarchies complexes.

### La hiérarchie cyclique

La hiérarchie cyclique survient quand un supertype fait référence ou dépend d'un sous-type, par exemple un type contenant un objet de sous-type, utilisant un nom ou un membre d'un sous-type.

Ces références peuvent être directes ou indirectes.

La solution est de faire une refonte du code dépendant.
