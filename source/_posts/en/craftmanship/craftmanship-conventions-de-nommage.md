---
title: "Conventions de nommage"
date: 2020-05-13 22:04:21
tags:
  - others
  - guideline
categories:
  - [others]
primary_category: others
primary_category_display_name: "Others"
---

# Des noms cleans

## Pourquoi ?

Incorrect :
```
def a(b, c)
  b + c
end

puts a(5, 4)
```

Correct :
```
def add(left, right)
  left + right
end

puts add(5, 4)
```

## Préférer la clarté à la brièveté

Pourquoi laisser le lecteur deviner le sens des variables ?

Incorrect :
```
const fruits = [
  'apple',
  'banana',
  'cherry',
  'date'
];

for (let i = 0; i < fruits.length; i++) {
  const f = fruits[i];
  console.log(f);
}
```

Correct :
```
const fruits = [
  'apple',
  'banana',
  'cherry',
  'date'
];

for (let index = 0; index < fruits.length; index++) {
  const fruit = fruits[index];
  console.log(fruit);
}
```

## Acronymes et abréviations

@When you're using multiple acronyms in a name, make sure that you can clearly distinguish between them.

Incorrect :
`class HTTPAPIClient`

Correct :
`class HttpApiClient`

@For abbreviations is to avoid them unless they are very, very common.

Incorrect :
`private static async Task<List<Repo>> RetRepos(`

Correct :
`private static async Task<List<Repo>> RetrieveRepositories(`

## Noms de classe et de type

@Prefer nouns with the first letter capitalized for your class names.

@Use an adjective as a prefix.

Une classe ou un type est utilisé pour représenter une catégorie de choses. Donc pour les noms de classe, ne pas utiliser de verbe :
`class Perform: pass`

Préférer les noms :
`class Performer: pass`

Pour passer le passage du temps dans un nom de classe, utiliser un adjectif en préfixe :
`class ActivePerformance: pass`

Eviter d'utiliser un adjectif ou un nom de couleur juste en nom de classe :
`class Small: pass`

Un adjectif peut être utilisé en préfixe d'un nom :
`class SmallPerformance: pass`

Eviter les préfixes vagues :
`class MyPerformer: pass`

Eviter les noms de classe d'une seule lettre :
`class P: pass`

Eviter les préfixes d'une seule lettre, sauf pour un système de template qui utiliser un "T" pour "Template" :
`class CPerformer: pass`

Pour les acronymes, ne pas utiliser que des lettres majuscules :
`class HTTPAPIPerformer: pass`
Plutôt mettre en majuscule la première lettre de chaque acronyme :
`class HttpApiPerformer: pass`

Eviter les abréviations :
`class Perf: pass`

Ne pas utilise de pluriel pour les classes normales :
`class Performers: pass`
L'utiliser pour les classes de collection.

## Noms de méthode et de fonction

Pour les noms de méthode, utiliser le présent :
`def perform; end`

Pour le gérondif, utiliser le préfixe `is_` :
`def is_performing; end`

Pour le passé, utiliser un préxie `has_` :
`def has_performed; end`

Pour les accesseurs, utiliser des noms :
`def name; end`
ou utiliser un préfixe `get_` :
`def get_name; end`

## Les noms de variable

Pour les types primitifs et pour les instances d'objet, préférer des noms au singulier :
`String name = "Alma";`
`Performer performer = new Performer(name);`

Pour les tableaux et les collections, utiliser des noms au pluriel :
`String names[] = {"Alex", "Ali", "Aesop"};`
`List<Integer> years = Arrays.asList(1980, 1999, 2003, 2010);`

Pour les variables qui stockent des types de primitive, utiliser des noms plutôt que des verbes :
`int performanceCode = 12;`

Pour les variables qui stockent des fonctions lambda ou des closures, suivre les règles des noms des méthodes :
`BiFunction<Integer, Integer, Integer> add = (left, right) -> {`

Eviter les noms de variable d'une lettre :
`int t = 12;`

Eviter les acronymes et les abréviations.

Eviter les préfixes compliqués.

Eviter d'utiliser un nom de type en suffixe :
`String lastNameString = "Amaya";`

## Les noms de paramètre

Pour les noms de paramètre, appliquer les mêmes règles que pour les noms de variable.

## Les noms de constante

Capitaliser le première caractère.

Utiliser des noms au singulier pour les valeurs primitives.
`Color (Color::red)`

Utiliser le pluriel pour une collection de valeurs.

Eviter les noms d'une seule lettre et les abréviations.

S'assurer de la séparation entre les acronymes.

Garder une consistence dans les membres Enum : éviter `Action::red`

Respecter les règles de formatage des constantes du langage.

Respecter les règles de formatage des énumérations du langage.

# Formatage propre

Un long programme peut être formaté sur uen seule ligne de texte.

Il est possible de créer du code difficile à lire.

Suivre les règles pour créer du code qui sera facile à lire dans un futur éloigné.

## Indentation et placement des accolages

Certains langages forcent des règles d'indentation.

Règles :
- etre consistent.
- le mélange et l'appariement causeront une grande confusion
- choisir des tabulations ou des espaces, mais jamais les deux

Combien d'espace ?
- 2 ou 4 est un bon choix.
- Choisir un nombre et être consistent.

L'éditeur de texte peut forcer les tabulations ou les espaces.

Respecter le style des accolades { et } du langage.

## Wrapping de ligne

Le code devient difficile à lire si le lecteur doit scroller de gauche à droite pour lire le code.

Une ligne à 80 colonnes est une limite répandue dans l'industrie.

Ecrire un paramètre par ligne.

## Les lignes blanches (whitespaces)

Les lignes blanches (whitespaces) :
- verticaux :
  - améliorent la lisibilité du top-down,
  - sont utilisées pour grouper des morceaux de code liés,
  - sont similaires au fonctionnement des paragraphes dans les phrases.
- horizontaux :
  - améliorent la lisibilité gauche-droite,
  - un espace après chaque virgule
  - l'identation est une forme spécial de whitespace
  - un espace avant et après les opérateurs booléens (+, -, /, *)

## Les commentaires

Les commentaires sont ignorés par le compilateur. Il n'est lu que par les humains.

C'est un outil de communication.

Ils répondent aux questions "pourquoi".

Les mauvais commentaires :
- ceux qui répondent à la question "quoi",
- ceux qui sont destinés à facilier la lecture du code,
- ceux qui sont utilisés pour désactiver du code lors de l'exécution.

# Une logique clean

## Nombre magiques et constantes

Pour les variables qui ne peuvent pas changer, utiliser des constantes nommées. Cela permet d'associer un nom descriptif à une valeur constante.

## Listes de paramètres

Le nombre de paramètres des méthodes devrait être le plus petit possible. Moins il y a de paramètres pour une méthode, plus elle est facile à lire.

## Méthodes prédicats

La logique booléenne complexe est utilisée pour encoder des détails importa,ts et des règles métier.

Elle est souvent complexe et difficile à comprendre, et donc pas toujours claire.

Les méthodes prédicat encapsulent la logique booléenne et retourne `true`ou `false`. Le nom de la méthode peut porter son sens.

```
        private static bool EndsCorrectly(string input)
        {
            return EndsWithStatementMark(input) ||
                   EndsWithQuestionMark(input) ||
                   EndsWithExclamationMark(input);
        }
```

## La bonne utilisation des boucles

La règle de trois : ne pas se répéter plus d'une fois.

print('')

print('')
print('')

for _ in range(3):
	print('')

# Des tests unitaires clean

Un test unitaire doit être :
- isolé,
- propre (clean),
- rapide :
  - Il doit être exécuté en moins d'une seconde.
  - Eviter la communication avec des services externes et avec la base de données.
- focalisé,
- indépendant,
- concis.

Utiliser les tests unitaires comme documentation.

Don't Repeat Yourself (DRY) :
- Il est possible de pousser trop loin cette approche : combien de DRY appliquer au code du test.

Les suites de test avec une répétition excessive sont difficiles à maintenir.
