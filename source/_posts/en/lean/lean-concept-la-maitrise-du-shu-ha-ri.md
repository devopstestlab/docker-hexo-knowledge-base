---
title: "La maîtrise du Shu Ha Ri"
date: 2020-05-13 22:04:21
tags:
  - lean
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

### Présentation

Le concept de Shu Ha Ri est un concept très important dans le monde d’Agile. Il prend ses racines dans les Arts Martiaux japonais.

Shu – Suivre les règles :
- L’étape Shu est d’obtenir les bases.
- Dans le cas des Arts Martiaux, les élèves suivent les instructions du professeur et répètent encore et encore les pratiques. Avec le temps, ces gestes deviennent naturels.
- Appliqué à Scrum, cela correspond à la manière dont sont réalisés les Sprint Planning et la manière dont les User Stories sont rédigées.
- Une fois que les gens les appliquent sans y penser, ils peuvent passer au stade suivant, le stade Ha.

Ha – Briser les règles :
- L’objectif de l’étape Ha est de maîtriser les bases.
- Dans le cas des Arts Martiaux, à ce stade, les élèves commencent à comprendre le but de ces pratiques. Ils peuvent alors commencer à combiner les conseils de plusieurs professeurs et à faire quelques écarts tout en conservant le sens d’origine.
- Appliqué à Scrum, cela correspond au questionnement de la raison d’être des pratiques Scrum et de leurs liens.
- Une fois que les gens ont bien compris le pourquoi de ces pratiques, ils peuvent passer au stade suivant, le stade Ri.

Ri – Etre la règle :
- L’objectif de l’étape Ri est d’apprendre de ses propres expériences.
- Dans le cas des Arts Martiaux, les élèves s’appuient sur l’expérience acquises lors de l’étape du Shu et sur la compréhension profonde acquise lors de l’étape du Ha pour devenir eux-mêmes des professeurs et créer leurs propres concepts et pratiques.
- Appliqué à Scrum, cela correspond par exemple à l’application de Scrum à d’autres domaines.

### Conclusion

Le Scrum Master peut utiliser ce concept Shu Ha Ri pour ajuster son approche en fonction du stade où se trouve l’équipe. Passer d’une étape à l’autre prend du temps.

Le Scrum Master veille à ce que l’équipe passe bien par chacune de ces trois étapes afin qu’ils maîtrisent Scrum, sous peine de pêcher par excès de confiance.
- Et dans votre cas, votre équipe se trouve à quel stade ?
- Quelles sont les pratiques qu’elle doit renforcer ?
- Quelles sont les pratiques qu’elle doit mieux comprendre ?
