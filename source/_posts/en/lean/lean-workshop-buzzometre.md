---
title: "Buzzomètre"
date: 2020-05-13 22:04:21
tags:
  - lean
  - workshop
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

### Présentation

Pour quoi faire ?
- Comprendre les perceptions et les croyances d’un groupe de travail sur un sujet.
- Permet de prendre la température ou les freins.
- Permet de prendre conscience des divergences dans les perceptions et convictions sur un sujet.

### Processus

Etape 1 (3 min) :
- Présenter l’atelier.
- Présenter le sujet.
- Préciser la confidentialité.
- Préciser qu’après le vote, chacun argumentera son choix, sur la base du volontariat.

Etape 2 (2 min) :
- Afficher la question posée : « Sur une échelle de 0 à 10, pensez-vous que … ».
- Laisser répondre.

Etape 3 (8 min) :
- Afficher les résultats.
- Indiquer la moyenne des votes.
- Indiquer comment se répartissent les votes entre les différentes valeurs.
- Proposer aux participants ayant voté des valeurs proches de 0 et de 10 d’argumenter leur choix.
- Faire de même pour les autres valeurs.

Etape 4 (2 min) :
- Effectuer une synthèse des échanges.
 Clôturer.
