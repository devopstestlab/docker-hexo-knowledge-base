---
title: "Suivi du temps de résolution des problèmes"
date: 2020-07-21 20:04:20
tags:
  - lean
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

### Le temps de résolution des problèmes

Face aux problèmes de délais de delivery, il faut se concentrer sur les files d'attente et les travaux en cours (work-in-progress).

Si le temps de traitement des problèmes tandis que la taille de la file d'attente n’évolue pas, le client attendant la résolution des problèmes peut ressentir de plus en plus de frustration.

Le délai et le temps de cycle sont des mesures du travail achevé tandis que le temps de résolution de problème est une mesure du travail non achevé.

L’analyse des durées (totale et moyenne) de résolution des problèmes permet d’améliorer les activités de résolution de problème, ce qui est important pour le client.

Les personnes doivent être incitées à signaler tout problème identifié. Il est très important que les problèmes ne soient pas cachés.

Les problèmes devant être résolus rapidement, commencer par les problèmes récents. Par ailleurs, traiter les problèmes anciens peut faire augmenter la frustration.

Les problèmes ne devraient pas être récurrents : une fois résolus, ils ne doivent plus survenir.

Il faut éviter les solutions de contournement à court terme.

Voici comment mesurer le temps de résolution des problèmes :
- Créer un Backlog d’améliorations.
- S’assurer que le board contient les problèmes, pas simplement des tâches.
- Calculer le temps de résolution des problèmes par ticket à partir de la date d’ajout au tableau.
- Calculer la durée totale et moyenne de résolution des problèmes pour l'ensemble du board.
- La communiquer aux membres de l’équipe.

Procéder ainsi chaque semaine. Répondre à la question « Que pouvons-nous faire de mieux ? » du point de vue des stakeholders.
