---
title: "Persona"
date: 2020-05-13 22:04:21
tags:
  - lean
  - workshop
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

### Présentation

Un persona est un archétype représentant un groupe de personnes dont les comportements, motivations et buts sont proches.

Pour quoi faire ?
- Mieux comprendre les clients et leurs besoins et habitudes de consommation.
Conseils :
- Prendre le temps d’identifier les bons personas.
- Ne retenir que les traits principaux des personas : ne pas chercher à être exhaustif.

### Processus

Etape 1 (5 min) :
- Expliquer les règles de l’atelier.

Etape 2 (15 min) :
- Lister les personas que les participants pensent devoir décrire (exemple : « Paul, graphiste, indépendant », « un cadre trentenaire marié sans enfant et plutôt aventurier », …).

Etape 3 (30-45 min) :
- Faire travailler les groupes sur leur person : qui il est, son contexte, ses motivations, ses occupations, ses attentes, ses besoins, ses contraintes, ses modes de consommation, ses objectifs, ses comportements, …
- Choisir quelques-uns de ces éléments en fonction de la finalité de l’atelier.
- Faire décrire ces éléments.

Etape 4 (5-10 min par groupe) :
- Faire restituer par chaque groupe son persona et ce qu’il retient.
- Faire intervenir les autres groupes pendant la restitution.

Etape 5 (5 min) :
- Conclure l’atelier.
