---
title: "La pensée diagnostique"
date: 2020-05-13 22:04:21
tags:
  - lean
  - workshop
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

La pensée diagnostique (diagnostic thinking) est la capacité d'examiner un problème et non pas d'aller directement d'un problème à une solution mais de passer à une étape intermédiaire, le diagnostic.

Dans le feu de l'action, lorsque les managers tentent de prendre des décisions, il est tentant orienté vers l'action : regarder le problème et proposer une solution.

L'idée est de faire une courte pause, de faire un choix puis d'aller de l'avant.

Il est vraiment tentant de passer d'un problème et de dire : voici une solution, foncez maintenant.

Nous avons besoin d'un peu de recul : regarder l'information, la comprendre et diagnostiquer.

Le diagnostic thinking consiste à amener les gens à regarder le problème, d'essayer de comprendre quels sont réellement les facteurs sous-jacents qui peuvent être à l'origine du problème, puis d'aller vers la solution.

Car parfois les causes sont autre part que le problème.

Regardez sous le problème pour comprendre ce qui se cache vraiment derrière.

Ensuite proposez une solution permettant un raisonnement de ediagnostique.
