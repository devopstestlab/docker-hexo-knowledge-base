---
title: "Les dysfonctionnements d’une équipe_REVOIR"
date: 2020-05-13 22:04:21
tags:
  - lean
  - workshop
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

### Présentation

Dans son livre « The Five Dysfunctions of a Team », Patrick Lencioni présente le concept « Les cinq dysfonctionnements d'une équipe ». Celui-ci est représenté sous forme de pyramide avec les couches les plus basiques en bas :

!(/images/les-dysfonctionnements-d-une-equipe.png)
￼
Pour pouvoir obtenir l'engagement de l’équipe, elle doit se faire confiance et pouvoir communiquer de manière efficace et honnête, même si elle n'est pas d'accord.

Pour exploiter ce concept, procédez comme suit :
- Identifier la profondeur de la pyramide des dysfonctionnements de l’équipe.
- Enseigner à l'équipe à accroître sa compréhension.
- Lui faire comprendre qu'elle a un problème.
- L'aider à créer un esprit d'équipe ou un accord sur leur manière de travailler ensemble.
- Une fois ces étapes franchies, ne plus demander aux individus ce qu'ils pensent, le demander à l’équipe.

Des dysfonctionnement :
- Absence de confiance : Une équipe subissant une absence de confiance reconnait rarement une absence de confiance. Pourtant, ses membres sont habitués à rester silencieux, à travailler individuellement. Leur peur d'être vulnérable les empêche de discuter et de coopérer. Chaque membre croit qu'il possède des connaissances techniques spécifiques. L’équipe protège le statu quo et maintient les silos.
- Peur du conflit : Une équipe ayant peur du conflit évite les conflits en évitant les discussions qui pourraient devenir difficiles, maintenant ainsi une harmonie artificielle. Elle maintient également les silos. Cela est représenté par des questions du type « Ca n'a pas de sens de coopérer sur une seule User Story. ».
- Manque d'engagement : Une équipe manquant d’engagement est un cas fréquent au début d’une transformation en Agile. Elle admet généralement qu'elle a du mal à s'engager. Cela est représenté par des questions du type « On ne peut pas dire ce que nous finirons à la fin du Sprint. » Cela est généralement lié à l’absence de confiance.
- Evitement de la responsabilité : Une équipe évitant de la responsabilité est assez fréquent. Par exemple, à la fin du Sprint au cours duquel tout le travail n’est pas terminé, l’équipe prétend fréquemment que c’est une exception et planifient la même quantité de travail pour le Sprint suivant. Si cela peut arriver de temps en temps, ça ne doit pas être régulier.
- Inattention aux résultats : Une équipe qui ne prête pas attention aux résultats cherche à atteindre des objectifs individuels (typiquement terminer son code ou ses tests) au lieu d'un objectif commun (offrir de la valeur au client).
