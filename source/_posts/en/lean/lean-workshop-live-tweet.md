---
title: "Live Tweet"
date: 2020-05-13 22:04:21
tags:
  - lean
  - workshop
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

### Présentation

Pour quoi faire ?
- Intelligence collective.
- Co-construction.
- Libérer la parole lors des réunions.
- Ancrer les messages importants grâce au nuage de mots-clés.

### Processus

Etape 1 (3 min) :
- Expliquer le mode de fonctionnement du Live Tweet et les attentes.
- Ouvrir le Live Tweet.

Etape 2 (10 min) :
- Faire tweeter les participants : idées, questions.
- Voter pour les idées ou questions à plébisciter.

Etape 3 (15 min) :
- Afficher le fil de tweet.
- Trier les tweets par nombre de likes.
- Sélectionner les principales questions.
- Les poser à l’oral.
- Organiser les échanges.

Etape 4 (7 min) :
- Afficher le nuage de mots-clés associés au fil de tweets.
- Travailller sur la mémorisation et la sémantique.

Etape 5 (5 min) :
- Mettre en évidence les messages clés.
