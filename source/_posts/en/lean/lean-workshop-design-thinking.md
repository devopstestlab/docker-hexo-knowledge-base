---
title: "Design thinking"
date: 2020-05-13 22:04:21
tags:
  - lean
  - workshop
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

### Présentation

Pour quoi faire ?
- Observer les comportements.
- Observer en mode etchnologique.
- Mieux percevoir les aspirations et besoins.

### Conseils

- Varier les profils pour avoir le plus de diversité possible.
- Rendre réaliste la situation.

### Processus

Etape 1 (15 min) :
- Présenter l’atelier et les phases successives.
- Choisir un sujet pour l’atelier.

Etape 2 - Phase empathique (1h)
- Mettre les participants à tour de rôle dans la peau du client visé par l’atelier au moment de l’usage du produit ou service.
- Observer le comportement.
- Le verbaliser.
- Le confronter au vécu du participant cobaye.

Etape 3 - Phase d’idéation (1 h)
- Faire émerger des idées de produits et services pour améliorer l’usage (exemple : « Imaginez que vous êtes Apple, que feriez-vous alors ? »).

Etape 4 (1 h) :
- Sélectionner des idées.
- Les réaliser sous forme de maquette à base de papier, carton, scotch, … pour rendre les choses plus concrètes.
- A partir de l’observation de leur maquette, faire améliorer le projet.
- Exposer la maquette à des clients ciblés afin de recueillir leur réaction.
