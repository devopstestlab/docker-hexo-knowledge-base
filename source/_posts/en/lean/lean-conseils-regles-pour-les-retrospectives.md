---
title: "Règles pour les rétrospectives"
date: 2020-05-13 22:04:21
tags:
  - lean
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

Eviter les expressions « aurait » ou « pourrait avoir » car ce ne sont pas des faits.

Eviter les expressions telles que « J'aurais pu … » ou « Si j'en avais eu connaissance, j'aurais dû … » car il est fait référence au système tel qu'il est imaginé, pas au réel.

Les gens se reprochent souvent des choses hors de leur contrôle ou remettent en question leurs propres capacités.

La meilleure question à se poser est la suivante : « Pourquoi cela a-t-il eu un sens quand j'ai pris cette mesure ? ».
