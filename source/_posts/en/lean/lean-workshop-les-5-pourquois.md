---
title: "Les 5 pourquois"
date: 2020-05-13 22:04:21
tags:
  - lean
  - workshop
categories:
  - [lean]
primary_category: lean
primary_category_display_name: "Lean"
---

### Présentation

Les 5 pourquois (Five Whys) permettent de réaliser une analyse des causes profondes (root-cause).

### Processus

- Demander « Pourquoi ? » cinq fois.
  - Exemple : « Notre produit a trop de bugs ».
- Pourquoi avez-vous autant de bugs ?
  - Parce que nous ne testons pas.
- Pourquoi ne testez-vous pas ?
  - Nous testons dans certains cas, mais comme le système est très complexe, nous ne pouvons pas comprendre comment fonctionne chaque scénario possible.
- Pourquoi ne le comprenez-vous pas ?
  - Nous ne savons pas comment les utilisateurs utilisent le système.
- Pourquoi ne savez-vous pas comment les utilisateurs utilisent le système ?
  - Nous n'avons jamais vu nos utilisateurs ni leur avons demandé des commentaires.
- Pourquoi ne leur avez-vous jamais demandé des commentaires ?
  - Parce que nous pensions que c'était le travail du Product Owner de le faire.
